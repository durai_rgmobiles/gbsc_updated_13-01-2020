import React from "react";
import LeftSidebar from "./left_sidebar.js";
import Footer from "./footer.js";
import Topbar from "./topbar.js";
import { PDFtoIMG } from "react-pdf-to-image";
import FetchAllApi from "./api_links/fetch_all_api.js";

import jQuery from "jquery";
// import 'bootstrap';
// import 'bootstrap-select';

class compose extends React.Component {
  constructor(props) {
    super(props);
    //const { history } = this.props;
    this.state = {
      ext:"",
      fileName: '',
      fileSelected: "",
      subject: "",
      message: "",
      value: "",
      item_details: "",
      item_file_path: "",
      selectedFile: null,
      usethis:""
    };
  }
  componentDidMount() {
    //jQuery(".select-picker").selectpicker();

    require("jquery-mousewheel");
    require("malihu-custom-scrollbar-plugin");

    jQuery(".item-listwrap").mCustomScrollbar({
      scrollEasing: "linear",
      scrollInertia: 600,
      scrollbarPosition: "outside"
    });

    jQuery(".label-enclose .label span").click(function() {
      jQuery(".label-enclose .label").removeClass("active");
      jQuery(this)
        .parent(".label-enclose .label")
        .addClass("active");
    });
    jQuery(".label-enclose .label a").click(function() {
      jQuery(this)
        .parent(".label-enclose .label")
        .removeClass("active");
    });
  }

  logoutLink() {
    localStorage.setItem("logged_user_id", "");
    localStorage.setItem("logged_client_id", "");
    localStorage.setItem("logged_role_id", "");
    localStorage.setItem("logged_user_name", "");
    localStorage.setItem("logged_user_email", "");
    localStorage.setItem("logged_user_phone", "");
    localStorage.setItem("logged_user_image", "");
    localStorage.setItem("logged_company_name", "");

    this.props.history.push("/");
  }

  dataTaggingFunc(file_id) {
    this.props.history.push("/data_tagging/" + file_id);
    window.scrollTo(0, 0);
  }

  handleEmpidChange = e => {
    this.setState({
      subject: e.target.value
    });
  };
  handlePasswordChange = e => {
    this.setState({
      message: e.target.value
    });
  };
  onChangeFile(event) {
    
    event.stopPropagation();
    event.preventDefault();
    var file = event.target.files[0];
    this.setState({ fileSelected:URL.createObjectURL(event.target.files[0]),
      usethis:file,
      fileName: event.target.files[0].name});
      this.getFileExtension( event.target.files[0].name);

    /// if you want to upload latter

  }

  sendUserInput(e) {
    e.preventDefault();
    var userData = {
      client_id:1,
      user_id:17,
      title: this.state.subject,
      description: this.state.message,
      attachments: this.state.usethis
    };
    console.log("userdajjjjjjjjjjjjjjjjjjta1", userData);

    FetchAllApi.saveNewDocument(userData, (err, response) => {
      //alert(response)
      console.log("userdajjjjjjjjjjjjjjjjjjta", response);

      console.log("erwrjhgjkghkasdjgjkgasdgajkdghjkdg", response.message);
      if (response.status === 1) {
        this.props.history.push("/inbox");

      } else {
        alert(response.message);
      }
    });

    console.log("itsbadtime", userData);
  }

getFileExtension=(filenameval)=>{
var file = filenameval;
var ext = file.split('.').pop();
this.setState({ext:ext})



  }

 render() {
console.log('hhhhhhhhuuuuu',this.state.ext)
    const { fileName } = this.state;
    let file = null;

    console.log("itsbadtime", this.state.file);

    let get_file_path,
      dis_file_path = [],
      item_file_path = [],
      attach_file_path,
      options = [],
      page_no = 1,
      items_limit = 10,
      no_items;

    if (
      this.state.item_details.user_image !== "" &&
      this.state.item_details.user_image !== "null"
    ) {
      var item_user_image = this.state.item_details.user_image;
    } else {
      var item_user_image = "images/user-img-1.png";
    }

    //console.log('item_files', this.state.item_file_path);
    if (
      this.state.item_file_path !== "" &&
      this.state.item_file_path !== "null"
    ) {
      item_file_path = [];
      var split_file_path = this.state.item_file_path.toString().split(",");
      var split_file_id = this.state.item_file_id.toString().split(",");
      if (split_file_path.length >= 1) {
        for (var i = 0; i < split_file_path.length; i++) {
          var get_file_name = split_file_path[i];
          var split_file_name = split_file_path[i].toString().split("/");
          var arr_reverse = split_file_name.reverse();

          var get_file_ext = arr_reverse[0].substring(
            arr_reverse[0].lastIndexOf(".") + 1,
            arr_reverse[0].length
          );
          if (get_file_ext === "pdf") {
            var file_icon = "images/pdf-icon.png";
          } else {
            var file_icon = "images/img-icon.png";
          }

          item_file_path.push(
            <div className="attach-item">
              <a
                onClick={this.dataTaggingFunc.bind(this, split_file_id[i])}
                className="img-wrap"
                data-id={split_file_id[i]}
              >
                <img
                  className="img-responsive mCS_img_loaded"
                  src="../images/scan-thumbnail.png"
                  alt={get_file_ext}
                />
                <span className="go">
                  <img
                    src="../images/next-arrow-white.svg"
                    className="mCS_img_loaded"
                  />
                </span>
              </a>
              <a
                onClick={this.dataTaggingFunc.bind(this, split_file_id[i])}
                data-toggle="tooltip"
                data-placement="top"
                title="Hooray!"
                data-id={split_file_id[i]}
              >
                <span>
                  {arr_reverse[0].substring(
                    arr_reverse[0].length - 15,
                    arr_reverse[0].length
                  )}
                </span>
                <img
                  src="../images/download-icon.svg"
                  alt="Icon"
                  className="mCS_img_loaded"
                />
              </a>
            </div>
          );
        }
      }
    }

    options.push(<option>ORG-250</option>);

    return (
      <div>
        <div className="container-fluid">
          <div className="row">
            <LeftSidebar />

            <div className="main-wrap col-md-12 col-xs-12 pad-r-no">
              <div className="top-bar col-md-12 col-xs-12 pad-r-no">
                <div className="nav-brand-res visible-xs">
                  <img
                    className="img-responsive"
                    src="../images/logo-icon.png"
                    alt="LogoIcon"
                  />
                </div>
                <a href="javascript:;" class="back hidden-xs">
                  <svg width="18.5" height="14.249" viewBox="0 0 18.5 14.249">
                    <g
                      id="left-arrow_2_"
                      data-name="left-arrow (2)"
                      transform="translate(0 -58.83)"
                    >
                      <g
                        id="Group_25"
                        data-name="Group 25"
                        transform="translate(0 65.207)"
                      >
                        <g
                          id="Group_24"
                          data-name="Group 24"
                          transform="translate(0 0)"
                        >
                          <path
                            id="Path_19"
                            data-name="Path 19"
                            d="M17.753,235.318H.747a.747.747,0,0,0,0,1.495H17.753a.747.747,0,0,0,0-1.495Z"
                            transform="translate(0 -235.318)"
                          ></path>
                        </g>
                      </g>
                      <g
                        id="Group_27"
                        data-name="Group 27"
                        transform="translate(0 58.83)"
                      >
                        <g
                          id="Group_26"
                          data-name="Group 26"
                          transform="translate(0 0)"
                        >
                          <path
                            id="Path_20"
                            data-name="Path 20"
                            d="M1.8,65.954l5.849-5.849A.747.747,0,1,0,6.6,59.049L.219,65.426a.747.747,0,0,0,0,1.057L6.6,72.86A.747.747,0,1,0,7.653,71.8Z"
                            transform="translate(0 -58.83)"
                          ></path>
                        </g>
                      </g>
                    </g>
                  </svg>
                </a>

                <span class="page-title hidden-xs">Compose</span>

                <Topbar logoutSubmit={e => this.logoutLink()} />
              </div>

              <div className="main-content col-md-12 col-xs-12">
                <div class="content-top col-md-12 col-xs-12 pad-no">
                  <img src="images/mail-attachment.svg" alt="icon" />
                  <span class="page-title">Send New Attachments</span>
                </div>
                <div class="content-sec col-md-12 col-xs-12 pad-no send-attachment">
                  <form
                    class="attachment-form row custom-form"
                    method="post"
                    onSubmit={this.sendUserInput.bind(this)}
                  >
                    <div class="col-md-6 col-xs-12">
                      <div class="form-group">
                        <label>Subject</label>
                        {/* <input type="text" value={this.state.value} onChange={this.handleChange} /> */}
                        <input
                          type="text"
                          name="subject"
                          class="form-control"
                          value={this.state.subject}
                          onChange={this.handleEmpidChange}
                        />
                      </div>
                      <div class="form-group">
                        <label>Message</label>
                        <textarea
                          value={this.state.message}
                          onChange={this.handlePasswordChange}
                          class="form-control"
                        ></textarea>
                      </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                      <div class="form-group">
                        <label>Upload Attachments</label>
                        <div class="upload-wizard">
                          <div class="upload-thumb"                         >
                            <ul class="list-inline">
                              {/* <li>
                                <div class="thumb-img">
                                  <img
                                    src="images/file-thumbnail-2.jpg"
                                    alt="image"
                                  />
                                </div>
                                <span class="file-name">bill-payment.pdf</span>
                                <a href="javascript:;" class="del-btn">
                                  <img
                                    src="images/close-icon-white.svg"
                                    alt="delete"
                                  />
                                </a>
                              </li> */}
                              {/* <li>
                                <div class="thumb-img">
                                  <img
                                    src="images/file-thumbnail-1.jpg"
                                    alt="image"
                                  />
                                </div>
                                <span class="file-name">taxi-receipt.pdf</span>
                                <a href="javascript:;" class="del-btn">
                                  <img
                                    src="images/close-icon-white.svg"
                                    alt="delete"
                                  />
                                </a>
                              </li> */}
                              {/* <li>
                                <div class="thumb-img">
                                  <img
                                    src="images/file-thumbnail-2.jpg"
                                    alt="image"
                                  />
                                </div>
                                <span class="file-name">
                                  stationary-bill.pdf
                                </span>
                                <a href="javascript:;" class="del-btn">
                                  <img
                                    src="images/close-icon-white.svg"
                                    alt="delete"
                                  />
                                </a>
                              </li> */}
                              <li>
                                <a
                                  href="javascript:;"
                                  class="add-more"
                                  primary={false}
                                  onClick={() => {
                                    this.upload.click();
                                  }}
                                >
                                  <div >
                                  <input
                                      id="myInput"
                                      type="file"
                                      accept=".pdf,.png"
                                      ref={ref => (this.upload = ref)}
                                      style={{ display: "none" }}
                                      onChange={this.onChangeFile.bind(this)}
                                      multiple
                                    />
                                    <label htmlFor="file">{file}</label>

                                    {this.state.fileSelected ? (

                                      this.state.ext=== 'pdf' ?
                                      <PDFtoIMG file={this.state.fileSelected}>
                                        {({ pages }) => {
                                          if (!pages.length)
                                            return "Loading...";
                                          return pages.map((page, index) => (
                                            (index==0)?
                                            <img   key={index} src={page} style={{height:104.9,width:105.9,marginTop: 13,}}  />
                                            
                                            :""
                                          ));
                                        }}
                                      </PDFtoIMG>: 

                                       <img src={this.state.fileSelected} style={{height:104.9,width:105.9,marginTop: 13,}} />


                                    ) : (
                                      <div>Add more</div>
                                    )}
                                    <span class="file-name">
{fileName}                                          </span>
                              
                                  </div>
                                  
                                </a>
                              </li>
                            </ul>
                          </div>
                          <div class="upload-space hide">
                            <div>
                              <img
                                class="icon"
                                src="images/upload-icon.svg"
                                alt="icon"
                              />
                              <span>.pdf .png .jpg</span>
                              <span class="note-txt">
                                Drag and drop your files here
                                <br />
                                or
                                <br />
                                <a href="javascript:;">click here</a>
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12 col-xs-12 text-right ">
                      <button class="btn btn-lightgray">Cancel</button>
                      {"   "}

                      <button class="btn btn-yellow">Save Draft</button>
                      {"   "}

                      <button type="submit" class="btn btn-green">
                        Send
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <Footer logoutSubmit={e => this.logoutLink(e)} />
        </div>
      </div>
    );
  }
}
export default compose;