import React from 'react';
import jQuery from 'jquery';

class topbar extends React.Component {

    constructor(props) {
        super(props);
        //const { history } = this.props;
        this.state = { logged_user_id: localStorage.getItem("logged_user_id"), logged_client_id: localStorage.getItem("logged_client_id"), logged_role_id: localStorage.getItem("logged_role_id"), logged_user_name: localStorage.getItem("logged_user_name"), logged_user_email: localStorage.getItem("logged_user_email"), logged_user_phone: localStorage.getItem("logged_user_phone"), logged_user_image: localStorage.getItem("logged_user_image"), logged_company_name: localStorage.getItem("logged_company_name") };
    }


    logoutFunc(e){
        e.preventDefault();
        this.props.logoutSubmit();
    }

    componentDidMount(){
        jQuery(".search-btn").click(function(){
            jQuery(".hdr-search").addClass("active");
        });
        jQuery(".hdr-search .close-icon").click(function(){
            jQuery(".hdr-search").removeClass("active");
        });
    }

    render() {
       return (


            <div className="pull-right">
                <form className="hdr-search">
                    <input type="text" className="form-control" name="search" placeholder="Search..."  autoComplete='off'/>
                    {/* <button type="submit" className="btn btn-green">Search</button> */}
                    <a href="javascript:;" className="close-icon"><img src="../../images/close-icon-red.svg" alt="Close" /></a>
                </form>
            
                <div className="search-wrap">
                    <a className="search-btn">
                        <img className="search" src="../../images/search-icon.svg" alt="Search" />
                    </a>
                </div>
                <div className="notify-wrap">
                    <a href="/"><img src="../../images/notification-icon.svg" alt="Notification" /></a>
                </div>
                <div className="profile-wrap dropdown">
                    <a href="javascript:;" className="avatar dropdown-toggle" data-toggle="dropdown">
                        <span className="avatar-img"><img className="img-responsive" src="../../images/user-img-1.png" alt={this.state.logged_user_name} /></span>
                        <span className="hidden-xs">{this.state.logged_user_name}</span>
                    </a>
                    <ul className="dropdown-menu">
                        <li>
                            <a href="/"><img src="../../images/edit-icon.svg" alt="icon" />Edit Profile</a>
                        </li>
                        <li><a href="/"><img src="../../images/settings-icon.svg" alt="icon" />Settings</a></li>
                        <li><a href="/" onClick={this.logoutFunc.bind(this)}><img src="../../images/turn-off-icon.svg" alt="icon" />Logout</a></li>
                    </ul>
                </div>
            </div>

        )
    }
}
export default topbar;