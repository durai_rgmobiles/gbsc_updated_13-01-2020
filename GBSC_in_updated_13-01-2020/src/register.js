import React, { Fragment } from "react";
import jQuery from "jquery";
import DatePicker from "react-datepicker";
import FetchAllApi from "./api_links/fetch_all_api.js";

//import "react-datepicker/dist/react-datepicker.css";


export default class register extends React.Component {
  constructor(props) {
    super(props);
    //const { history } = this.props;
    this.state = { fname: "", lname: "", country_region: "", state_province: "", email_id: "", phone_no: "", password: "", cpassword: "",
    entity_name: "", entity_num: "", incorport_date: "", principle_activities: "", home_currency:"", company_phn: "", company_email: "", comp_address: "", entity_type_list:[],stateSelected: "",startDate: "",selectedDate: "", succ_msg: "", today_date: '', end_date: '', country_list: [], country_list_length: 0, state_list: [], dialing_code: ""};
  }

  UNSAFE_componentWillMount(){

    this.get_entity_list();

    FetchAllApi.get_countries((err, response) => {
      //alert(response)
      console.log("get_countries_list", response.list.length);
      if(response.status === 1){
        this.setState({
          country_list: response.list,
          country_list_length: response.list.length
        })
      } 
    });

  }

  get_entity_list(){
    FetchAllApi.get_entity_types((err, response) => {
      //alert(response)
      console.log("entity_type_list", response);
      if(response.status === 1){
        this.setState({
          entity_type_list: response.list
        })
      } else{

      }
    });
  }

  componentDidMount() {

    //GET Today Date
    var today = new Date();

    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    if(dd<10){ dd='0'+dd } 
    if(mm<10){ mm='0'+mm } 
    var today_date = yyyy+'-'+mm+'-'+dd;

    var cur_end_date = new Date();
    cur_end_date.setMonth( cur_end_date.getMonth( ) + 1 );
    var dd = cur_end_date.getDate();
    var mm = cur_end_date.getMonth()+1; //January is 0!
    var yyyy = cur_end_date.getFullYear();
    if(dd<10){ dd='0'+dd } 
    if(mm<10){ mm='0'+mm } 
    var end_date = yyyy+'-'+mm+'-'+(dd-1);
    document.getElementById("incorport_date").setAttribute("max", today_date);

    this.setState({
      today_date: today_date,
      end_date: end_date
    })

    // jQuery(".form-step1").hide();
    // jQuery(".form-step2").show();
    // jQuery("a.back").show();

    jQuery("a.back").click(function() {
      jQuery(".form-step2").hide();
      jQuery(".form-step1").show();
      jQuery(this).hide();
    });
    //    this. fnameValidate();
    var THIS = this;

    jQuery("#country_region").change(function() {
      var selectedItem = jQuery(this).val();
      var country_id = jQuery(this).find(':selected').data('id');
      var phone_code = jQuery(this).find(':selected').data('dialing-code');

      THIS.setState({ country_region: selectedItem, dialing_code: phone_code });

      FetchAllApi.get_states(country_id, (err, response) => {
        //alert(response)
        console.log("get_states_list", response.list.length);
        if(response.status === 1){
          THIS.setState({
            state_list: response.list,
          })
        } 
      });
    });

    jQuery(document).ready(function() {

      jQuery(".pass-visible").click(function() {
        jQuery(this).toggleClass("off");
        var input = jQuery(jQuery(this).attr("toggle"));
        if (input.attr("type") == "password") {
          input.attr("type", "text");
        } else {
          input.attr("type", "password");
        }
      });
      // jQuery('.datepicker').datepicker();
    });

    jQuery(".add-phone-input").click(function() {
      jQuery(
        "<div class='form-group clone'><input class='form-control comp_phone_no' type='tel' required/><a href='javascript:;' class='remove-input'><img class='img-responsive' src='images/close-icon-red.svg'/></a></div>"
      ).insertBefore(this);
    });
    jQuery(".add-email-input").click(function() {
      jQuery(
        "<div class='form-group clone'><input class='form-control comp_email_id' type='email' required/><a href='javascript:;' class='remove-input'><img class='img-responsive' src='images/close-icon-red.svg'/></a></div>"
      ).insertBefore(this);
    });

    jQuery(".form-group").on("click", ".remove-input", function() {
      jQuery(this)
        .parent(".form-group")
        .remove();
    });

  }

  changeEntType(ent_type_id, ent_type_name){
    console.log("ent_type_id", ent_type_id);
    jQuery("#entity_type").val(ent_type_id);
    jQuery(".dropdown-select .btn-value").text(ent_type_name);
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  regFunc(e){
      e.preventDefault();

      var passWrd = false;
      var formValidation_1 = false;

      var first_name = this.state.fname;
      var last_name = this.state.lname;
      var country_region = this.state.country_region;
      var state_province = this.state.state_province;
      var email_id = this.state.email_id;
      var phone_no = this.state.phone_no;
      var password = this.state.password;
      var cpassword = this.state.cpassword;

      if(password !== cpassword){
        jQuery("#password-fieldc").next(".text-red").html("Password and confirm password fields doesn't match!");
        passWrd = false;
      } else{
        jQuery("#password-fieldc").next(".text-red").html("");
        passWrd = true;
      }

      if (first_name != "" && last_name != "" && country_region != "" && state_province != "" && email_id != "" && phone_no != "" && passWrd === true) {
        jQuery(".form-step1").hide();
        jQuery(".form-step2").show();
        jQuery(".regBack").show();
        formValidation_1 = true;
      } else{
        jQuery(".form-step2").hide();
        jQuery(".form-step1").show();
        jQuery(".regBack").hide();
        formValidation_1 = false;
      }

  }

  regFunc_2(e){
      e.preventDefault();

      var passWrd = false;
      var formValidation_1 = false;
      var formValidation_2 = false;

      var first_name = this.state.fname;
      var last_name = this.state.lname;
      var country_region = this.state.country_region;
      var state_province = this.state.state_province;
      var email_id = this.state.email_id;
      var phone_no = this.state.phone_no;
      var password = this.state.password;
      var cpassword = this.state.cpassword;

      var entity_name = this.state.entity_name;
      var entity_num = this.state.entity_num;
      var incorport_date = this.state.incorport_date;
      var home_currency = this.state.home_currency;
      var principle_activities = this.state.principle_activities;
      var company_phn = this.state.company_phn;
      var company_email = this.state.company_email;
      var comp_address = this.state.comp_address;
      var entity_type = jQuery("#entity_type").val();

      let country_code = this.state.dialing_code;
      let plan_id = 0;
      let subscription_start_date = this.state.today_date;
      let subscription_end_date = this.state.end_date;

      if(password !== cpassword){
        jQuery("#password-fieldc").next(".text-red").html("Password and confirm password fields doesn't match!");
        passWrd = false;
      } else{
        jQuery("#password-fieldc").next(".text-red").html("");
        passWrd = true;
      }

      if (first_name != "" && last_name != "" && country_region != "" && state_province != "" && email_id != "" && phone_no != "" && passWrd === true) {
        jQuery(".form-step1").hide();
        jQuery(".form-step2").show();
        jQuery(".regBack").show();
        formValidation_1 = true;
      } else{
        jQuery(".form-step2").hide();
        jQuery(".form-step1").show();
        jQuery(".regBack").hide();
        formValidation_1 = false;
      }

      

      if (entity_name != "" && entity_num != "" && incorport_date != "" && home_currency != "" && principle_activities != "" && company_phn != "" && company_email !== "" && comp_address !== "" && entity_type != "") {
        formValidation_2 = true;
      } else{
        formValidation_2 = false;
      }

      //alert(formValidation_1+' '+formValidation_2);

      if(formValidation_1 === true && formValidation_2 === true){
        var company_phn_arr = [];
        var company_email_arr = [];
        
        company_phn_arr.push(this.state.company_phn);
        var comp_phn_length = jQuery(".comp_phone_no").length;
        if(comp_phn_length > 0){
          jQuery('.comp_phone_no').each(function(){
            company_phn_arr.push(this.value); 
          });
        }
        //console.log("company_phn_arr", company_phn_arr);

        company_email_arr.push(this.state.company_email);
        var comp_email_length = jQuery(".comp_email_id").length;
        if(comp_email_length > 0){
          jQuery('.comp_phone_no').each(function(){
            company_email_arr.push(this.value); 
          });
        }
        //console.log("company_email_arr", company_email_arr);

        let registerDetails = {          
          first_name: first_name,
          last_name: last_name,
          country: country_region,
          state: state_province,
          email: email_id,
          phone: phone_no,
          password: password,
          entity_name: entity_name,
          entity_number: entity_num,
          entity_type: entity_type,
          incorporation_date: incorport_date,
          home_currency: home_currency,
          principle_activities: principle_activities,
          company_phone: company_phn_arr,
          company_email: company_email_arr,
          country_code: country_code,
          address: comp_address,
          plan_id: plan_id,
          subscription_start_date: subscription_start_date,
          subscription_end_date: subscription_end_date
        };

        console.log("registerDetails", registerDetails);

        FetchAllApi.registerNewCompany(registerDetails, (err, response) => {
          //alert(response)
          //console.log("userdajjjjjjjjjjjjjjjjjjta", response);
          var THIS = this;
          if (response.status === 1) {
            this.setState({
              succ_msg: response.message
            })

            jQuery(".resp_msg").fadeIn(500);
            setTimeout(function(){ 
              jQuery(".resp_msg").fadeOut(2000); 
              THIS.props.history.push("/register_finished");             
            },8000);
            
          } else {
            this.setState({
              succ_msg: response.message
            })

            jQuery(".resp_msg").fadeIn(500);
            setTimeout(function(){ jQuery(".resp_msg").fadeOut(2000); },8000);
          }
        });
      }
  }

  createEntity(e){
    e.preventDefault();
    //alert("test");
    var create_entity = jQuery("#create_entity").val();

    if(create_entity != ""){
      FetchAllApi.add_new_entity(create_entity, (err, response) => {
        var THIS = this;
        if (response.status === 1) {
          this.setState({
            succ_msg: response.message
          })
          this.get_entity_list();
          jQuery(".resp_msg").fadeIn(500);
          setTimeout(function(){ 
            jQuery(".resp_msg").fadeOut(2000);             
          },8000);

        } else {
          this.setState({
            succ_msg: response.message
          })
          jQuery(".resp_msg").fadeIn(500);
          setTimeout(function(){ jQuery(".resp_msg").fadeOut(2000); },8000);

        }
      });
    } else{
      jQuery(".enttype_drpdwn").next(".text-red").html("Please enter entitty name!");
    }
  }

  render() {
    console.log('country_length', this.state.country_list.length);
    return (
      <div>
        <div className="container-fluid">
          <div className="row dflex">
            <div className="col-md-4 col-sm-4 register-left hidden-xs">
              <div className="nav-brand text-center">
                <img
                  className="img-responsive"
                  src="images/nav-brand-transparent.png"
                  alt="Logo"
                />
              </div>
              <h1>Genie lorem ipsum dolor</h1>
              <ul className="list-unstyled">
                <li>
                  <img src="images/ease-icon.svg" alt="icon" />
                  <span>
                    Ease of deployment of customised process solutions
                  </span>
                </li>
                <li>
                  <img src="images/performance-icon.svg" alt="icon" />
                  <span>Process performance visibilty</span>
                </li>
                <li>
                  <img src="images/increase-icon.svg" alt="icon" />
                  <span>Finance process flexibility to scale</span>
                </li>
                <li>
                  <img src="images/accuracy-icon.svg" alt="icon" />
                  <span>Data accuracy</span>
                </li>
                <li>
                  <img src="images/fulltime-icon.svg" alt="icon" />
                  <span>24/7 Operational capability</span>
                </li>
                <li>
                  <img src="images/cost-icon.svg" alt="icon" />
                  <span>Affordable cost</span>
                </li>
              </ul>
            </div>

            <div className="col-md-8 col-sm-8 register-right">
              <div className="resp_msg">{this.state.succ_msg}</div>

              <div className="register-form col-md-12 col-xs-12">
                <div className="nav-brand text-center visible-xs">
                  <img
                    className="img-responsive"
                    src="images/logo-genie.png"
                    alt="Logo"
                  />
                </div>
                <div className="register-head col-md-12 col-xs-12">
                  <a href="javascript:;" className="back regBack">
                    <img src="images/back-arrow-blue.svg" />
                  </a>
                  <span>
                    Ready to Get Started?
                    <br />
                    <small>Please fill with your details</small>
                  </span>
                </div>

                
                <div className="formstep-enclose step-1 col-md-12 col-xs-12 pad-no">

                  <form action="#" className="custom-form" onSubmit={this.regFunc.bind(this)} autoComplete="off">
                  <div className="form-step1 col-md-12 col-xs-12 pad-no">
                    
                      <div className="form-group col-md-6">
                        <label>First Name<span className="astrick">*</span></label>
                        <input type="text" name="fname" id="fname" onChange={event => this.handleChange(event)} className="form-control" required/>
                      </div>
                      <div className="form-group col-md-6">
                        <label>Last Name<span className="astrick">*</span></label>
                        <input type="text" name="lname" id="lname" onChange={event => this.handleChange(event)} className="form-control" required/>
                      </div>
                      <div className="form-group col-md-6">
                        <label>Country / Region<span className="astrick">*</span></label>
                        <select className="select-dropdown form-control" data-live-search="true" name="country_region" title="Choose..." id="country_region" required>
                          <option value="">Choose...</option>
                          {                             
                            this.state.country_list_length > 0 ? (
                              this.state.country_list.map((country_data, index) => {
                                return(
                                  <option value={country_data.name} data-id={country_data.id} data-dialing-code={country_data.phonecode}>{country_data.name}</option>
                                )
                                })
                              ) : ''
                            }
                        </select>
                      </div>
                      <div className="form-group col-md-6">
                        <label>State / Province<span className="astrick">*</span></label>
                        <select className="select-dropdown form-control" data-live-search="true" name="state_province" title="Choose..." id="state_province" onChange={event => this.handleChange(event)} required>
                          <option value="">Choose...</option>
                          {                             
                            this.state.state_list.length > 0 ? (
                              this.state.state_list.map((state_data, index) => {
                                return(
                                  <option value={state_data.name} data-id={state_data.id}>{state_data.name}</option>
                                )
                                })
                            ) : ''
                          }
                        </select>
                      </div>
                      <div className="form-group col-md-6">
                        <label>Email<span className="astrick">*</span></label>
                        <input type="email" name="email_id" id="email_id" className="form-control" onChange={event => this.handleChange(event)} required/>
                      </div>
                      <div className="form-group col-md-6">
                        <label>Phone<span className="astrick">*</span></label>
                        <input type="tel" name="phone_no" id="phone_no" className="form-control" onChange={event => this.handleChange(event)} required/>
                      </div>
                      <div className="form-group col-md-6">
                        <label>Password<span className="astrick">*</span></label>
                        <i className="pass-visible" toggle="#password-field">
                            <img className="off" src="images/visibility-off.svg" alt="hide"/>
                            <img className="on" src="images/visibility.svg" alt="show"/>
                        </i>
                        <input id="password-field" type="password" name="password" className="form-control" onChange={event => this.handleChange(event)} required/>
                      </div>
                      <div className="form-group col-md-6">
                        <label>Confirm Password<span className="astrick">*</span></label>
                        <i className="pass-visible" toggle="#password-fieldc">
                            <img className="off" src="images/visibility-off.svg" alt="hide"/>
                            <img className="on" src="images/visibility.svg" alt="show"/>
                        </i>
                        <input id="password-fieldc" type="password" name="cpassword" className="form-control" onChange={event => this.handleChange(event)} required/>
                        <small className="text-red"></small>
                      </div>

                      <div className="col-md-12 col-xs-12 text-right">
                        <button type="submit" className="btn btn-blue btn-rounded next">Next</button>
                      </div>
                  </div>
                  </form>
                  
                  <form action="#" className="custom-form" onSubmit={this.regFunc_2.bind(this)} autoComplete="off">
                    <div className="form-step2 col-md-12 col-xs-12 pad-no">

                      <div className="form-group col-md-6">
                        <label>Entity Name<span className="astrick">*</span></label>
                        <input type="text" name="entity_name" onChange={event => this.handleChange(event)} className="form-control" required/>
                      </div>
                      <div className="form-group col-md-6">
                        <label>Unique Entity Number (UEN)<span className="astrick">*</span></label>
                        <input type="text" name="entity_num" onChange={event => this.handleChange(event)} className="form-control" required/>
                      </div>
                      <div className="form-group col-md-6">
                        <label>Entity Type<span className="astrick">*</span></label>
                        <input type="hidden" name="entity_type" id="entity_type" required/>

                        <div className="dropdown custom-select-drop enttype_drpdwn">
                          <button id="dLabel" className="form-control dropdown-select" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span className="btn-value">Choose...</span>
                            <span className="bs-caret"><span className="caret"></span></span>
                          </button>
                          <ul className="dropdown-menu" aria-labelledby="dLabel">
                            <li className="create-new">
                              <input type="text" name="create_entity" id="create_entity" className="form-control" placeholder="Create New..."/>
                              <button className="btn btn-blue btn-rounded" onClick={this.createEntity.bind(this)}>
                                Create New <strong>"Loan"</strong>
                                <img src="images/btn-arrow-white.svg" alt="Icon"/>
                              </button>
                            </li>
                            {                             
                            this.state.entity_type_list.length > 0 ? (
                              this.state.entity_type_list.map((ent_type_date, index) => {
                                return(
                                  <li><a href="javascript:;" onClick={this.changeEntType.bind(this,ent_type_date.id,ent_type_date.name)}>{ent_type_date.name}</a></li>
                                )
                                })
                              ) : ''
                            }
                          </ul>
                        </div>
                        <small className="text-red"></small>
                      </div>
                      <div className="form-group col-md-6">
                        <label>Incorporation Date<span className="astrick">*</span></label>
                        <div className="input-group date">
                          <input type="date" name="incorport_date" id="incorport_date" onChange={event => this.handleChange(event)} className="form-control" required/>
                          <div className="input-group-addon"><img src="images/calendar-icon.svg" alt="icon" /></div>
                        </div>
                        <small className="text-red"></small>
                      </div>
                      <div className="form-group col-md-6">
                        <label>Home Currency<span className="astrick">*</span></label>
                        <select className="select-dropdown selectpicker" onChange={event => this.handleChange(event)} data-live-search="true" title="Choose..." id="home_currency" name="home_currency" required>
                          <option value="SGD">Singapore - SGD</option>
                          <option value="INR">India - INR</option>
                          <option value="USD">United States - USD</option>
                          <option value="AUD">Australia - AUD</option>
                          <option value="JPY">Japanese - JPY</option>
                          <option value="MXN">Mexicon - MXN</option>
                          <option value="BRL">Brazil - BRL</option>
                        </select>
                      </div>

                      <div className="form-group col-md-6">
                        <label>Principle Activities<span className="astrick">*</span></label>
                        <input type="text" name="principle_activities" onChange={event => this.handleChange(event)} className="form-control"  required/>
                      </div>
                      <div className="form-group col-md-6">
                        <label>Phone<span className="astrick">*</span></label>
                        <input type="tel" name="company_phn" onChange={event => this.handleChange(event)} className="form-control" required/>
                        <a href="javascript:;" className="add-input add-phone-input">ADD MORE</a>
                      </div>
                      <div className="form-group col-md-6">
                        <label>Email<span className="astrick">*</span></label>
                        <input type="email" name="company_email" onChange={event => this.handleChange(event)} className="form-control" required/>
                        <a href="javascript:;" className="add-input add-email-input">ADD MORE</a>
                      </div>
                      <div className="form-group col-md-12 col-xs-12">
                        <label>Address<span className="astrick">*</span></label>
                        <textarea className="form-control" cols="8" rows="5" name="comp_address" onChange={event => this.handleChange(event)} required></textarea>
                      </div>
                      <div className="col-md-12 col-xs-12 text-right">
                        <button type="submit" className="btn btn-rounded btn-blue">Submit</button>
                      </div>

                  </div>
                  </form>

                </div>
                

              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
