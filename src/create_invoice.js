import React from 'react'
import LeftSidebar from './left_sidebar.js'
import Footer from './footer.js'
import htmlToImage from 'html-to-image'
import { PDFtoIMG } from 'react-pdf-to-image'
import { Document, Page } from 'react-pdf'
// import PDFViewer from 'pdf-viewer-reactjs'

import Topbar from './topbar.js'

import FetchAllApi from './api_links/fetch_all_api.js'
import parse from 'html-react-parser'

import jQuery from 'jquery'
// import 'bootstrap';
// import 'bootstrap-select';

class invoice_template_listing extends React.Component {
  constructor (props) {
    super(props)
    //const { history } = this.props;
    this.state = {
      dataUrl: '',
      logged_user_id: localStorage.getItem('logged_user_id'),
      logged_client_id: localStorage.getItem('logged_client_id'),
      logged_role_id: localStorage.getItem('logged_role_id'),
      logged_user_name: localStorage.getItem('logged_user_name'),
      logged_user_email: localStorage.getItem('logged_user_email'),
      logged_user_phone: localStorage.getItem('logged_user_phone'),
      logged_user_image: localStorage.getItem('logged_user_image'),
      logged_company_name: localStorage.getItem('logged_company_name'),
      logged_subscription_start_date: localStorage.getItem(
        'logged_subscription_start_date'
      ),
      logged_subscription_end_date: localStorage.getItem(
        'logged_subscription_end_date'
      ),
      logged_plan_id: localStorage.getItem('logged_plan_id'),
      dropdown: '',
      inbox_list: [],
      response_stus: 0,
      response_msg: 'No data found',
      item_details: '',
      item_file_path: '',
      list_id: '',
      response: '',
      htmlcont: ''
    }
  }

  UNSAFE_componentWillMount () {
    this.get_invoice_list()
    if (
      this.state.logged_user_id === '' ||
      this.state.logged_user_id === 'null' ||
      this.state.logged_user_id === 'undefined'
    ) {
      this.props.history.push('/')
    }

    var today = new Date()
    var dd = today.getDate()
    var mm = today.getMonth() + 1 //January is 0!
    var yyyy = today.getFullYear()
    if (dd < 10) {
      dd = '0' + dd
    }
    if (mm < 10) {
      mm = '0' + mm
    }
    var today_date = yyyy + '-' + mm + '-' + dd

    console.log('logged_user_id', this.state.logged_user_id)
    console.log(
      'logged_subscription_end_date',
      this.state.logged_subscription_end_date
    )

    if (this.state.logged_subscription_end_date < today_date) {
      //this.props.history.push('/register_Payment');
    }
  }

  get_invoice_list = () => {
    let client_id = 2
    FetchAllApi.get_edit_invoice_html(client_id, (err, response) => {
      console.log('defaultcatejhwdjkjhgorylist', response)
      if (response.status === 1) {
        this.setState({
          response: response,
          htmlcont: response.list[0].html_content
        })
      } else {
        this.setState({})
      }
    })
  }

  delete_invoice_list = id => {
    let client_id = 2
    let template_id = id
    FetchAllApi.delete_invoice_template(
      client_id,
      template_id,
      (err, response) => {
        console.log('defaultcatejhwdjkjhgorylist', response)
        alert(response.message)
        if (response.status === 1) {
          this.get_invoice_list()
          this.setState({})
        } else {
          this.setState({})
        }
      }
    )
  }

  componentDidMount () {
    //jQuery(".select-picker").selectpicker();

    require('jquery-mousewheel')
    require('malihu-custom-scrollbar-plugin')

    jQuery('.item-listwrap').mCustomScrollbar({
      scrollEasing: 'linear',
      scrollInertia: 600,
      scrollbarPosition: 'outside'
    })

    jQuery('.label-enclose .label span').click(function () {
      jQuery('.label-enclose .label').removeClass('active')
      jQuery(this)
        .parent('.label-enclose .label')
        .addClass('active')
    })
    jQuery('.label-enclose .label a').click(function () {
      jQuery(this)
        .parent('.label-enclose .label')
        .removeClass('active')
    })

    jQuery(document).ready(function(){
        jQuery(".left-navmenu .has-sub").click(function(){
            jQuery(".left-navmenu li a").removeClass("active");
            jQuery(this).addClass("active");
            jQuery(".left-navmenu li a:not(.active)").siblings(".sub-menu").slideUp();
            jQuery(this).siblings(".sub-menu").slideToggle();
        });
        jQuery(".left-navmenu .sub-menu li a").click(function(){
            jQuery(".left-navmenu .sub-menu li a").removeClass("active");
            jQuery(this).addClass("active");
        });
        jQuery(".search-btn").click(function(){
            jQuery(".hdr-search").addClass("active");
        });
        jQuery(".hdr-search .close-icon").click(function(){
            jQuery(".hdr-search").removeClass("active");
        });
        // jQuery(".select-picker").selectpicker();
        jQuery(".label-enclose .label").click(function(){
            jQuery(this).toggleClass("active");
        });
        jQuery(".nav-brand-res").click(function(){
            jQuery(".left-navbar").addClass("active");
        });
        jQuery(".menu-close").click(function(){
            jQuery(".left-navbar").removeClass("active");
        });
        jQuery('.custom-select-drop .dropdown-menu a').click(function(){
            jQuery(".open.custom-select-drop .dropdown-menu li.active").removeClass("active");
            jQuery(this).parent("li").addClass("active");
            jQuery('.open #selected').text(jQuery(this).text());
        });

        // jQuery('.input-group.date').datepicker({format: "dd/mm/yyyy"});

        // jQuery(".invoice-item-table tbody").sortable({
        // appendTo: "parent",
        // helper: "clone"
        // }).disableSelection();

        jQuery('.invoice-item-table').on( 'change keyup keydown paste cut', 'textarea', function (){
            jQuery(this).height(0).height(this.scrollHeight);
        }).find( 'textarea' ).change();
        
    });

 
  }
  onDocumentLoadSuccess = () => {
    console.log('success')
  }

  logoutLink () {
    localStorage.setItem('logged_user_id', '')
    localStorage.setItem('logged_client_id', '')
    localStorage.setItem('logged_role_id', '')
    localStorage.setItem('logged_user_name', '')
    localStorage.setItem('logged_user_email', '')
    localStorage.setItem('logged_user_phone', '')
    localStorage.setItem('logged_user_image', '')
    localStorage.setItem('logged_company_name', '')

    this.props.history.push('/')
  }

  //   dataTaggingFunc(list_id,file_id) {
  //     this.props.history.push("/data_tagging/" + list_id + "/" + file_id );
  //     window.scrollTo(0, 0);
  //   }

  routedChange (parameter) {
    this.props.history.push('/' + parameter)
    window.scrollTo(0, 0)
  }

  pageLink (page_slug) {
    this.props.history.push('/' + page_slug)
  }

  render () {
    return (
      <div id='mynode'>
        <div className='container-fluid'>
          <div className='row'>
            <LeftSidebar pageSubmit={e => this.pageLink(e)} />

            <div className='main-wrap col-md-12 col-xs-12 pad-r-no'>
              <div className='top-bar col-md-12 col-xs-12 pad-r-no'>
                <div className='nav-brand-res visible-xs'>
                  <img
                    className='img-responsive'
                    src='../images/logo-icon.png'
                    alt='LogoIcon'
                  />
                </div>
                <a href="javascript:;"  onClick={this.routedChange.bind(this, 'inbox')}
                  className='back hidden-xs' class="back hidden-xs">
                    <img src="../../images/back-arrow-blue.svg"/>
                </a>

                <ul class="list-unstyled breadcrumb page-title hidden-xs">
                    <li><a href="javascript: ;" >Create Invoice</a></li>
                    <li>Accounts Receivable</li>
                </ul>
                <Topbar logoutSubmit={e => this.logoutLink()} />
                
              </div>

              <div>
          <div className="content-top col-md-12 col-xs-12">
            <form className="custom-form form-inline">
              <div className="form-group mar-rgt">
                <label>Accounts</label>
                <div className="custom-select-drop dropdown">
                  <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" className="dropdown-toggle btn" href="javascript:;">
                    <span id="selected">Accounts Receivable</span><span className="caret" />
                  </a>
                  <ul className="dropdown-menu">
                    <li className="active"><a href="javascript:;">Accounts Receivable</a></li>
                    <li><a href="javascript:;">Accounts Receivable (USD)</a></li>
                    <li><a href="javascript:;">Accounts Receivable (INR)</a></li>
                  </ul>
                </div>
              </div>
              <div className="form-group">
                <label>Templates</label>
                <div className="custom-select-drop dropdown">
                  <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" className="dropdown-toggle btn" href="javascript:;">
                    <span id="selected">Simple Invoice</span><span className="caret" />
                  </a>
                  <ul className="dropdown-menu">
                    <li className="active"><a href="javascript:;">Simple Invoice</a></li>
                    <li><a href="javascript:;">Basic Invoices</a></li>
                    <li><a href="javascript:;">Shipping Simple Invoice</a></li>
                    <li><a href="javascript:;">Shipping Bascic Invoice</a></li>
                  </ul>
                </div>
              </div>
            </form>
          </div>

        </div>
        <div className="main-content col-md-12 col-xs-12">
          <div className="content-sec col-md-12 col-xs-12 pad-no mar-t-no">
            <form className="custom-form invoice-form">
              <div className="row">
                <div className="form-group col-md-4">
                  <label>Customer Name</label>
                  <div className="custom-select-drop dropdown">
                    <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" className="dropdown-toggle btn form-control" href="javascript:;">
                      <span id="selected">Choose Customer</span><span className="caret" />
                    </a>
                    <ul className="dropdown-menu">
                      <li className="active"><a href="javascript:;">Choose Customer</a></li>
                      <li><a href="javascript:;">Terry Gutierrez</a></li>
                      <li><a href="javascript:;">Sherry Fields</a></li>
                      <li><a href="javascript:;">Jessica Walker</a></li>
                    </ul>
                  </div>
                </div>
                <div className="form-group col-md-4">
                  <label>Date</label>
                  <div className="input-group date mar-t-no" data-date-format="dd/mm/yyyy">
                    <input type="text" className="form-control" />
                    <div className="input-group-addon">
                      <img src="images/calendar-icon.svg" alt="icon" />
                    </div>
                  </div>
                </div>
                <div className="form-group col-md-4">
                  <label>Invoice No</label>
                  <input type="text" name="inv-no" className="form-control" />
                </div>
                <div className="form-group col-md-4">
                  <label>Job Name</label>
                  <textarea className="form-control" defaultValue={""} />
                </div>
                <div className="form-group col-md-4">
                  <label>Invoice to</label>
                  <textarea className="form-control" defaultValue={""} />
                </div>
                <div className="form-group col-md-4">
                  <label>Shipping to</label>
                  <textarea className="form-control" defaultValue={""} />
                </div>
                <div className="form-group col-md-12">
                  <div className="table-responsive col-md-12">
                    <table className="invoice-item-table">
                      <thead>
                        <tr>
                          <th>Item &amp; Description</th>
                          <th>Unit Price</th>
                          <th className="text-center">Quantity</th>
                          <th className="text-center">Tax</th>
                          <th className="text-center">Total</th>
                        </tr>
                      </thead>
                      <tbody className="ui-sortable">
                        <tr>
                          <td style={{width: '46%'}}>
                            <span className="drag-icon">
                              <img src="images/dots-menu.svg" alt="icon" />
                            </span>
                            <textarea className="form-control" placeholder="Enter Description" style={{height: '42px'}} defaultValue={""} />
                          </td>
                          <td>
                            <input type="text" name="unit-price" className="form-control" placeholder={'00.00'} />
                          </td>
                          <td className="text-center">
                            <input type="text" name="qty" className="form-control" placeholder={'00.00'} />
                          </td>
                          <td className="text-center">
                            <input type="text" name="tax" className="form-control" placeholder={'00.00'} />
                          </td>
                          <td className="text-center">
                            <input type="text" name="total" className="form-control" placeholder={'00.00'} />
                            <div className="action-wrap">
                              <a href="javascript:;" className="clone-row">
                                <img src="images/clone-icon.svg" alt="icon" />
                              </a>
                              <a href="javascript:;" className="del-row">
                                <img src="images/delete-icon.svg" alt="icon" />
                              </a>
                            </div>
                          </td>
                        </tr><tr style={{}}>
                          <td style={{width: '46%'}}>
                            <span className="drag-icon">
                              <img src="images/dots-menu.svg" alt="icon" />
                            </span>
                            <textarea className="form-control" placeholder="Enter Description" style={{height: '42px'}} defaultValue={""} />
                          </td>
                          <td>
                            <input type="text" name="unit-price" className="form-control" placeholder={'00.00'} />
                          </td>
                          <td className="text-center">
                            <input type="text" name="qty" className="form-control" placeholder={'00.00'} />
                          </td>
                          <td className="text-center">
                            <input type="text" name="tax" className="form-control" placeholder={'00.00'} />
                          </td>
                          <td className="text-center">
                            <input type="text" name="total" className="form-control" placeholder={'00.00'} />
                            <div className="action-wrap">
                              <a href="javascript:;" className="clone-row">
                                <img src="images/clone-icon.svg" alt="icon" />
                              </a>
                              <a href="javascript:;" className="del-row">
                                <img src="images/delete-icon.svg" alt="icon" />
                              </a>
                            </div>
                          </td>
                        </tr><tr style={{}}>
                          <td style={{width: '46%'}}>
                            <span className="drag-icon">
                              <img src="images/dots-menu.svg" alt="icon" />
                            </span>
                            <textarea className="form-control" placeholder="Enter Description" style={{height: '42px'}} defaultValue={""} />
                          </td>
                          <td>
                            <input type="text" name="unit-price" className="form-control" placeholder={'00.00'} />
                          </td>
                          <td className="text-center">
                            <input type="text" name="qty" className="form-control" placeholder={'00.00'} />
                          </td>
                          <td className="text-center">
                            <input type="text" name="tax" className="form-control" placeholder={'00.00'} />
                          </td>
                          <td className="text-center">
                            <input type="text" name="total" className="form-control" placeholder={'00.00'} />
                            <div className="action-wrap">
                              <a href="javascript:;" className="clone-row">
                                <img src="images/clone-icon.svg" alt="icon" />
                              </a>
                              <a href="javascript:;" className="del-row">
                                <img src="images/delete-icon.svg" alt="icon" />
                              </a>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <div className="form-group col-md-12 mar-b-no">
                      <a href="javascript:;" className="add-input">ADD ROW</a>
                    </div>
                  </div>
                </div>
                <div className="col-md-12 total-row">
                  <div className="row">
                    <div className="form-group exchange-col col-md-5 col-xs-12">
                      <label className="fw-sbold">Exchange Rate 1 USD</label>
                      <div>
                        <input type="text" name="exchangeRate" className="form-control" defaultValue="1.38" />
                        <span className="label">SGD</span>
                      </div>
                    </div>
                    <div className="form-group col-md-7 col-xs-12 total-table">
                      <table className="pull-right">
                        <thead>
                          <tr><th>&nbsp;</th>
                            <th className="text-center">Foreign Currency<br />(USD)</th>
                            <th className="text-center">Home Currency<br />(SGD)</th>
                          </tr></thead>
                        <tbody>
                          <tr>
                            <td className="text-right">Sub Total</td>
                            <td className="text-center">00.00</td>
                            <td className="text-center">00.00</td>
                          </tr>
                          <tr>
                            <td className="text-right">Tax</td>
                            <td className="text-center">00.00</td>
                            <td className="text-center">00.00</td>
                          </tr>
                          <tr>
                            <td className="text-right">Grand Total</td>
                            <td className="text-center">00.00</td>
                            <td className="text-center">00.00</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div className="form-group col-md-4">
                  <label>Memo</label>
                  <textarea className="form-control" defaultValue={""} />
                </div>
                <div className="form-group col-md-4">
                  <label>Thanking Message</label>
                  <textarea className="form-control" defaultValue={""} />
                </div>
                <div className="form-group col-md-4">
                  <label>Terms &amp; Conditions</label>
                  <textarea className="form-control" defaultValue={""} />
                </div>
                <div className="pf-btm-wrap">
                  <div className="col-md-6 col-sm-6 col-xs-12 pad-no">
                    <button className="btn btn-empty ico">
                      <img src="images/print-icon.svg" alt="icon" />
                      Print
                    </button>
                    <button className="btn btn-empty ico">
                      <img src="images/pdf-icon.svg" alt="icon" />
                      Save as PDF
                    </button>
                  </div>
                  <div className="col-md-6 col-sm-6 col-xs-12 text-right pad-no">
                    <button className="btn btn-lightgray">Close</button>
                    <button className="btn btn-yellow">Save &amp; New</button>
                    <button className="btn btn-green">Save</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>


     
            </div>
          </div>
        </div>

        <Footer logoutSubmit={e => this.logoutLink(e)} />
      </div>
    )
  }
}
export default invoice_template_listing


  
       

