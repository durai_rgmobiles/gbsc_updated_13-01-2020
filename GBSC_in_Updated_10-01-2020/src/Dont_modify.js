import React from "react";
import LeftSidebar from "./left_sidebar.js";
import Footer from "./footer.js";

import Topbar from "./topbar.js";

import FetchAllApi from "./api_links/fetch_all_api.js";

import jQuery from "jquery";
import DatePicker from 'react-date-picker';
//npm i react-date-picker

import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";

class data_tagging extends React.Component {
  constructor(props) {
    super(props);
    //const { history } = this.props;
    this.state = {
      tablerows: [],isChecked: false,logged_user_id: localStorage.getItem("logged_user_id"),
      logged_client_id: localStorage.getItem("logged_client_id"), logged_role_id: localStorage.getItem("logged_role_id"), logged_user_name: localStorage.getItem("logged_user_name"), logged_user_email: localStorage.getItem("logged_user_email"), logged_user_phone: localStorage.getItem("logged_user_phone"),
      logged_user_image: localStorage.getItem("logged_user_image"),
      logged_company_name: localStorage.getItem("logged_company_name"),get_file_path: [],add_cmnt_msg: "",
      file_comments: [],list_id: this.props.match.params.list_id, is_called: false,sub_comments: [],checkSubComments: false,
      clickedParentId: null,attachment_file: [],attachment_file_jquery: [],attachment_file_length: 0, attachment_fileName: [],currencies: [],item_total_home_currency: "", tax_amount_foreign_currency: "", grand_total_foreign_currency: "",grand_total_home_currency: "",item_total_foreign_currency: "",
      ToCurrency: "USD",company_name: "", invoice_no: "", incorport_date: '2019-04-12',address: "",account_category: "",exchange_value: "",
      rows: ["row 1"],myarray: [],isTax: true,default_category_list:[],selected:"",selectedindex:"",balancesheetlist:[],balance_list_selected:"",changeme:'',    date: new Date(),
    };


    this.loadFile = this.loadFile.bind(this);
  }
  onChange = date => this.setState({ date })
  componentDidMount() {  

    


    jQuery(document).ready(function() {
      jQuery(".has-sub").click(function() {
        jQuery(this)
          .parent()
          .addClass("active")
          .next(".sub-menu")
          .slideToggle();
      });
      jQuery(".search-btn").click(function() {
        jQuery(".hdr-search").addClass("active");
      });
      jQuery(".hdr-search .close-icon").click(function() {
        jQuery(".hdr-search").removeClass("active");
      });
      // jQuery(".select-picker").selectpicker();
      jQuery(".label-enclose .label").click(function() {
        jQuery(this).toggleClass("active");
      });
      jQuery(".nav-brand-res").click(function() {
        jQuery(".left-navbar").addClass("active");
      });
      jQuery(".menu-close").click(function() {
        jQuery(".left-navbar").removeClass("active");
      });
      jQuery(".custom-select-drop .dropdown-menu a").click(function() {
        // alert()
        jQuery(".open.custom-select-drop .dropdown-menu li.active").removeClass(
          "active"
        );
        jQuery(this)
          .parent("li")
          .addClass("active");
        jQuery(".open #selected").text(jQuery(this).text());
      });

      jQuery('body').on('click', function() {
          if(jQuery('.custom-select-drop.dropdown').hasClass('open')) {
            jQuery('.form-table').removeClass("ovrFlwRmve");
          } else{
            jQuery('.form-table').addClass("ovrFlwRmve");
          }
      });
      
    });
    
  }

  addRow = () => {
    
    var rows = this.state.rows;
    rows.push("new row");
    this.setState({ rows: rows });
  };
  UNSAFE_componentWillMount() {
    FetchAllApi.defaultcategorylist((err, response) => {
      console.log("defaultcategorylist", response);
      if (response.status === 1) {
        this.setState({
          default_category_list: response.list
        })
      } else {

      }
    });

    FetchAllApi.balancesheetlist((err, response) => {
      console.log("defaultcategorylist", response);
      if (response.status === 1) {
        this.setState({
          balancesheetlist: response.list
        })
      } else {

      }
    });
    
    fetch("https://api.exchangerate-api.com/v4/latest/SGD")
      .then(response => response.json())
      .then(data => {
        const currencyAr = [];
        let first = data.rates;
        for (const key in first) {
          currencyAr.push(key);
        }

        this.setState({ currencies: currencyAr });
      });

    //console.log("logged_user_id", this.state.logged_user_id);
    jQuery("title").html("Data Tagging | GBSC");

    if (
      this.state.logged_user_id === "" ||
      this.state.logged_user_id === "null" ||
      this.state.logged_user_id === "undefined"
    ) {
      this.props.history.push("/");
    }

    var file_id = this.props.match.params.file_id;

    FetchAllApi.getFilePath(file_id, (err, response) => {
      //console.log('\Article Data', response);
      if (response.status === 1) {
        this.setState({
          get_file_path: response.file_path
        });
      } else {
      }
    });

    this.getCommments(file_id);
  }

  getCommments(file_id) {
    FetchAllApi.getFileCmnts(file_id, (err, response) => {
      //console.log('\Article Data', response);
      if (response.status === 1) {
        this.setState({
          file_comments: response.details
        });
      } else {
      }
    });
  }

  handleClick(e, data) {
    console.log(data.foo);
  }

  loadFile(e) {
    var files = e.target.files;
    this.setState({ attachment_file_length: files.length });

    if (files.length > 0) {
      var fileArra = this.state.attachment_file;
      //var fileThumbArra = this.state.imgThumb;

      for (var i = 0; i < files.length; i++) {
        fileArra.push(e.target.files[i]);
        this.setState({
          //   selectedFile:URL.createObjectURL(e.target.files[i]),
          attachment_file: fileArra
        });
      }
    }

    console.log("attachments_length", this.state.attachment_file.length);
  }
  

  addCommentFunc(e) {
    e.preventDefault();
    var pstCommnt = jQuery("#comment_text").val();
    var user_id = parseInt(this.state.logged_user_id);
    var list_id = parseInt(this.props.match.params.list_id);
    var file_id = parseInt(this.props.match.params.file_id);
    var parent_comment_id = 0;
    var attachments = "";

    FetchAllApi.addComment(
      pstCommnt,
      user_id,
      list_id,
      file_id,
      attachments,
      parent_comment_id,
      (err, response) => {
        console.log("add scomment", response.status);
        if (response.status === 1) {
          // alert('success');
          // window.location.reload();
          this.setState({
            add_cmnt_msg: response.message
          });
          jQuery(".comment-sec")[0].reset();

          jQuery(".resp_msg").fadeIn(2000);
          setTimeout(function() {
            jQuery(".resp_msg").fadeOut(2000);
          }, 8000);

          this.getCommments(file_id);
        } else {
          this.setState({
            add_cmnt_msg: response.message
          });
          jQuery(".resp_msg").fadeIn(2000);
          setTimeout(function() {
            jQuery(".resp_msg").fadeOut(2000);
          }, 8000);
        }
      }
    );
  }

  addSubCommentFunc(e) {
    e.preventDefault();
    var pstCommnt = e.target.elements.reply_txt.value;
    var user_id = parseInt(this.state.logged_user_id);
    var list_id = parseInt(this.props.match.params.list_id);
    var file_id = parseInt(this.props.match.params.file_id);
    var parent_comment_id = e.target.elements.parent_comment_id.value;
    if (this.state.attachment_file.length > 0) {
      var attachments = this.state.attachment_file;
    } else {
      var attachments = "";
    }

    this.addSubComment(
      pstCommnt,
      user_id,
      list_id,
      file_id,
      attachments,
      parent_comment_id
    );
  }

  addSubComment(
    pstCommnt,
    user_id,
    list_id,
    file_id,
    attachments,
    parent_comment_id
  ) {
    console.log("attachments", attachments);

    FetchAllApi.addComment(
      pstCommnt,
      user_id,
      list_id,
      file_id,
      attachments,
      parent_comment_id,
      (err, response) => {
        console.log("add comment", response.status);
        if (response.status === 1) {
          this.setState({
            add_cmnt_msg: response.message,
            attachment_file: []
          });
          jQuery(".reply_txt_cls").val("");
          jQuery(".add_img").val("");
          jQuery(".files_txt").html("");

          jQuery(".resp_msg").fadeIn(2000);
          setTimeout(function() {
            jQuery(".resp_msg").fadeOut(2000);
          }, 8000);

          this.getCommments(file_id);
          this.replyLink(parent_comment_id, "replyBtn");
        } else {
          this.setState({
            add_cmnt_msg: response.message
          });
          jQuery(".resp_msg").fadeIn(2000);
          setTimeout(function() {
            jQuery(".resp_msg").fadeOut(2000);
          }, 8000);
        }
      }
    );
  }

  dataTaggingFunc(list_id, file_id) {
    this.props.history.push("/data_tagging/" + list_id + "/" + file_id);
    window.scrollTo(0, 0);
  }

  logoutLink() {
    localStorage.setItem("logged_user_id", "");
    localStorage.setItem("logged_client_id", "");
    localStorage.setItem("logged_role_id", "");
    localStorage.setItem("logged_user_name", "");
    localStorage.setItem("logged_user_email", "");
    localStorage.setItem("logged_user_phone", "");
    localStorage.setItem("logged_user_image", "");
    localStorage.setItem("logged_company_name", "");

    this.props.history.push("/");
  }

  routedChange(parameter) {
    this.props.history.push("/" + parameter);
    window.scrollTo(0, 0);
  }

  pageLink(page_slug) {
    this.props.history.push("/" + page_slug);
  }

  updateCmmnt(e) {
    e.preventDefault();
    var comment_id = e.target.elements.comment_id.value;
    var comment_text = e.target.elements.comment_text.value;
    var user_id = this.state.logged_user_id;
    var file_id = this.props.match.params.file_id;
    var parent_comment_id = e.target.elements.parent_comment_id.value;

    this.updateCommentApi(
      comment_id,
      comment_text,
      user_id,
      file_id,
      parent_comment_id
    );
  }

  updateCommentApi(
    comment_id,
    comment_text,
    user_id,
    file_id,
    parent_comment_id
  ) {
    FetchAllApi.updateComment(
      comment_id,
      comment_text,
      user_id,
      (err, response) => {
        console.log("update comment", response.status);
        if (response.status === 1) {
          this.setState({
            add_cmnt_msg: response.message
          });

          jQuery(".resp_msg").fadeIn(2000);
          setTimeout(function() {
            jQuery(".resp_msg").fadeOut(2000);
          }, 8000);

          jQuery(".comment-txt").removeClass("hide");
          jQuery(".update_cmnt").addClass("hide");

          this.getCommments(this.props.match.params.file_id);
          // if(parseInt(parent_comment_id) > 0){
          //     this.replyFunc(parent_comment_id);
          // }
          this.replyLink(parent_comment_id, "replyUpdt");
        } else {
          this.setState({
            add_cmnt_msg: response.message
          });
          jQuery(".resp_msg").fadeIn(2000);
          setTimeout(function() {
            jQuery(".resp_msg").fadeOut(2000);
          }, 8000);
        }
      }
    );
  }

  deleteComment(comment_id, list_id, parent_comment_id) {
    FetchAllApi.deleteComment(comment_id, list_id, (err, response) => {
      console.log("delete comment", response.status);
      if (response.status === 1) {
        this.setState({
          add_cmnt_msg: response.message
        });

        //jQuery(".comment-txt").removeClass('hide');
        //jQuery(".reply-form").addClass('hide');

        jQuery(".resp_msg").fadeIn(2000);
        setTimeout(function() {
          jQuery(".resp_msg").fadeOut(2000);
        }, 8000);

        //this.getCommments(this.props.match.params.file_id);
        if (parseInt(parent_comment_id) > 0) {
          console.log("parent_comment_id123", parent_comment_id);
          this.replyLink(parseInt(parent_comment_id), "replyDel");
        } else {
          this.getCommments(this.props.match.params.file_id);
        }
      } else {
        this.setState({
          add_cmnt_msg: response.message
        });
        jQuery(".resp_msg").fadeIn(2000);
        setTimeout(function() {
          jQuery(".resp_msg").fadeOut(2000);
        }, 8000);
      }
    });
  }

  addNewRole = inputfromuser => {
    let input = inputfromuser;
    alert(input);
    this.setState({
      newrole: input
    });
  };
  componentDidUpdate() {
    var THIS = this;
    jQuery(".edit_cmnt").click(function() {
      var text_cmnt = jQuery(this)
        .closest(".prnt_cmnt")
        .next(".comment-txt")
        .text();
      var this_cmnt_id = jQuery(this).attr("data-comment-id");
      jQuery(this)
        .closest(".prnt_cmnt")
        .next(".comment-txt")
        .addClass("hide");
      jQuery(this)
        .closest(".prnt_cmnt")
        .next(".comment-txt")
        .next(".reply-form")
        .removeClass("hide");
      jQuery("#cmmnt_txt_id" + this_cmnt_id).val(text_cmnt);
    });

    jQuery(".cancel_button").click(function() {
      jQuery(this)
        .closest(".reply-form")
        .addClass("hide");
      jQuery(this)
        .closest(".reply-form")
        .prev(".comment-txt")
        .removeClass("hide");
    });
  }

  replyLink(parent_comment_id, replyToggele) {
    var THIS = this;
    //alert(replyToggele+' '+typeof(parent_comment_id));
    if (replyToggele === "replyLink") {
      jQuery("#reply_cnt" + parent_comment_id).toggleClass("in");
    }

    console.log("parent_comment_id", parent_comment_id);

    FetchAllApi.getSubCmmnts(parent_comment_id, (err, response) => {
      //console.log('\Article Data', response);
      if (response.status === 1) {
        this.setState({
          sub_comments: response.details
        });

        var comments_length = response.details.length;
        var comment_list = [];
        if (comments_length > 0) {
          for (var i = 0; i < comments_length; i++) {
            if (response.details[i].user_image) {
              var user_img = response.details[i].user_image;
            } else {
              var user_img = "../../images/user-img-1.png";
            }

            var comment_id = response.details[i].comment_id;

            if (response.details[i].sub_comment_count > 0) {
              var replyTxt =
                "Replies (" + response.details[i].sub_comment_count + ")";
            } else {
              var replyTxt = "Reply";
            }
            var dis_file_path = "";
            var get_file_path = response.details[i].file_path;
            console.log("get_file_path_" + i, response.details[i].file_path);
            if (get_file_path.length > 0) {
              var dis_file_path = [];
              var attach_file_path = [];
              var split_file_path = get_file_path.toString().split(",");

              var split_file_id = response.details[i].attachments
                .toString()
                .split(",");

              if (get_file_path.length > 1) {
                for (var j = 0; j < get_file_path.length; j++) {
                  var get_file_name = split_file_path[j];
                  var split_file_name = split_file_path[j]
                    .toString()
                    .split("/");
                  var arr_reverse = split_file_name.reverse();

                  var get_file_ext = arr_reverse[0].substring(
                    arr_reverse[0].lastIndexOf(".") + 1,
                    arr_reverse[0].length
                  );
                  if (get_file_ext === "pdf") {
                    var file_icon = "../../images/pdf-icon.png";
                  } else {
                    var file_icon = "../../images/img-icon.png";
                  }

                  dis_file_path.push(
                    '<a onClick="dataTaggingFunc(' + this,
                    this.state.list_id,
                    split_file_id[j] +
                      ')"><img src="' +
                      file_icon +
                      '" alt="' +
                      get_file_ext +
                      '" /><span>' +
                      arr_reverse[0].substring(
                        arr_reverse[0].length - 15,
                        arr_reverse[0].length
                      ) +
                      "</span></a>"
                  );
                }
              } else {
                var split_file_name = response.details[i].file_path[0]
                  .toString()
                  .split("/");
                var arr_reverse = split_file_name.reverse();

                var get_file_ext = arr_reverse[0].substring(
                  arr_reverse[0].lastIndexOf(".") + 1,
                  arr_reverse[0].length
                );
                if (get_file_ext === "pdf") {
                  var file_icon = "../../images/pdf-icon.png";
                } else {
                  var file_icon = "../../images/img-icon.png";
                }
                dis_file_path.push(
                  '<a onClick="dataTaggingFunc(' + this,
                  this.state.list_id,
                  split_file_id[0] +
                    ')"><img src="' +
                    file_icon +
                    '" alt="' +
                    get_file_ext +
                    '" /><span>' +
                    arr_reverse[0].substring(
                      arr_reverse[0].length - 15,
                      arr_reverse[0].length
                    ) +
                    "</span></a>"
                );
              }
            }

            var replyFrm =
              '<div className="reply_cnt collapse" id=reply_cnt' +
              comment_id +
              '><form action="javascript:;" className="col-md-12 col-xs-12 pad-no reply-form reply-frm"><input type="hidden" className="parent_comment_id" name="parent_comment_id" value="' +
              comment_id +
              '" /><textarea className="col-md-12 col-xs-12 reply_txt_cls" name="reply_txt" placeholder="Reply..." required></textarea><div className="pull-right"><div className="files_txt"></div><a href="javascript:;" className=" btn btn-empty"><input type="file" name="imgInp[]" id="imgInp2" className="add_img replyAttach" id="upload_file_' +
              comment_id +
              '" multiple accept="image/*,application/pdf" /><img src="../../images/attach-icon.svg" alt="icon"/></a><button type="submit" className="btn btn-green replySubmit"><img src="../../images/reply-icon.svg" alt="icon"/>Reply</button></div></form><div className="subCmnt"></div></div>';

            comment_list.push(
              '<div className="reply_cmment_row col-md-12 col-xs-12 pad-no" id="' +
                response.details[i].comment_id +
                '"><div className="reply-cont col-md-12 col-xs-12"><div className="col-md-12 col-xs-12 pad-no"><div className="avatar-img"><img className="img-responsive" src="' +
                user_img +
                '" alt="AvatarIMG"/></div><div className="reply-user"><span className="col-md-12 col-xs-12 pad-no user-name">' +
                response.details[i].comment_user +
                '</span><span className="col-md-12 col-xs-12 pad-no date">' +
                response.details[i].ago_value +
                '</span></div></div><div className="dropdown menu-item prnt_cmnt sub_cmnt"><a href="javascript" className="dropdown-toggle" data-toggle="dropdown"><img src="../../images/menu-dot.svg" alt="icon"/></a><ul className="dropdown-menu"><li><a href="javascript:;" data-comment-id="' +
                response.details[i].comment_id +
                '" className="edit_cmnt">Edit</a></li><li><a href="javascript:;" data-comment-id="' +
                response.details[i].comment_id +
                '" data-parent-id="' +
                parent_comment_id +
                '" className="del_cmnt">Delete</a></li></ul></div><p className="col-md-12 col-xs-12 pad-no comment-txt">' +
                response.details[i].comment_text +
                '</p><form action="javascript:;" className="col-md-12 col-xs-12 pad-no reply-form update_cmnt hide"><input type="hidden" name="comment_id" className="comment_id" value="' +
                response.details[i].comment_id +
                '" /><input type="hidden" name="parent_comment_id" className="parent_comment_id" value="' +
                response.details[i].comment_id +
                '" /><textarea className="col-md-12 col-xs-12 cmnt_txt" id="cmmnt_txt_id' +
                response.details[i].comment_id +
                '" name="comment_text" placeholder="" required></textarea><div className="pull-right"><button type="button" className="btn btn-default cancel_button">Cancel</button><button type="submit" className="btn btn-green updateCmnt">Update</button></div></form><div className="attachment-item col-md-12 col-xs-12 pad-no">' +
                dis_file_path +
                '</div></div><div className="col-md-12 col-xs-12 pad-no"><button className="btn btn-lightgray">Resolved</button><a href="javascript:;" className="reply-link" data-comment-id="' +
                response.details[i].comment_id +
                '" onclick="return replyLink(' +
                response.details[i].comment_id +
                ');">' +
                replyTxt +
                "</a></div>" +
                replyFrm +
                "</div>"
            );
          }

          jQuery("#reply_cnt" + parent_comment_id)
            .children(".subCmnt")
            .html(comment_list);

          jQuery(".edit_cmnt").click(function() {
            var text_cmnt = jQuery(this)
              .closest(".prnt_cmnt")
              .next(".comment-txt")
              .text();
            var this_cmnt_id = jQuery(this).attr("data-comment-id");
            jQuery(this)
              .closest(".prnt_cmnt")
              .next(".comment-txt")
              .addClass("hide");
            jQuery(this)
              .closest(".prnt_cmnt")
              .next(".comment-txt")
              .next(".reply-form")
              .removeClass("hide");
            jQuery("#cmmnt_txt_id" + this_cmnt_id).val(text_cmnt);
          });

          jQuery(".cancel_button").click(function() {
            jQuery(this)
              .closest(".reply-form")
              .addClass("hide");
            jQuery(this)
              .closest(".reply-form")
              .prev(".comment-txt")
              .removeClass("hide");
          });

          jQuery(".updateCmnt").click(function(e) {
            e.preventDefault();
            var parent_comment_id = jQuery(this)
              .closest(".reply-form")
              .children(".parent_comment_id")
              .val();
            var comment_text = jQuery(this)
              .closest(".pull-right")
              .prev(".cmnt_txt")
              .val();
            var user_id = THIS.state.logged_user_id;
            var file_id = THIS.props.match.params.file_id;

            THIS.updateCommentApi(
              comment_id,
              comment_text,
              user_id,
              file_id,
              parent_comment_id
            );
            jQuery(this)
              .closest(".reply-form")
              .prev(".comment-txt")
              .text(comment_text);
          });

          jQuery(".del_cmnt").click(function(e) {
            e.preventDefault();
            var comment_id = jQuery(this).attr("data-comment-id");
            var parent_comment_id = jQuery(this).attr("data-parent-id");
            var user_id = parseInt(THIS.state.logged_user_id);
            var list_id = parseInt(THIS.props.match.params.list_id);
            var file_id = parseInt(THIS.props.match.params.file_id);

            THIS.deleteComment(comment_id, list_id, parent_comment_id);
          });

          jQuery(".replySubmit").click(function(e) {
            e.preventDefault();

            var pstCommnt = jQuery(this)
              .closest(".pull-right")
              .prev(".reply_txt_cls")
              .val();
            var user_id = parseInt(THIS.state.logged_user_id);
            var list_id = parseInt(THIS.props.match.params.list_id);
            var file_id = parseInt(THIS.props.match.params.file_id);
            var parent_comment_id = jQuery(this)
              .closest(".reply-form")
              .children(".parent_comment_id")
              .val();
            if (THIS.state.attachment_file_jquery.length > 0) {
              var attachments = THIS.state.attachment_file_jquery;
            } else {
              var attachments = "";
            }

            console.log("jquery_attachments", attachments);

            THIS.addSubComment(
              pstCommnt,
              user_id,
              list_id,
              file_id,
              attachments,
              parent_comment_id
            );
          });

          jQuery(".replyAttach").on("change", function(e) {
            var files = e.target.files;
            THIS.setState({ attachment_file_length: files.length });

            if (files.length > 0) {
              var fileArra = THIS.state.attachment_file_jquery;
              //var fileThumbArra = this.state.imgThumb;

              for (var i = 0; i < files.length; i++) {
                fileArra.push(e.target.files[i]);
                THIS.setState({
                  //   selectedFile:URL.createObjectURL(e.target.files[i]),
                  attachment_file_jquery: fileArra
                });
              }
            }

            jQuery(this)
              .closest(".pull-right")
              .children(".files_txt")
              .html("2 files are attached");

            console.log(
              "attachments_length",
              THIS.state.attachment_file_jquery.length
            );
            console.log("attachment_file", THIS.state.attachment_file_jquery);
          });
        }
      } else {
        jQuery("#reply_cnt" + parent_comment_id)
          .children(".subCmnt")
          .html("");
      }
    });
  }
  
  selectHandlerBalancelist = event => {
    event.preventDefault();
    let balance_list_selected = event.target.value;
    this.setState({ balance_list_selected: event.target.value });
    // this.convertHandler(ToCurrency);
  };
  selectHandler = event => {
    event.preventDefault();
    let ToCurrency = event.target.value;
    this.setState({ ToCurrency: event.target.value });
    // this.convertHandler(ToCurrency);
  };

  saveAndContinue = () => {
    let items = {
      client_id: 1,
      item_total_foreign_currency: this.state.item_total_home_currency,
      tax_amount_home_currency: this.state.tax_amount_home_currency,
      grand_total_home_currency: this.state.grand_total_home_currency,
      item_total_home_currency: this.state.item_total_foreign_currency,
      tax_amount_foreign_currency: this.state.tax_amount_foreign_currency,
      grand_total_foreign_currency: this.state.grand_total_foreign_currency,
      currency: this.state.ToCurrency,
      exchange_rate: this.state.exchange_value,
      type: 1,
      list_id: 102,
      tagged_user_id: 33,
      invoice_date: this.state.incorport_date,
      company_name: this.state.company_name,
      // invoice_no: this.state.invoice_no,
      invoice_number: "0004",
      company_address: this.state.address,
      incorport_date: this.state.date,
      account_category: this.state.account_category,
      item_list: this.state.myarray
    };
    console.log("nike", items);
    FetchAllApi.saveAndContinue(items, (err, response) => {
      console.log("add comment", response.status);
      if (response.status === 1) {
       // alert(response.message);
      //  alert(response.message)
        this.setState({
          add_cmnt_msg: response.message,
          


        });
        

        jQuery(".resp_msg").fadeIn(2000);
        setTimeout(function() {
          jQuery(".resp_msg").fadeOut(2000);
        }, 8000);
        this.callme();
      } else {


        this.setState({
        //  add_cmnt_msg:
        });

        // jQuery(".resp_msg").fadeIn(2);
        // setTimeout(function() {
        //   jQuery(".resp_msg").fadeOut(20);
        // }, 8000);
      }
    });
  };
 

  callme=()=> {
    // alert('success')
   
        window.location.reload();
        alert('success')
  }
 
  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
    // alert(event.target.value)
  }
  handleChangeItems(e, itemid) {
    var gateway=e

    // console.log('findmine',e.currentTarget.dataset.id)
    var result = [];
    var itemprice = [];
    

    for (let i = itemid; i >= 0; i--) {
      let item_name = jQuery("#item" + i).val()!="" ? jQuery("#item" + i).val():0;
      let quantity = jQuery("#quantity" + i).val() !=""?jQuery("#quantity" + i).val():0;
      let unit_price = jQuery("#unit_price" + i).val() !=""?jQuery("#unit_price" + i).val():0;
      let price = quantity*unit_price;
      let tyryt = jQuery("#selectVehicle" + i).val() !=""?jQuery("#selectVehicle" + i).val():0;
      let category_id=tyryt !="Select"?tyryt:this.state.selectedindex !=""?this.state.selectedindex :"";

      // let  category_id=id+1;
      // let price = jQuery("#subtotal" + i).val() !=""? jQuery("#subtotal" + i).val():0;

      let item_list = {
        item_name: item_name,
        quantity: quantity,
        price: price,
        unit_price:unit_price,
        category_id:category_id
      };
      // console.log('hyyyyy',hello)
      result.push(item_list);
            console.log('item_list',result)

      itemprice.push(parseInt(price));
    }

    const add = (a, b) => a + b;
    const sum = itemprice.length > 0 ? itemprice.reduce(add) : 0;
    if (typeof sum === "number") {
      //  alert(typeof sum)
    }
    console.log("itemprice", sum);

    if (this.state.isChecked) {
      
      let foreign_currency = this.state.ToCurrency;
      let value = sum;
      let nope = "https://api.exchangerate-api.com/v4/latest/";
        let res = nope.concat(foreign_currency);
        fetch(res)
          .then(response => response.json())
          .then(data => {
            let todayValue = data.rates;
            let exchange_value = todayValue["SGD"];
            let item_total_foreign_currency=(value/107)*100;
            let tax_amount_foreign_currency=(item_total_foreign_currency*7)/100;
            // let convertedValueed = todayValue["SGD"] * value;
            // var convertedValuee = convertedValueed.toFixed(2);
            let item_total_home_currency=todayValue["SGD"] * item_total_foreign_currency;
            let tax_amount_home_currency=todayValue["SGD"] * tax_amount_foreign_currency;
            let grand_total_home_currency=todayValue["SGD"] * value;
  
            this.setState({
              myarray: result.reverse(),
              exchange_value:exchange_value.toFixed(2),
              item_total_foreign_currency:item_total_foreign_currency.toFixed(2),
              tax_amount_foreign_currency:tax_amount_foreign_currency.toFixed(2),
              grand_total_foreign_currency:sum.toFixed(2),
              item_total_home_currency:item_total_home_currency.toFixed(2),
              tax_amount_home_currency:tax_amount_home_currency.toFixed(2),
              grand_total_home_currency:grand_total_home_currency.toFixed(2)
  
            })
           
          });
  

    } else {
      
      let foreign_currency = this.state.ToCurrency;
      let value = sum;
      let nope = "https://api.exchangerate-api.com/v4/latest/";
        let res = nope.concat(foreign_currency);
        fetch(res)
          .then(response => response.json())
          .then(data => {
            let todayValue = data.rates;
            let exchange_value = todayValue["SGD"];
            let item_total_foreign_currency=value;
            let tax_amount_foreign_currency=(item_total_foreign_currency*7)/100;
            let  grand_total_foreign_currency=value+tax_amount_foreign_currency;
           // alert(grand_total_foreign_currency)
            // let convertedValueed = todayValue["SGD"] * value;
            // var convertedValuee = convertedValueed.toFixed(2);
            let item_total_home_currency=todayValue["SGD"] * item_total_foreign_currency;
            let tax_amount_home_currency=todayValue["SGD"] * tax_amount_foreign_currency;
            let grand_total_home_currency=todayValue["SGD"] * grand_total_foreign_currency;
  
            this.setState({
              myarray: result.reverse(),
              exchange_value:exchange_value.toFixed(2),
              item_total_foreign_currency:item_total_foreign_currency.toFixed(2),
              tax_amount_foreign_currency:tax_amount_foreign_currency.toFixed(2),
              grand_total_foreign_currency:grand_total_foreign_currency.toFixed(2),
              item_total_home_currency:item_total_home_currency.toFixed(2),
              tax_amount_home_currency:tax_amount_home_currency.toFixed(2),
              grand_total_home_currency:grand_total_home_currency.toFixed(2)
  
            })
           
          });
    }
  }

  toggleChange = () => {
    // alert()
    this.setState({
      isChecked: !this.state.isChecked,
    });
   this.runFunction();  };

 
runFunction=()=>{
  this. handleChangeItems(0,this.state.rows.length-1);



}

handleCheck(e) {
    
     this.setState({selected:e.currentTarget.dataset.id,
      selectedindex:e.currentTarget.dataset.the
    })
    
    }
     
  render() {
    if(this.state.myarray.length>0 &&this.state.myarray[0].price !="" && this.state.myarray[0].price !=0){    console.log("hyyyyy",  this.state.myarray[0].price);
  }
  console.log('balance_sheet_category_list',this.state.balancesheetlist)
    let THIS = this;
    let file_path = [],
      file_path_list = "",
      scanned_div = [],
      comment_list = [];

    file_path_list = this.state.get_file_path.toString();
    if (file_path_list !== "") {
      var get_file_ext = file_path_list.substring(
        file_path_list.lastIndexOf(".") + 1,
        file_path_list.length
      );
      if (
        get_file_ext === "png" ||
        get_file_ext === "jpg" ||
        get_file_ext === "jpeg"
      ) {
        file_path.push(
          <li>
            <a href="javascript:;" className="active">
              <img src={file_path_list} className="img-responsive" />
            </a>
          </li>
        );

        scanned_div.push(
          <div className="scanned-file">
            <img src={file_path_list} alt="Scanned-file" />
          </div>
        );
      } else {
        var pdf_file_url = file_path_list + "#toolbar=0&navpanes=0";
        file_path.push(
          <li>
            <a href="javascript:;" className="active">
              {/* <img src={file_path_list} className="img-responsive" /> */}
              <iframe
                src={pdf_file_url}
                className="data_tagging_thumb"
                frameborder="0"
                scrolling="no"
              ></iframe>
            </a>
          </li>
        );

        scanned_div.push(
          <div className="scanned-file">
            {/* <img src={file_path_list} alt="Scanned-file" /> */}
            <iframe
              src={pdf_file_url}
              className="data_tagging_large"
              frameborder="0"
              scrolling="no"
            ></iframe>
          </div>
        );
      }
    }

    return (
      <div>
        <div className="container-fluid">
          <div className="row">
            <LeftSidebar pageSubmit={e => this.pageLink(e)} />

            <div className="main-wrap col-md-12 col-xs-12 pad-r-no">
              <div className="top-bar col-md-12 col-xs-12 pad-r-no">
                <div className="nav-brand-res visible-xs">
                  <img
                    className="img-responsive"
                    src="../../images/logo-icon.png"
                    alt="LogoIcon"
                  />
                </div>
                <a
                  onClick={this.routedChange.bind(this, "user_inbox")}
                  className="back hidden-xs"
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="18.5"
                    height="14.249"
                    viewBox="0 0 18.5 14.249"
                  >
                    <g
                      id="left-arrow_2_"
                      data-name="left-arrow (2)"
                      transform="translate(0 -58.83)"
                    >
                      <g
                        id="Group_25"
                        data-name="Group 25"
                        transform="translate(0 65.207)"
                      >
                        <g
                          id="Group_24"
                          data-name="Group 24"
                          transform="translate(0 0)"
                        >
                          <path
                            id="Path_19"
                            data-name="Path 19"
                            d="M17.753,235.318H.747a.747.747,0,0,0,0,1.495H17.753a.747.747,0,0,0,0-1.495Z"
                            transform="translate(0 -235.318)"
                          />
                        </g>
                      </g>
                      <g
                        id="Group_27"
                        data-name="Group 27"
                        transform="translate(0 58.83)"
                      >
                        <g
                          id="Group_26"
                          data-name="Group 26"
                          transform="translate(0 0)"
                        >
                          <path
                            id="Path_20"
                            data-name="Path 20"
                            d="M1.8,65.954l5.849-5.849A.747.747,0,1,0,6.6,59.049L.219,65.426a.747.747,0,0,0,0,1.057L6.6,72.86A.747.747,0,1,0,7.653,71.8Z"
                            transform="translate(0 -58.83)"
                          />
                        </g>
                      </g>
                    </g>
                  </svg>
                </a>
                <span className="page-title hidden-xs">
                  Inbox / <span>Data Tagging</span>
                </span>

                <Topbar logoutSubmit={e => this.logoutLink()} />
              </div>

              <div className="main-content col-md-12 col-xs-12">
                <div className="resp_msg">{this.state.add_cmnt_msg}</div>

                <input
                  type="hidden"
                  id="logged_user_id"
                  value={this.state.logged_user_id}
                />
                <input
                  type="hidden"
                  id="file_id"
                  value={this.props.match.params.file_id}
                />
                <input
                  type="hidden"
                  id="list_id"
                  value={this.props.match.params.list_id}
                />

                <a
                  onClick={this.routedChange.bind(this, "user_inbox")}
                  className="back visible-xs"
                >
                  {/* <img src="../../images/back-arrow-dark.svg"> */}
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="18.5"
                    height="14.249"
                    viewBox="0 0 18.5 14.249"
                  >
                    <g
                      id="left-arrow_2_"
                      data-name="left-arrow (2)"
                      transform="translate(0 -58.83)"
                    >
                      <g
                        id="Group_25"
                        data-name="Group 25"
                        transform="translate(0 65.207)"
                      >
                        <g
                          id="Group_24"
                          data-name="Group 24"
                          transform="translate(0 0)"
                        >
                          <path
                            id="Path_19"
                            data-name="Path 19"
                            d="M17.753,235.318H.747a.747.747,0,0,0,0,1.495H17.753a.747.747,0,0,0,0-1.495Z"
                            transform="translate(0 -235.318)"
                          />
                        </g>
                      </g>
                      <g
                        id="Group_27"
                        data-name="Group 27"
                        transform="translate(0 58.83)"
                      >
                        <g
                          id="Group_26"
                          data-name="Group 26"
                          transform="translate(0 0)"
                        >
                          <path
                            id="Path_20"
                            data-name="Path 20"
                            d="M1.8,65.954l5.849-5.849A.747.747,0,1,0,6.6,59.049L.219,65.426a.747.747,0,0,0,0,1.057L6.6,72.86A.747.747,0,1,0,7.653,71.8Z"
                            transform="translate(0 -58.83)"
                          />
                        </g>
                      </g>
                    </g>
                  </svg>
                </a>
                <span className="page-title visible-xs">
                  Inbox / <span>Data Tagging</span>
                </span>
                <div className="content-top col-md-12 col-xs-12 pad-no">
                  <select className="select-dropdown selectpicker">
                    <option>Bill-Payment.pdf</option>
                    <option>Taxi-bill.jpg</option>
                    <option>Stationary.jpg</option>
                  </select>
                </div>

                <div className="content-sec col-md-12 col-xs-12 pad-no inbox-listing">
                  <div className="col-md-12 col-xs-12 scanned-wrap">
                    <p className="visible-xs note-content">
                      This feature cant use in mobile. Please use Desktop
                    </p>
                    <div className="col-md-6 col-md-12 scanned-left hidden-xs">
                      <div className="file-thumbnail">
                        <ul className="list-unstyled">{file_path}</ul>
                      </div>
                      <div className="doc-wrap">
                        <div className="zoom-btn">
                          <a href="javascript:;" className="plus">
                            <img src="../../images/zoom-in.svg" alt="icon" />
                          </a>
                          <a href="javascript:;" className="minus">
                            <img src="../../images/zoom-out.svg" alt="icon" />
                          </a>
                        </div>

                        <ContextMenuTrigger id="some_unique_identifier">
                          {scanned_div}
                        </ContextMenuTrigger>

                        <ContextMenu id="some_unique_identifier">
                          <MenuItem
                            data={{ foo: "bar1" }}
                            onClick={this.handleClick}
                          >
                            Copy
                          </MenuItem>
                          <MenuItem
                            data={{ foo: "bar2" }}
                            onClick={this.handleClick}
                          >
                            Add to First Name
                          </MenuItem>
                          <MenuItem divider />
                          <MenuItem
                            data={{ foo: "bar3" }}
                            onClick={this.handleClick}
                          >
                            Add to Last Name
                          </MenuItem>
                        </ContextMenu>

                        <form
                          className="comment-sec"
                          method="post"
                          onSubmit={this.addCommentFunc.bind(this)}
                        >
                          <textarea
                            cols="3"
                            rows="5"
                            name="comment_text"
                            id="comment_text"
                            className="form-control"
                            placeholder="Comments"
                            required
                          ></textarea>
                          <button className="btn btn-green" type="submit">
                            Send
                          </button>
                        </form>

                        <div className="comments-wrap">
                          {this.state.file_comments.map(
                            (comments_data, index) => {
                              if (comments_data.user_image) {
                                var user_img = comments_data.user_image;
                              } else {
                                var user_img = "../../images/user-img-1.png";
                              }

                              if (
                                comments_data.user_id ===
                                parseInt(this.state.logged_user_id)
                              ) {
                                var disp_cmnt = "show";
                              } else {
                                var disp_cmnt = "hide";
                              }

                              var get_file_path = comments_data.file_path;
                              if (get_file_path.length > 0) {
                                var dis_file_path = [];
                                var attach_file_path = [];
                                var split_file_path = get_file_path
                                  .toString()
                                  .split(",");

                                var split_file_id = comments_data.attachments
                                  .toString()
                                  .split(",");

                                if (get_file_path.length > 1) {
                                  for (var i = 0; i < 2; i++) {
                                    var get_file_name = split_file_path[i];
                                    var split_file_name = split_file_path[i]
                                      .toString()
                                      .split("/");
                                    var arr_reverse = split_file_name.reverse();

                                    var get_file_ext = arr_reverse[0].substring(
                                      arr_reverse[0].lastIndexOf(".") + 1,
                                      arr_reverse[0].length
                                    );
                                    if (get_file_ext === "pdf") {
                                      var file_icon = "images/pdf-icon.png";
                                    } else {
                                      var file_icon = "images/img-icon.png";
                                    }

                                    dis_file_path.push(
                                      <a
                                        onClick={this.dataTaggingFunc.bind(
                                          this,
                                          this.state.list_id,
                                          split_file_id[i]
                                        )}
                                      >
                                        <img
                                          src={file_icon}
                                          alt={get_file_ext}
                                        />
                                        <span>
                                          {arr_reverse[0].substring(
                                            arr_reverse[0].length - 15,
                                            arr_reverse[0].length
                                          )}
                                        </span>
                                      </a>
                                    );
                                  }

                                  if (get_file_path.length > 2) {
                                    var more_div = (
                                      <span className="etc">+2 more</span>
                                    );
                                  }
                                } else {
                                  var split_file_name = comments_data.file_path[0]
                                    .toString()
                                    .split("/");
                                  var arr_reverse = split_file_name.reverse();

                                  var get_file_ext = arr_reverse[0].substring(
                                    arr_reverse[0].lastIndexOf(".") + 1,
                                    arr_reverse[0].length
                                  );
                                  if (get_file_ext === "pdf") {
                                    var file_icon = "../../images/pdf-icon.png";
                                  } else {
                                    var file_icon = "../../images/img-icon.png";
                                  }
                                  var dis_file_path = (
                                    <a
                                      onClick={this.dataTaggingFunc.bind(
                                        this,
                                        this.state.list_id,
                                        split_file_id[0]
                                      )}
                                    >
                                      <img src={file_icon} alt={get_file_ext} />
                                      <span>
                                        {arr_reverse[0].substring(
                                          arr_reverse[0].length - 15,
                                          arr_reverse[0].length
                                        )}
                                      </span>
                                    </a>
                                  );
                                }

                                var replyTxt = "Reply";
                                console.log(
                                  "sub_comment_count",
                                  comments_data.sub_comment_count
                                );
                                if (comments_data.sub_comment_count > 0) {
                                  var replyTxt =
                                    "Replies(" +
                                    comments_data.sub_comment_count +
                                    ")";
                                } else {
                                  var replyTxt = "Reply";
                                }
                              }
                              return (
                                <div
                                  className="comment-sec col-md-12 col-xs-12 pad-no"
                                  data-comment-id={comments_data.comment_id}
                                >
                                  <div className="avatar-img">
                                    <img
                                      className="img-responsive"
                                      src={user_img}
                                      alt="AvatarIMG"
                                    />
                                  </div>
                                  <div className="comment-cont">
                                    <span className="col-md-12 col-xs-12 pad-no user-name">
                                      {comments_data.comment_user}
                                    </span>
                                    <span className="col-md-12 col-xs-12 pad-no date">
                                      {comments_data.ago_value}
                                    </span>

                                    <div
                                      className={`dropdown menu-item prnt_cmnt ${disp_cmnt}`}
                                    >
                                      <a
                                        href="javascript"
                                        className="dropdown-toggle"
                                        data-toggle="dropdown"
                                      >
                                        <img
                                          src="../../images/menu-dot.svg"
                                          alt="icon"
                                        />
                                      </a>
                                      <ul className="dropdown-menu">
                                        <li>
                                          <a
                                            href="javascript:;"
                                            data-comment-id={
                                              comments_data.comment_id
                                            }
                                            className="edit_cmnt"
                                          >
                                            Edit
                                          </a>
                                        </li>
                                        <li>
                                          <a
                                            onClick={this.deleteComment.bind(
                                              this,
                                              comments_data.comment_id,
                                              this.props.match.params.list_id,
                                              0
                                            )}
                                          >
                                            Delete
                                          </a>
                                        </li>
                                      </ul>
                                    </div>

                                    <p className="col-md-12 col-xs-12 pad-no comment-txt">
                                      {comments_data.comment_text}
                                    </p>

                                    <form
                                      className="col-md-12 col-xs-12 pad-no reply-form update_cmnt hide"
                                      onSubmit={this.updateCmmnt.bind(this)}
                                    >
                                      <input
                                        type="hidden"
                                        name="comment_id"
                                        value={comments_data.comment_id}
                                      />
                                      <input
                                        type="hidden"
                                        name="parent_comment_id"
                                        value="0"
                                      />
                                      <textarea
                                        className="col-md-12 col-xs-12 cmnt_txt"
                                        id={`cmmnt_txt_id${comments_data.comment_id}`}
                                        name="comment_text"
                                        placeholder=""
                                        required
                                      ></textarea>
                                      <div className="pull-right">
                                        <button
                                          type="button"
                                          className="btn btn-default cancel_button"
                                        >
                                          Cancel
                                        </button>
                                        <button
                                          type="submit"
                                          className="btn btn-green"
                                        >
                                          Update
                                        </button>
                                      </div>
                                    </form>

                                    <div className="attachment-item col-md-12 col-xs-12 pad-no">
                                      {dis_file_path} {more_div}
                                    </div>

                                    {/* <a href="javascript:;" data-target={`#reply_frm${comments_data.comment_id}`} className="pull-left reply-link" onClick={this.replyFunc.bind(this,comments_data.comment_id)}>Reply</a> */}

                                    <a
                                      href="javascript:;"
                                      data-target={`#reply_frm${comments_data.comment_id}`}
                                      className="pull-left reply-link"
                                      data-comment-id={comments_data.comment_id}
                                      onClick={this.replyLink.bind(
                                        this,
                                        comments_data.comment_id,
                                        "replyLink"
                                      )}
                                    >
                                      {comments_data.sub_comment_count
                                        ? "Replies (" +
                                          comments_data.sub_comment_count +
                                          ")"
                                        : "Reply"}
                                    </a>

                                    <div
                                      className="reply_cnt collapse"
                                      id={`reply_cnt${comments_data.comment_id}`}
                                    >
                                      <form
                                        className="col-md-12 col-xs-12 pad-no reply-form reply-frm"
                                        onSubmit={this.addSubCommentFunc.bind(
                                          this
                                        )}
                                        ref="form"
                                      >
                                        <input
                                          type="hidden"
                                          name="parent_comment_id"
                                          value={comments_data.comment_id}
                                        />

                                        <textarea
                                          className="col-md-12 col-xs-12 reply_txt_cls"
                                          name="reply_txt"
                                          placeholder="Reply..."
                                          required
                                        ></textarea>
                                        <div className="pull-right">
                                          <div className="files_txt">
                                            {this.state.attachment_file.length
                                              ? this.state.attachment_file
                                                  .length +
                                                " files are attached"
                                              : ""}
                                          </div>
                                          <a
                                            href="javascript:;"
                                            className=" btn btn-empty"
                                          >
                                            <input
                                              type="file"
                                              name="imgInp[]"
                                              id="imgInp2"
                                              className="add_img replyAttach"
                                              multiple
                                              onChange={this.loadFile.bind(
                                                this
                                              )}
                                              accept="image/*,application/pdf"
                                            />
                                            <img
                                              src="../../images/attach-icon.svg"
                                              alt="icon"
                                            />
                                          </a>
                                          <button
                                            type="submit"
                                            className="btn btn-green"
                                          >
                                            <img
                                              src="../../images/reply-icon.svg"
                                              alt="icon"
                                            />
                                            Reply
                                          </button>
                                        </div>
                                      </form>

                                      <div className="subCmnt"></div>
                                    </div>
                                  </div>
                                </div>
                              );
                            }
                          )}
                        </div>
                      </div>
                    </div>

                    <div className="col-lg-6 col-md-12 col-xs-12 scanned-right hidden-xs">
                      <form  id="form_save" className="data-feed" onSubmit={(e) => { e.preventDefault();
                     this.saveAndContinue() ;}}>
                        <div className="form-group col-md-12 col-xs-12">
                          <label>Company Name</label>
                          <input
                            type="text"
                            name="company_name"
                            className="form-control"
                            onChange={event => this.handleChange(event)}
                            required
                          />
                        </div>
                        <div className="form-group col-md-6 col-sm-6">
                          <label>invoice No</label>
                          <input
                            type="text"
                            name="invoice_no"
                            className="form-control"
                            onChange={event => this.handleChange(event)}
                            required
                          />
                        </div>
                        <div className="form-group col-md-6 col-sm-6">
                          <label>Date</label>
                          {/* <input
                            type="date"
                            name="incorport_date"
                            id="incorport_date"
                            onChange={event => this.handleChange(event)}
                            className="form-control"
                            required
                          /> */}
                           <div>
        <DatePicker
          onChange={this.onChange}
          value={this.state.date}
          className="form-control"
          clearIcon={null}
          clearAriaLabel="aria-label"
          style={{paddingLeft:30}}
          format="dd-MM-yyyy"
          placeholder="dd-mm-yyyy"
        />
      </div>
                        </div>
                        <div className="form-group col-md-12 col-xs-12">
                          <label>Address</label>
                          <textarea
                            cols="3"
                            rows="5"
                            name="address"
                            className="form-control "
                            onChange={event => this.handleChange(event)}
                            required
                          ></textarea>
                        </div>
                        <div className="form-group col-md-6 col-sm-6">
                          <label> Currency </label>
                          <select
                            className="form-control select-dropdown  "
                            value={this.state.ToCurrency}
                            onChange={event => this.selectHandler(event)}
                            required
                          >
                            <option> Select </option>
                            {this.state.currencies.map((cur, index) => (
                              <option key={index} value={cur}>
                                {cur}
                              </option>
                            ))}
                          </select>
                        </div>
                        <div className="form-group col-md-6 col-sm-6">
                          <label>Default Category</label>
                          <div className="custom-select-drop dropdown">
                            <a
                              aria-expanded="false"
                              aria-haspopup="true"
                              role="button"
                              data-toggle="dropdown"
                              className="dropdown-toggle btn"
                              href="javascript:;"
                              value={this.state.selected}
                              required
                            >
                              <span
                                id="selected"
                                onChange={event => this.handleChange(event)}
                              >
                          {this.state.selected !=""?this.state.selected:'Choose Category'}      
                              </span>
                              <span className="caret"></span>
                            </a>
                            <ul className="dropdown-menu category" style={{height:213,overflow:'scroll',width:'auto'}}>
                              <li>
                                <input
                                  type="text"
                                  name="search"
                                  className="form-control"
                                  placeholder="Search"
                                  required
                                />
                                <button
                                  type="button"
                                  className="btn btn-rounded btn-blue"
                                  data-toggle="modal"
                                  data-target="#pop-modal"
                                >
                                  Add New
                                  
                                  <img
                                    className="arrow-icon"
                                    src="../../images/right-arrow.svg"
                                    alt="icon"
                                  />
                                </button>
                              </li>
                              <li>
                              <ul className="list-unstyled">

                               {
                      this.state.default_category_list.map((item,index)=>{
                        return(

                          <li  key={index} onClick={this.handleCheck.bind(this)} name={item} data-id={item.name} data-the={item.id} >
                          <a href="javascript:;"  value={item.name}>{item.name}</a>
                          
                            </li>
                            );
                        
                      })}
                       </ul>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <div class="modal fade pop-modal" id="pop-modal" role="dialog">
    <div class="modal-dialog modal-md custom-modal">

      <button type="button" class="close hidden-xs" data-dismiss="modal">
          <img class="img-responsive" src="../../images/close-red.svg" alt="icon"/>
      </button>
      <div class="modal-content">
        <div class="modal-body text-center">
            <h3>Create Account Name</h3>
            <form class="custom-form row">
                {/* <div class="form-group col-md-12 col-xs-12 pad-no">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <label>Account Name</label>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" name="ac-name" class="form-control" required/>
                    </div>
                </div> */}
                 <div class="form-group col-md-12 col-xs-12 pad-no">
                    <div class="col-md-4 col-sm-4 col-md-12">
                        <label>Category</label>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <div class="custom-select-drop dropdown">
                            <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle btn" href="javascript:;">
                            <span id="selected">Choose Category</span><span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="javascript:;">Balance sheet</a></li>
                                <li><a href="javascript:;">Profit or loss</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-12 col-xs-12 pad-no">
                    <div class="col-md-4 col-sm-4 col-md-12">
                        <label>Sub Category</label>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <div class="custom-select-drop dropdown">
                            <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle btn" href="javascript:;">
                            <span id="selected">Choose Sub Category</span><span class="caret"></span>
                            </a>
    

                            <ul class="dropdown-menu">
                                
                                <li><a href="javascript:;">Revenue</a></li>
                                <li><a href="javascript:;">Other income</a></li>
                                <li><a href="javascript:;">Cost of goods sold</a></li>
                                <li><a href="javascript:;">Expenses</a></li>
                                <li><a href="javascript:;">Other expenses</a></li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-12 col-xs-12 pad-no">
                    <div class="col-md-4 col-sm-4 col-md-12">
                        <label>Account Type</label>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <div class="custom-select-drop dropdown">
                            <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle btn" href="javascript:;">
                            <span id="selected">Choose Account Type </span><span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                              
                                <li><a href="javascript:;">Other expenses</a></li>
                                <li><a href="javascript:;">Bank</a></li>
                                <li><a href="javascript:;">Accounts receivables</a></li>
                                <li><a href="javascript:;">Other current asset</a></li>

                            </ul>
                        </div>
                    </div>
                </div>
               
{/*                
                <div class="form-group col-md-12 col-xs-12 pad-no">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <label>Account Type</label>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <div class="custom-select-drop dropdown">
                            <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle btn" href="javascript:;">
                            <span id="selected">Choose Category</span><span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="javascript:;">Accounts Payable</a></li>
                                <li><a href="javascript:;">Accounts Receivable</a></li>
                                <li><a href="javascript:;">Paid</a></li>
                            </ul>
                        </div>
                    </div>
                </div> */}
                 <div class="form-group col-md-12 col-xs-12 pad-no">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <label>Account Name</label>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <input type="text" name="ac-name" class="form-control" required/>
                    </div>
                </div>
                <div class="form-group col-md-12 col-xs-12 btn-sec pad-no">
  <button class="btn btn-lightgray">Cancel</button><span>{" "}</span>
                    <button class="btn btn-green">Save</button>
                </div>
            </form>
           
        </div>
      </div>
      
    </div>
</div>
                        <div class="form-group col-md-6 col-sm-6">
                                    <label>GST</label>
                                    <div class="custom-select-drop dropdown">
                                        <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle btn" href="javascript:;">
                                        <span id="selected">Standard-Rated Supplies</span><span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="active"><a href="javascript:;">Standard-Rated Supplies</a></li>
                                            <li><a href="javascript:;">Zero-Rated Supplies</a></li>
                                            <li><a href="javascript:;">Exempt Supplies</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group col-md-6 col-sm-6">
                                    <span class="form-label clearfix">Taxes</span>
                                    <label class="custom-checkbox">
                                          <input
                              type="checkbox"
                              checked={this.state.isChecked}
                              onChange={this.toggleChange}
                            /> Including Tax
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                        
                        <div className="form-table">
                          <table>
                            <thead>
                              <tr>
                                <th>No</th>
                                <th>Items</th>
                                <th>Qty</th>
                                <th>Unit price</th>
                                <th>Sub Total</th>
                                <th>Category</th>
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.rows.map(function(row, index) {
                                let itemid = index;
                                let myvalue= THIS.state.myarray.length>0 &&THIS.state.myarray[itemid]&&THIS.state.myarray[itemid].price !="" && THIS.state.myarray[itemid].price !=0 ?THIS.state.myarray[itemid].price:"";
                                let selected_categeory =THIS.state.myarray.length>0 &&THIS.state.myarray[itemid]&&THIS.state.myarray[itemid].category_id !="" && THIS.state.myarray[itemid].category_id !=0 ?THIS.state.myarray[itemid].category_id:THIS.state.selected
                                //   alert(itemid)
                                return (
                                  <tr key={itemid}>
                                    <td> {itemid + 1} </td>
                                    <td>
                                      <input
                                        type="text"
                                        className="form-control no-bg"
                                        name="item_name"
                                        id={`item${itemid}`}
                                        onInput={event =>
                                          THIS.handleChangeItems(event, itemid)
                                        }
                                        required
                                      />
                                    </td>

                                    <td>
                                      <input
                                        type="text"
                                        className="form-control no-bg"
                                        name="quantity"
                                        id={`quantity${itemid}`}
                                        onChange={THIS.onChangeRestricttext}
                                        

                                        onInput={event =>
                                          THIS.handleChangeItems(event, itemid)
                                        }
                                        required

                                      />
                                    </td>
                                     <td>
                                      <input
                                        type="text"
                                        className="form-control no-bg"
                                        name="unit_price"
                                        onChange={THIS.onChangeRestricttext}
                                        id={`unit_price${itemid}`}
                                        onInput={event =>
                                          THIS.handleChangeItems(event, itemid)
                                        }
                                                                              required

                                      />
                                    </td>
                                    <td>
                                      <span
                                        type="text"
                                        className="form-control no-bg"
                                        name="sub_total"
                                        id={`subtotal${itemid}`}
                                       style={{textAlign:'end',backgroundColor:'none'}}
                                    
                              >{ myvalue}</span>
                                    </td>
                                    <td>
                            
                            <select
                            className="form-control select-dropdown no-bg  "
                            onChange={event =>
                              THIS.handleChangeItems(event, itemid)
                             }
                            data-id={(event)=>event.target.value}
                            // id="selectVehicle"
                            id={`selectVehicle${itemid}`}

                          >
                            <option> {THIS.state.selected!=""?THIS.state.selected:"select"} </option>
                            {THIS.state.default_category_list.map((item, index) => (
                              <option key={index} data-year={item.id} value={item.id}>
                                {item.name}
                              </option>
                            ))}
                          </select>

                                    </td>
                                  </tr>
                                );
                              })}
                            </tbody>
                          </table>
                          <a href="javascript:;" className="add-col"><img src="../../images/plus-icon.svg" alt="icon"/></a>
                          <button
                            id="addBtn"
                            className="btn btn-green"
                            type="button"
                            onClick={this.addRow}
                          >
                            Add more
                          </button>
                        </div>
                        <div className="form-group currency-label">
                          <span>Foreign Currency({this.state.ToCurrency})</span>
                          <span> Home Currency(SGD) </span>
                        </div>
                        <div className="form-group total-input">
                          <label>Item Total</label>
                          <div>
                            <span
                              type="text"
                              name="item_total_foreign_currency"
                              className="form-control"
                              onChange={this.convertCurrency}
                               style={{textAlign:'end'}}
                            >
                              {this.state.item_total_foreign_currency}
                            </span>
                            <span
                              type="text"
                              name="item_total_home_currency"
                              className="form-control"
                               style={{textAlign:'end'}}
                            >
                              {this.state.item_total_home_currency}
                            </span>
                          </div>
                        </div>
                        <div className="form-group total-input">
                          <label>Tax</label>
                          <div>
                            <span
                              type="text"
                              name="tax_amount_foreign_currency"
                              className="form-control"
                               style={{textAlign:'end'}}
                            >
                              {this.state.tax_amount_foreign_currency}
                            </span>
                            <span
                              type="text"
                              name="tax_amount_home_currency"
                              className="form-control"
                              onChange={this.convertCurrency}
                               style={{textAlign:'end'}}
                            >
                              {this.state.tax_amount_home_currency}
                            </span>
                          </div>
                        </div>
                       
                        <div className="form-group total-input" >
                          <label>Grand Total</label>
                          <div>
                            <span
                              name="grand_total_foreign_currency"
                              className="form-control"
                               style={{textAlign:'end'}}
                            >
                              {this.state.grand_total_foreign_currency}
                            </span>
                            <span
                              name="grand_total_home_currency"
                              className="form-control"
                               style={{textAlign:'end'}}
                              // onChange={this.convertCurrency}
                            >
                              {this.state.grand_total_home_currency}
                            </span>
                          </div>
                        </div>
                        <div className="form-group total-input">
                                    <label>Balance sheet category</label>
                                    <div >
                                    <select
                            className="form-control select-dropdown  "
                            value={this.state.balance_list_selected}
                            style={{width:"100%"}}
                            onChange={event => this.selectHandlerBalancelist(event)}
                          >
                            <option> Select </option>
                            {this.state.balancesheetlist.map((item, index) => (
                              <option key={index} value={item.id}>
                                {item.name}
                              </option>
                            ))}
                          </select>
                          </div>
                                    {/* <div style={{paddingLeft:-5}}>
                                        <div className="custom-select-drop dropdown">
                                            <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" className="dropdown-toggle btn" href="javascript:;">
                                            <span id="selected">Choose Category</span><span className="caret"></span>
                                            </a>
                                            <ul className="dropdown-menu">
                                                <li><a href="javascript:;">Accounts Payable</a></li>
                                                <li><a href="javascript:;">Accounts Receivable</a></li>
                                                <li><a href="javascript:;">Paid</a></li>
                                            </ul>
                                        </div>
                                    </div> */}
                                </div>

                        <div className="form-group exchange-rate">
                          <label>Exchange Rate 1 {this.state.ToCurrency}</label>
                          <div>
                            <span
                              type="text"
                              name="exchangeRate"
                              className="form-control"
                            >
                              {this.state.exchange_value}
                            </span>
                            <span className="label">SGD</span>
                          </div>
                        </div>
                        <div className="form-group total-input text-right">
                          <div className="submit-enclose">
                            <button
                              className="btn btn-gray"
                              type="button"
                              // onClick={this.saveAndContinue}
                            >
                              Clear
                            </button>
                            <span> </span>
                            <button className="btn btn-yellow" type="button">
                              Save Draft
                            </button>
                            <span> </span>

                            <button
                              className="btn btn-green"
                              type="submit"
                              onClick={this.saveAndContinue}
                            >
                              Save & Continue
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer logoutSubmit={e => this.logoutLink()} />
      </div>
    );
  }
}
export default data_tagging;
