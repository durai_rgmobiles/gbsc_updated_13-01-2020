import React, { Component } from 'react'
import FetchAllApi from './api_links/fetch_all_api.js'
import parse from 'html-react-parser'
import jQuery from 'jquery'

export class custom_editable_template extends Component {
  constructor (props) {
    super(props)
    this.state = {
      header_entity_logo: 'true',
      header_logo_size: '',
      header_logo_path: '',
      header_stripe_background: '',
      header_text_size: '',
      header_text_color: '',
      header_entity_name: '',
      header_entity_address: 'none',
      header_entity_contact_no: 'none',
      header_entity_name: 'none',
      header_entity_email: 'true',
      item_text_size: 23,
      item_text_color: 'black',
      item_invoice_to: 'none',
      item_invoice_to_address:
        'Blk 194, Pandan Loop #02-17, Pantech Ind Complex Singapore 128383, Singapore',
      item_bill_to: 'abc company',
      item_invoice_no: '1234',
      item_date: '12-09-2012',
      item_text_size: 15,
      item_text_color: 'black',
      item_table_background: 'blue',
      item_table_head_background: 'green',
      item_table_row_background: 'green',
      item_table_border_color: 'yellow',
      item_table_label_no: 'none',
      item_table_label_item: 'none',
      item_table_label_price_each: 'none',
      item_table_label_tax: 'none',
      item_table_label_total: 'none',
      item_table_label_sub_total: 'none',
      item_table_headinglabel_num_name: 'no',
      item_table_headinglabel_item_name: 'Item',
      item_table_headinglabel_priceEach_name: 'Unit price',
      item_table_headinglabel_quantity_name: 'Quantity',
      item_table_headinglabel_tax_name: 'Tax',
      item_table_headinglabel_total_name: 'Total',
      item_total_props_subtotal: 'none',
      item_total_props_tax: 'none',
      item_total_props_grant_total: 'none',
      item_total_props_amnt_inwords: 'none',
      item_total_props_subtotal_labl_name: 'Sub Total',
      item_total_props_tax_labl_name: 'Tax',
      item_total_props_grant_total_labl_name: 'Grand Total',
      item_total_props_amnt_inwords_labl_name: 'Amount in Words',
      footer_text_size: 15,
      footer_text_color: 'black',
      footer_thanking_msg: 'none',
      footer_thanking_msg_content: 'Thank you',
      footer_terms_conditions: 'none',
      footer_terms_conditions_label_name: 'none',
      footer_terms_conditions_content: 'Its true to my best ',
      html_content: ''
    }
  }
  increase_logo_size = () => {
    jQuery('#logo_img').width(700)
  }

  componentDidMount () {
    var client_id = 1
    FetchAllApi.get_edit_invoice_html(client_id, (err, response) => {
      console.log('defaultcategorylist', response)
      //alert(response.message)
      console.log('jhsh', response.list[0].html_content)
      if (response.status === 1) {
        this.setState({
          html_content: response.list[0].html_content
        })
      } else {
        this.setState({})
      }
    })
  }

  render () {
    return (
      <div>
        {/* Main Wrapper Starts here */}
        <div className='container-fluid invoice-editor'>
          <div className='row'>
            {/* Builder Left Starts here */}
            <div className='builder-left'>
              <div
                className='mscroll-y mCustomScrollbar _mCS_1 mCS-autoHide'
                style={{ position: 'relative', overflow: 'visible' }}
              >
                <div
                  id='mCSB_1'
                  className='mCustomScrollBox mCS-light mCSB_vertical mCSB_outside'
                  style={{ maxHeight: 'none' }}
                  tabIndex={0}
                >
                  <div
                    id='mCSB_1_container'
                    className='mCSB_container'
                    style={{ position: 'relative', top: 0, left: 0 }}
                    dir='ltr'
                  >
                    <h5>Template Properties</h5>
                    <ul className='nav nav-pills'>
                      <li className='active'>
                        <a data-toggle='pill' href='#tab-header'>
                          Header
                        </a>
                      </li>
                      <li>
                        <a data-toggle='pill' href='#tab-item'>
                          Item
                        </a>
                      </li>
                      <li>
                        <a data-toggle='pill' href='#tab-footer'>
                          Footer
                        </a>
                      </li>
                    </ul>
                    <div className='tab-content'>
                      <div id='tab-header' className='tab-pane fade in active'>
                        <form className='custom-form'>
                          <div className='form-group clearfix'>
                            <span className='editor-label'>
                              <label className='switch'>
                                <input
                                  type='checkbox'
                                  defaultChecked='checked'
                                />
                                <span className='drag-ball'>
                                  <span className='off' />
                                  <span className='on' />
                                </span>
                              </label>
                              <span className='form-label'>
                                Entity Logo
                                <i
                                  data-toggle='tooltip'
                                  data-placement='right'
                                  tabIndex={0}
                                  title
                                  data-original-title='You can change the logo from Organization settings'
                                >
                                  <img
                                    src='images/round-info-icon.svg'
                                    alt='icon'
                                    className='mCS_img_loaded'
                                  />
                                </i>
                              </span>
                            </span>
                            <div className='logo-wrap'>
                              <img
                                className='img-responsive mCS_img_loaded'
                                src='images/form-sample-logo.png'
                                alt='icon'
                              />
                            </div>
                            <div className='range-encl'>
                              <span className='form-label'>Logo Size</span>
                              <input type='range' name='img-size' />
                            </div>
                          </div>
                          <div className='form-group clearfix'>
                            <span className='form-label fw-sbold w-100 mar-btm'>
                              Header Options
                            </span>
                            <div className='clearfix'>
                              <span className='editor-label'>
                                <label className='switch'>
                                  <input
                                    type='checkbox'
                                    defaultChecked='checked'
                                  />
                                  <span className='drag-ball'>
                                    <span className='off' />
                                    <span className='on' />
                                  </span>
                                </label>
                                <span className='form-label'>
                                  Stripe Background
                                </span>
                                <div className='input-group'>
                                  <input
                                    type='text'
                                    className='form-control'
                                    name='text-color'
                                  />
                                  <span className='input-group-addon'>
                                    <img
                                      src='images/color-wheel-icon.svg'
                                      alt='icon'
                                      className='mCS_img_loaded'
                                    />
                                  </span>
                                </div>
                              </span>
                            </div>
                            <div className='clearfix mar-btm'>
                              <span className='form-label'>Text Size</span>
                              <div className='input-group'>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='text-size'
                                />
                                <span className='input-group-addon fw-500'>
                                  pt
                                </span>
                              </div>
                            </div>
                            <div className='clearfix'>
                              <span className='form-label'>Text Color</span>
                              <div className='input-group'>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='text-color'
                                />
                                <span className='input-group-addon'>
                                  <img
                                    src='images/color-wheel-icon.svg'
                                    alt='icon'
                                    className='mCS_img_loaded'
                                  />
                                </span>
                              </div>
                            </div>
                          </div>
                          <div className='form-group clearfix'>
                            <span className='editor-label'>
                              <label className='switch'>
                                <input type='checkbox' />
                                <span className='drag-ball'>
                                  <span className='off' />
                                  <span className='on' />
                                </span>
                              </label>
                              <span className='form-label'>Entity Name</span>
                            </span>
                          </div>
                          <div className='form-group clearfix'>
                            <span className='editor-label'>
                              <label className='switch'>
                                <input
                                  type='checkbox'
                                  defaultChecked='checked'
                                />
                                <span className='drag-ball'>
                                  <span className='off' />
                                  <span className='on' />
                                </span>
                              </label>
                              <span className='form-label'>Entity Address</span>
                            </span>
                          </div>
                          <div className='form-group clearfix'>
                            <span className='editor-label'>
                              <label className='switch'>
                                <input
                                  type='checkbox'
                                  defaultChecked='checked'
                                />
                                <span className='drag-ball'>
                                  <span className='off' />
                                  <span className='on' />
                                </span>
                              </label>
                              <span className='form-label'>
                                Entity Contact No.
                              </span>
                            </span>
                          </div>
                          <div className='form-group clearfix mar-b-no'>
                            <span className='editor-label'>
                              <label className='switch'>
                                <input
                                  type='checkbox'
                                  defaultChecked='checked'
                                />
                                <span className='drag-ball'>
                                  <span className='off' />
                                  <span className='on' />
                                </span>
                              </label>
                              <span className='form-label'>Entity Email</span>
                            </span>
                          </div>
                          <small>
                            <i>
                              Change header item content from organization
                              profile
                            </i>
                          </small>
                        </form>
                      </div>
                      <div id='tab-item' className='tab-pane fade'>
                        <form className='custom-form'>
                          <div className='form-group clearfix'>
                            <span className='form-label fw-sbold w-100 mar-btm'>
                              Items Options
                            </span>
                            <div className='clearfix mar-btm'>
                              <span className='form-label'>Text Size</span>
                              <div className='input-group'>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='text-size'
                                />
                                <span className='input-group-addon fw-500'>
                                  pt
                                </span>
                              </div>
                            </div>
                            <div className='clearfix'>
                              <span className='form-label'>Text Color</span>
                              <div className='input-group'>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='text-color'
                                />
                                <span className='input-group-addon'>
                                  <img
                                    src='images/color-wheel-icon.svg'
                                    alt='icon'
                                    className='mCS_img_loaded'
                                  />
                                </span>
                              </div>
                            </div>
                          </div>
                          <div className='form-group clearfix'>
                            <span className='editor-label'>
                              <label className='switch'>
                                <input
                                  type='checkbox'
                                  defaultChecked='checked'
                                />
                                <span className='drag-ball'>
                                  <span className='off' />
                                  <span className='on' />
                                </span>
                              </label>
                              <span className='form-label'>Invoice to</span>
                            </span>
                            <div className='input-group'>
                              <span className='input-group-addon addon-left fw-500'>
                                Label
                              </span>
                              <input
                                type='text'
                                className='form-control'
                                name='label'
                                defaultValue='Invoice to'
                              />
                            </div>
                          </div>
                          <div className='form-group clearfix'>
                            <span className='editor-label'>
                              <label className='switch'>
                                <input type='checkbox' />
                                <span className='drag-ball'>
                                  <span className='off' />
                                  <span className='on' />
                                </span>
                              </label>
                              <span className='form-label'>Bill to</span>
                            </span>
                            <div className='input-group'>
                              <span className='input-group-addon addon-left fw-500'>
                                Label
                              </span>
                              <input
                                type='text'
                                className='form-control'
                                name='label'
                                defaultValue='Bill to'
                              />
                            </div>
                          </div>
                          <div className='form-group clearfix'>
                            <span className='editor-label'>
                              <label className='switch'>
                                <input
                                  type='checkbox'
                                  defaultChecked='checked'
                                />
                                <span className='drag-ball'>
                                  <span className='off' />
                                  <span className='on' />
                                </span>
                              </label>
                              <span className='form-label'>Invoice No</span>
                            </span>
                            <div className='input-group'>
                              <span className='input-group-addon addon-left fw-500'>
                                Label
                              </span>
                              <input
                                type='text'
                                className='form-control'
                                name='label'
                                defaultValue='Invoice no'
                              />
                            </div>
                          </div>
                          <div className='form-group clearfix'>
                            <span className='editor-label'>
                              <label className='switch'>
                                <input
                                  type='checkbox'
                                  defaultChecked='checked'
                                />
                                <span className='drag-ball'>
                                  <span className='off' />
                                  <span className='on' />
                                </span>
                              </label>
                              <span className='form-label'>Date</span>
                            </span>
                            <div className='input-group'>
                              <span className='input-group-addon addon-left fw-500'>
                                Label
                              </span>
                              <input
                                type='text'
                                className='form-control'
                                name='label'
                                defaultValue='Date'
                              />
                            </div>
                          </div>
                          <div className='form-group clearfix'>
                            <span className='form-label fw-sbold w-100 mar-btm'>
                              Table Properties
                            </span>
                            <div className='clearfix mar-btm'>
                              <span className='form-label'>Text Size</span>
                              <div className='input-group'>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='text-size'
                                />
                                <span className='input-group-addon fw-500'>
                                  pt
                                </span>
                              </div>
                            </div>
                            <div className='clearfix mar-btm'>
                              <span className='form-label'>Text Color</span>
                              <div className='input-group'>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='text-color'
                                />
                                <span className='input-group-addon'>
                                  <img
                                    src='images/color-wheel-icon.svg'
                                    alt='icon'
                                    className='mCS_img_loaded'
                                  />
                                </span>
                              </div>
                            </div>
                            <div className='clearfix mar-btm'>
                              <span className='form-label'>
                                Table background
                              </span>
                              <div className='input-group'>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='text-color'
                                />
                                <span className='input-group-addon'>
                                  <img
                                    src='images/color-wheel-icon.svg'
                                    alt='icon'
                                    className='mCS_img_loaded'
                                  />
                                </span>
                              </div>
                            </div>
                            <div className='clearfix mar-btm'>
                              <span className='form-label'>
                                Table head background
                              </span>
                              <div className='input-group'>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='text-color'
                                />
                                <span className='input-group-addon'>
                                  <img
                                    src='images/color-wheel-icon.svg'
                                    alt='icon'
                                    className='mCS_img_loaded'
                                  />
                                </span>
                              </div>
                            </div>
                            <div className='clearfix mar-btm'>
                              <span className='form-label'>
                                Table row background
                              </span>
                              <div className='input-group'>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='text-color'
                                />
                                <span className='input-group-addon'>
                                  <img
                                    src='images/color-wheel-icon.svg'
                                    alt='icon'
                                    className='mCS_img_loaded'
                                  />
                                </span>
                              </div>
                            </div>
                            <div className='clearfix mar-btm'>
                              <span className='form-label'>
                                Table border color
                              </span>
                              <div className='input-group'>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='text-color'
                                />
                                <span className='input-group-addon'>
                                  <img
                                    src='images/color-wheel-icon.svg'
                                    alt='icon'
                                    className='mCS_img_loaded'
                                  />
                                </span>
                              </div>
                            </div>
                            <span className='form-label fw-sbold w-100 mar-btm'>
                              Table Label
                            </span>
                            <button className='btn btn-blue btn-img mar-btm'>
                              <img
                                src='images/add-circular-icon.svg'
                                alt='icon'
                                className='mCS_img_loaded'
                              />
                              Add Label
                            </button>
                            <div className='clearfix mar-btm'>
                              <span className='editor-label'>
                                <label className='switch'>
                                  <input
                                    type='checkbox'
                                    defaultChecked='checked'
                                  />
                                  <span className='drag-ball'>
                                    <span className='off' />
                                    <span className='on' />
                                  </span>
                                </label>
                                <span className='form-label'>No</span>
                              </span>
                              <div className='input-group'>
                                <span className='input-group-addon addon-left fw-500'>
                                  Label
                                </span>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='label'
                                  defaultValue='No'
                                />
                              </div>
                            </div>
                            <div className='clearfix mar-btm'>
                              <span className='editor-label'>
                                <label className='switch'>
                                  <input
                                    type='checkbox'
                                    defaultChecked='checked'
                                  />
                                  <span className='drag-ball'>
                                    <span className='off' />
                                    <span className='on' />
                                  </span>
                                </label>
                                <span className='form-label'>Item</span>
                              </span>
                              <div className='input-group'>
                                <span className='input-group-addon addon-left fw-500'>
                                  Label
                                </span>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='label'
                                  defaultValue='Item & Description'
                                />
                              </div>
                            </div>
                            <div className='clearfix mar-btm'>
                              <span className='editor-label'>
                                <label className='switch'>
                                  <input
                                    type='checkbox'
                                    defaultChecked='checked'
                                  />
                                  <span className='drag-ball'>
                                    <span className='off' />
                                    <span className='on' />
                                  </span>
                                </label>
                                <span className='form-label'>Price Each</span>
                              </span>
                              <div className='input-group'>
                                <span className='input-group-addon addon-left fw-500'>
                                  Label
                                </span>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='label'
                                  defaultValue='Price Each'
                                />
                              </div>
                            </div>
                            <div className='clearfix mar-btm'>
                              <span className='editor-label'>
                                <label className='switch'>
                                  <input
                                    type='checkbox'
                                    defaultChecked='checked'
                                  />
                                  <span className='drag-ball'>
                                    <span className='off' />
                                    <span className='on' />
                                  </span>
                                </label>
                                <span className='form-label'>Quantity</span>
                              </span>
                              <div className='input-group'>
                                <span className='input-group-addon addon-left fw-500'>
                                  Label
                                </span>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='label'
                                  defaultValue='Qty'
                                />
                              </div>
                            </div>
                            <div className='clearfix mar-btm'>
                              <span className='editor-label'>
                                <label className='switch'>
                                  <input
                                    type='checkbox'
                                    defaultChecked='checked'
                                  />
                                  <span className='drag-ball'>
                                    <span className='off' />
                                    <span className='on' />
                                  </span>
                                </label>
                                <span className='form-label'>Tax</span>
                              </span>
                              <div className='input-group'>
                                <span className='input-group-addon addon-left fw-500'>
                                  Label
                                </span>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='label'
                                  defaultValue='Tax'
                                />
                              </div>
                            </div>
                            <div className='clearfix'>
                              <span className='editor-label'>
                                <label className='switch'>
                                  <input
                                    type='checkbox'
                                    defaultChecked='checked'
                                  />
                                  <span className='drag-ball'>
                                    <span className='off' />
                                    <span className='on' />
                                  </span>
                                </label>
                                <span className='form-label'>Total</span>
                              </span>
                              <div className='input-group'>
                                <span className='input-group-addon addon-left fw-500'>
                                  Label
                                </span>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='label'
                                  defaultValue='Total'
                                />
                              </div>
                            </div>
                          </div>
                          <div className='form-group clearfix'>
                            <span className='form-label fw-sbold w-100 mar-btm'>
                              Total Properties
                            </span>
                            <div className='clearfix mar-btm'>
                              <span className='editor-label'>
                                <label className='switch'>
                                  <input
                                    type='checkbox'
                                    defaultChecked='checked'
                                  />
                                  <span className='drag-ball'>
                                    <span className='off' />
                                    <span className='on' />
                                  </span>
                                </label>
                                <span className='form-label'>Sub Total</span>
                              </span>
                              <div className='input-group'>
                                <span className='input-group-addon addon-left fw-500'>
                                  Label
                                </span>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='label'
                                  defaultValue='Sub Total'
                                />
                              </div>
                            </div>
                            <div className='clearfix mar-btm'>
                              <span className='editor-label'>
                                <label className='switch'>
                                  <input
                                    type='checkbox'
                                    defaultChecked='checked'
                                  />
                                  <span className='drag-ball'>
                                    <span className='off' />
                                    <span className='on' />
                                  </span>
                                </label>
                                <span className='form-label'>Tax</span>
                              </span>
                              <div className='input-group'>
                                <span className='input-group-addon addon-left fw-500'>
                                  Label
                                </span>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='label'
                                  defaultValue='Tax'
                                />
                              </div>
                            </div>
                            <div className='clearfix mar-btm'>
                              <span className='editor-label'>
                                <label className='switch'>
                                  <input
                                    type='checkbox'
                                    defaultChecked='checked'
                                  />
                                  <span className='drag-ball'>
                                    <span className='off' />
                                    <span className='on' />
                                  </span>
                                </label>
                                <span className='form-label'>Grand Total</span>
                              </span>
                              <div className='input-group'>
                                <span className='input-group-addon addon-left fw-500'>
                                  Label
                                </span>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='label'
                                  defaultValue='Grand Total'
                                />
                              </div>
                            </div>
                            <div className='clearfix'>
                              <span className='editor-label'>
                                <label className='switch'>
                                  <input
                                    type='checkbox'
                                    defaultChecked='checked'
                                  />
                                  <span className='drag-ball'>
                                    <span className='off' />
                                    <span className='on' />
                                  </span>
                                </label>
                                <span className='form-label'>
                                  Amount in Words
                                </span>
                              </span>
                              <div className='input-group'>
                                <span className='input-group-addon addon-left fw-500'>
                                  Label
                                </span>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='label'
                                  defaultValue='Amount in Words'
                                />
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                      <div id='tab-footer' className='tab-pane fade'>
                        <form className='custom-form'>
                          <div className='form-group clearfix'>
                            <span className='form-label fw-sbold w-100 mar-btm'>
                              Footer Options
                            </span>
                            <div className='clearfix mar-btm'>
                              <span className='form-label'>Text Size</span>
                              <div className='input-group'>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='text-size'
                                />
                                <span className='input-group-addon fw-500'>
                                  pt
                                </span>
                              </div>
                            </div>
                            <div className='clearfix'>
                              <span className='form-label'>Text Color</span>
                              <div className='input-group'>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='text-color'
                                />
                                <span className='input-group-addon'>
                                  <img
                                    src='images/color-wheel-icon.svg'
                                    alt='icon'
                                    className='mCS_img_loaded'
                                  />
                                </span>
                              </div>
                            </div>
                          </div>
                          <div className='form-group clearfix'>
                            <span className='editor-label'>
                              <label className='switch'>
                                <input
                                  type='checkbox'
                                  defaultChecked='checked'
                                />
                                <span className='drag-ball'>
                                  <span className='off' />
                                  <span className='on' />
                                </span>
                              </label>
                              <span className='form-label'>
                                Thanking Message
                              </span>
                            </span>
                            <textarea
                              className='form-control'
                              placeholder='Thank you'
                              defaultValue={''}
                            />
                          </div>
                          <div className='form-group clearfix'>
                            <span className='editor-label'>
                              <label className='switch'>
                                <input
                                  type='checkbox'
                                  defaultChecked='checked'
                                />
                                <span className='drag-ball'>
                                  <span className='off' />
                                  <span className='on' />
                                </span>
                              </label>
                              <span className='form-label'>
                                Terms &amp; Conditions
                              </span>
                            </span>
                            <div className='input-group mar-btm'>
                              <span className='input-group-addon addon-left fw-500'>
                                Label
                              </span>
                              <input
                                type='text'
                                className='form-control'
                                name='label'
                                defaultValue='Terms & Conditions'
                              />
                            </div>
                            <textarea
                              className='form-control'
                              placeholder='Terms & Conditions'
                              defaultValue={''}
                            />
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  id='mCSB_1_scrollbar_vertical'
                  className='mCSB_scrollTools mCSB_1_scrollbar mCS-light mCSB_scrollTools_vertical mCSB_scrollTools_onDrag_expand'
                  style={{ display: 'block' }}
                >
                  <div className='mCSB_draggerContainer'>
                    <div
                      id='mCSB_1_dragger_vertical'
                      className='mCSB_dragger'
                      style={{
                        position: 'absolute',
                        minHeight: '30px',
                        display: 'block',
                        height: '15px',
                        maxHeight: '100px',
                        top: '0px'
                      }}
                    >
                      <div
                        className='mCSB_dragger_bar'
                        style={{ lineHeight: '30px' }}
                      />
                    </div>
                    <div className='mCSB_draggerRail' />
                  </div>
                </div>
              </div>
              <div className='btn-wrap text-right hidden-xs'>
                <button
                  className='btn btn-lightgray'
                  type='button'
                  onClick={this.increase_logo_size}
                >
                  Preview
                </button>
                <div className='custom-select-drop dropdown btn'>
                  <a
                    aria-expanded='false'
                    aria-haspopup='true'
                    role='button'
                    data-toggle='dropdown'
                    className='dropdown-toggle btn btn-green'
                    href='javascript:;'
                  >
                    <span id='selected'>Save</span>
                    <span className='caret' />
                  </a>
                  <ul className='dropdown-menu'>
                    <li className='active'>
                      <a
                        href='javascript:;'
                        // data-toggle='modal'
                        // data-target='#successModal'
                      >
                        Save
                      </a>
                    </li>
                    <li>
                      <a href='javascript:;'>Save &amp; Continue</a>
                    </li>
                    <li>
                      <a href='javascript:;'>Save as Copy</a>
                    </li>
                    <li>
                      <a href='javascript:;'>Save Draft</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            {/* Builder Left Ends here */}
            <div class='builder-right'>
              {this.state.html_content != ''
                ? parse(this.state.html_content)
                : ''}
            </div>
          </div>
        </div>
        {/* Main Wrapper Ends here */}
        {/* Modal Wrapper Starts here */}
        <div className='modal fade' id='successModal' role='dialog'>
          <div className='modal-dialog modal-md'>
            {/* Modal content*/}
            <button
              type='button'
              className='close hidden-xs'
              data-dismiss='modal'
            >
              <img
                className='img-responsive'
                src='images/close-red.svg'
                alt='icon'
              />
            </button>
            <div className='modal-content'>
              <div className='modal-body text-center success-modal'>
                <div className='pop-icon'>
                  <img src='images/template-success-icon.png' alt='icon' />
                </div>
                <h3>Awesome!</h3>
                <p className='fw-500'>
                  Your invoice template has been saved successfully
                </p>
                <button className='btn btn-green' data-dismiss='modal'>
                  OK
                </button>
              </div>
            </div>
          </div>
        </div>
        {/* Modal Wrapper Ends here */}
      </div>
    )
  }
}

export default custom_editable_template
