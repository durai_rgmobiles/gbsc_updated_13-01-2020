import React, { Component } from "react";
import jQuery from "jquery";
import DatePicker from "react-datepicker";
import FetchAllApi from "./api_links/fetch_all_api.js";

import "react-datepicker/dist/react-datepicker.css";

export default class registerPayment extends React.Component {
  constructor(props) {
    super(props);
    //const { history } = this.props;
    this.state = {
      standardAmount: 11.28,
      itemTotal: "13.46"
    };
  }
  callMe = () => {
    let k = this.state.standardAmount + 2.18;
    let res = k.toFixed(2);
    this.setState({ itemTotal: res });
  };
  componentDidMount() {
    var THIS = this;
    jQuery(document).on("click", ".callthislist", function() {
      THIS.setState({
        standardAmount: 11.28
      });
      THIS.callMe();
    });

    jQuery(document).on("click", ".callme2", function() {
      THIS.setState({
        standardAmount: 15.28
      });
      THIS.callMe();
    });

    jQuery(document).on("click", ".callme3", function() {
      THIS.setState({
        standardAmount: 22.28
      });
      THIS.callMe();
    });

    jQuery(document).ready(function() {
      jQuery("button").click(function() {
        jQuery("#paynow").addClass("hide");
        jQuery("#cart").removeClass("hide");
      });

      jQuery("a").click(function() {
        jQuery("#cart").addClass("hide");
        jQuery("#paynow").removeClass("hide");
      });

      jQuery(".plan-list > ul > li.active .plan-detail").css(
        "display",
        "block"
      );
      jQuery(".plan-list > ul > li").click(function() {
        jQuery(this)
          .siblings()
          .removeClass("active")
          .find(".plan-detail")
          .slideUp();
        jQuery(this)
          .addClass("active")
          .find(".plan-detail")
          .slideDown();
      });
    });
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  render() {
    console.log("qwerty", this.state.entitynum);
    return (
      <div>
        <div class="container-fluid">
          <div class="row dflex">
            <div class="col-md-7 plan-left">
              <div class="register-head col-md-12 col-xs-12">
                <a href="javascript:;" class="back hidden-xs">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="18.5"
                    height="14.249"
                    viewBox="0 0 18.5 14.249"
                  >
                    <g
                      id="left-arrow_2_"
                      data-name="left-arrow (2)"
                      transform="translate(0 -58.83)"
                    >
                      <g
                        id="Group_25"
                        data-name="Group 25"
                        transform="translate(0 65.207)"
                      >
                        <g
                          id="Group_24"
                          data-name="Group 24"
                          transform="translate(0 0)"
                        >
                          <path
                            id="Path_19"
                            data-name="Path 19"
                            d="M17.753,235.318H.747a.747.747,0,0,0,0,1.495H17.753a.747.747,0,0,0,0-1.495Z"
                            transform="translate(0 -235.318)"
                          />
                        </g>
                      </g>
                      <g
                        id="Group_27"
                        data-name="Group 27"
                        transform="translate(0 58.83)"
                      >
                        <g
                          id="Group_26"
                          data-name="Group 26"
                          transform="translate(0 0)"
                        >
                          <path
                            id="Path_20"
                            data-name="Path 20"
                            d="M1.8,65.954l5.849-5.849A.747.747,0,1,0,6.6,59.049L.219,65.426a.747.747,0,0,0,0,1.057L6.6,72.86A.747.747,0,1,0,7.653,71.8Z"
                            transform="translate(0 -58.83)"
                          />
                        </g>
                      </g>
                    </g>
                  </svg>
                </a>
                <span>
                  You're Almost Done!
                  <br />
                  <small>Choose your plan & pay</small>
                </span>
                <div class="pull-right nav-brand">
                  <img
                    class="img-responsive"
                    src="images/logo-genie.png"
                    alt="Logo"
                  />
                </div>
              </div>
              <div class="plan-list col-md-12 col-xs-12">
                <ul class="list-unstyled col-md-12 col-xs-12 pad-no">
                  <li class="col-md-12 col-xs-12 active callthislist">
                    <div class="plan-title">
                      <span>
                        <img src="images/circle-tick-green.svg" alt="icon" />
                        Standard
                      </span>
                      <span class="price">$ 11.28</span>
                    </div>
                    <div class="plan-detail">
                      <p>
                        Sed ut perspiciatis unde omnis iste natus error sit
                        voluptatem accusantium doloremque laudantium, totam rem
                        aperiam
                      </p>
                      <ul class="list-unstyled">
                        <li>
                          <img src="images/blue-tick.svg" alt="icon" />
                          Accounts Receivables
                        </li>
                        <li>
                          <img src="images/blue-tick.svg" alt="icon" />
                          Accounts Payable
                        </li>
                        <li>
                          <img src="images/blue-tick.svg" alt="icon" />
                          Bill of Entry
                        </li>
                        <li>
                          <img src="images/blue-tick.svg" alt="icon" />
                          GST Filing
                        </li>
                        <li>
                          <img src="images/blue-tick.svg" alt="icon" />
                          Multi Currency Handling
                        </li>
                        <li>
                          <img src="images/blue-tick.svg" alt="icon" />
                          Inventory Management
                        </li>
                        <li>
                          <img src="images/blue-tick.svg" alt="icon" />
                          Predefined User Roles
                        </li>
                        <li>
                          <img src="images/blue-tick.svg" alt="icon" />
                          Import PDF Statement
                        </li>
                      </ul>
                      <a>
                        {" "}
                        <span class="price-box">$ 11.28</span>
                      </a>
                      <span class="note">
                        *Billed annually or $ {this.state.standardAmount} month-to-month
                      </span>
                    </div>
                  </li>
                  <li class="col-md-12 col-xs-12 callme2">
                    <div class="plan-title">
                      <span>
                        <img src="images/circle-tick-green.svg" alt="icon" />
                        Advanced
                      </span>
                      <a>
                        <span class="price">$ 15.28</span>
                      </a>
                    </div>
                    <div class="plan-detail">
                      <p>
                        Sed ut perspiciatis unde omnis iste natus error sit
                        voluptatem accusantium doloremque laudantium, totam rem
                        aperiam
                      </p>
                      <ul class="list-unstyled">
                        <li>
                          <img src="images/blue-tick.svg" alt="icon" />
                          Accounts Receivables
                        </li>
                        <li>
                          <img src="images/blue-tick.svg" alt="icon" />
                          Accounts Payable
                        </li>
                        <li>
                          <img src="images/blue-tick.svg" alt="icon" />
                          Bill of Entry
                        </li>
                        <li>
                          <img src="images/blue-tick.svg" alt="icon" />
                          GST Filing
                        </li>
                        <li>
                          <img src="images/blue-tick.svg" alt="icon" />
                          Multi Currency Handling
                        </li>
                        <li>
                          <img src="images/blue-tick.svg" alt="icon" />
                          Inventory Management
                        </li>
                        <li>
                          <img src="images/blue-tick.svg" alt="icon" />
                          Predefined User Roles
                        </li>
                        <li>
                          <img src="images/blue-tick.svg" alt="icon" />
                          Import PDF Statement
                        </li>
                      </ul>
                      <a></a> <span class="price-box">$ 15.28</span>
                      <span class="note">
                        *Billed annually or $ {this.state.standardAmount} month-to-month
                      </span>
                    </div>
                  </li>
                  <li class="col-md-12 col-xs-12 callme3">
                    <div class="plan-title">
                      <span>
                        <img src="images/circle-tick-green.svg" alt="icon" />
                        Premium
                      </span>
                      <span class="price">$ 22.28</span>
                    </div>
                    <div class="plan-detail">
                      <p>
                        Sed ut perspiciatis unde omnis iste natus error sit
                        voluptatem accusantium doloremque laudantium, totam rem
                        aperiam
                      </p>
                      <ul class="list-unstyled">
                        <li>
                          <img src="images/blue-tick.svg" alt="icon" />
                          Accounts Receivables
                        </li>
                        <li>
                          <img src="images/blue-tick.svg" alt="icon" />
                          Accounts Payable
                        </li>
                        <li>
                          <img src="images/blue-tick.svg" alt="icon" />
                          Bill of Entry
                        </li>
                        <li>
                          <img src="images/blue-tick.svg" alt="icon" />
                          GST Filing
                        </li>
                        <li>
                          <img src="images/blue-tick.svg" alt="icon" />
                          Multi Currency Handling
                        </li>
                        <li>
                          <img src="images/blue-tick.svg" alt="icon" />
                          Inventory Management
                        </li>
                        <li>
                          <img src="images/blue-tick.svg" alt="icon" />
                          Predefined User Roles
                        </li>
                        <li>
                          <img src="images/blue-tick.svg" alt="icon" />
                          Import PDF Statement
                        </li>
                      </ul>
                      <span class="price-box">$ 22.28</span>
                      <span class="note">
                        *Billed annually or $ {this.state.standardAmount} month-to-month
                      </span>
                    </div>
                  </li>
                </ul>
              </div>
            </div>

            <div class="col-md-5 col-xs-12 payment-right">
              <div class="col-md-12 col-xs-12">
                <div
                  id="paynow"
                  class="cart-enclose col-md-12 col-xs-12 pad-no"
                >
                  <div class="text-center cart-head col-md-12 col-xs-12 pad-no">
                    <img src="images/notepad-icon.svg" alt="Icon" />
                    <span>Your subscription details</span>
                  </div>
                  <table class="table cart-table">
                    <thead>
                      <tr>
                        <th>Item</th>
                        <th class="text-center">Total</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          Standard Plan <small>(With Genie Accountant)</small>
                          <br />
                          <select class="select-dropdown selectpicker">
                            <option>Annual Plan, Paid Monthly</option>
                            <option>Annual Plan, Paid Annual</option>
                            <option>Monthly Plan, Paid Annual</option>
                            <option>Monthly Plan, Paid Monthly</option>
                          </select>
                        </td>
                        <td class="amount text-center">
                          $ {this.state.standardAmount}
                          <small>/mo</small>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table class="table total no-borderd">
                    <tbody>
                      <tr>
                        <td>Item Total</td>
                        <td>$ {this.state.standardAmount}</td>
                      </tr>
                      <tr>
                        <td>Tax</td>
                        <td>$ 02.18</td>
                      </tr>
                      <tr>
                        <td>Grand Total</td>
                        <td class="grand-total">$ {this.state.itemTotal}</td>
                      </tr>
                    </tbody>
                  </table>
                  <div class="col-md-12 col-xs-12 text-center">
                    <button class="btn btn-blue btn-rounded">Pay Now</button>
                  </div>
                </div>

                <div id="cart" class="hide col-md-12 col-xs-12 pad-no">
                  <div class="text-center cart-head col-md-12 col-xs-12 pad-no">
                    <img src="images/creditcard-icon.svg" alt="Icon" />
                    <span>
                      Payable Amount{" "}
                      <span class="amount">$ {this.state.itemTotal}</span>
                    </span>
                  </div>
                  <div class="col-md-12 col-xs-12 pad-no">
                    <form class="custom-form pay-form">
                      <div class="form-group">
                        <label>Card Holder's Name</label>
                        <input
                          type="text"
                          name="c-name"
                          placeholder="Eg: John Smith"
                          class="form-control"
                        />
                      </div>
                      <div class="form-group">
                        <label>Card Number</label>
                        <input
                          type="text"
                          name="c-num"
                          placeholder="XXXX XXXX XXXX XXXX"
                          class="form-control"
                        />
                        <div class="card-type">
                          <img src="images/visa-icon.svg" alt="icon" />
                        </div>
                      </div>
                      <div class="col-md-12 col-xs-12 pad-no">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Expiration</label>
                              <input
                                type="text"
                                name="c-exp"
                                placeholder="MM/YYYY"
                                class="form-control"
                              />
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>CVV / CVC</label>
                              <input
                                type="text"
                                name="c-cvv"
                                placeholder=""
                                class="form-control"
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12 col-xs-12 pad-no text-center">
                        <button class="btn btn-blue full-w">
                          Pay Now $ {this.state.itemTotal}
                        </button>
                        <a href="javascript:;" class="text-link">
                          <img src="images/back-arrow-small.svg" alt="icon" />
                          Back
                        </a>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
