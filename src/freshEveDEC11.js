import React from 'react'
import LeftSidebar from './left_sidebar.js'
import Footer from './footer.js'

import Topbar from './topbar.js'

import FetchAllApi from './api_links/fetch_all_api.js'

import jQuery from 'jquery'
import DatePicker from 'react-date-picker'
//npm i react-date-picker

import { ContextMenu, MenuItem, ContextMenuTrigger } from 'react-contextmenu'

class data_tagging extends React.Component {
  constructor (props) {
    super(props)
    //const { history } = this.props;
    this.state = {
      tablerows: [],
      isChecked: false,
      logged_user_id: localStorage.getItem('logged_user_id'),
      logged_client_id: localStorage.getItem('logged_client_id'),
      logged_role_id: localStorage.getItem('logged_role_id'),
      logged_user_name: localStorage.getItem('logged_user_name'),
      logged_user_email: localStorage.getItem('logged_user_email'),
      logged_user_phone: localStorage.getItem('logged_user_phone'),
      logged_user_image: localStorage.getItem('logged_user_image'),
      logged_company_name: localStorage.getItem('logged_company_name'),
      get_file_path: [],
      add_cmnt_msg: '',
      file_comments: [],
      list_id: this.props.match.params.list_id,
      is_called: false,
      sub_comments: [],
      checkSubComments: false,
      clickedParentId: null,
      attachment_file: [],
      attachment_file_jquery: [],
      attachment_file_length: 0,
      attachment_fileName: [],
      currencies: [],
      item_total_home_currency: '',
      tax_amount_foreign_currency: '',
      grand_total_foreign_currency: '',
      grand_total_home_currency: '',
      item_total_foreign_currency: '',
      ToCurrency: 'USD',
      company_name: '',
      invoice_no: '',
      incorport_date: '2019-04-12',
      address:'No address entered',
      account_category: '',
      exchange_value: '',
      rows: ['row 1'],
      coulmns: [],
      myarray: [],
      isTax: true,
      default_category_list: [],
      selected: '',
      selectedindex: '',
      balancesheetlist: [],
      balance_list_selected: '',
      changeme: '',
      date: new Date(),
      balance_sheet_category_name: '',
      balance_sheet_category_id: '',
      categorylist: [],
      sub_categorylist: [],
      categoryname: '',
      sub_categoryname: 'Choose sub category',
      category_id: '',
      sub_category_id: '',
      Accounttype: [],
      Account_type_name: '',
      Account_type_id: '',
      Currency_name: 'USD',
      account_name: '',
      isAdd: false,
      search_key: '',
      currency_clone: [],
      showAddmore: false,
      isClose: false,
      isClose1: false,
      gst_list: [],
      sales_tax_name: 'Zero-rated supplies',
      sales_tax_rate: 0,
      sales_tax_type: 1,
      search_key_gst: '',
      selected_rate_type: '%',
      maximum_chr_lngth: 4,
      sales_tax_code: '',
      rate_entered: '',
      salesTax_name_entered: '',
      modal_info_msg: '',
      show_succes: false,
      rate_type: 1,
      coulmn_header: [],
      selectedOption: 'option2',
      initial_value:0,
      specific_id_delete:'',
      isCompany_name:true,
      isInvoice_no:true,
      isBalance_sheet_category_name:true,
      isTable_notEmpty:false
    }

    this.loadFile = this.loadFile.bind(this)
    this.myDivToFocus = React.createRef()

  }
  onChange = date => this.setState({ date })

  handleOnClick = (event) => {
    if(this.myDivToFocus.current){
        this.myDivToFocus.current.scrollIntoView({ 
           behavior: "smooth", 
           block: "center"
        })
      }
    
}

  componentDidMount () {
    jQuery(document).ready(function () {
      var myform = jQuery('#myform'),
        iter = 0
      jQuery('#btnAddCol').click(function () {
        myform.find('tr').each(function () {
          var trow = jQuery(this)
          trow.append('<td contentEditable>Col' + iter + '</td>')
        })
        iter += 1
      })
    })

    jQuery(document).ready(function () {
      jQuery('.has-sub').click(function () {
        jQuery(this)
          .parent()
          .addClass('active')
          .next('.sub-menu')
          .slideToggle()
      })
      jQuery('.search-btn').click(function () {
        jQuery('.hdr-search').addClass('active')
      })
      jQuery('.hdr-search .close-icon').click(function () {
        jQuery('.hdr-search').removeClass('active')
      })
      // jQuery(".select-picker").selectpicker();
      jQuery('.label-enclose .label').click(function () {
        jQuery(this).toggleClass('active')
      })

      jQuery('.nav-brand-res').click(function () {
        jQuery('.left-navbar').addClass('active')
      })
      jQuery('.menu-close').click(function () {
        jQuery('.left-navbar').removeClass('active')
      })
      jQuery('.custom-select-drop .dropdown-menu a').click(function () {
        // alert()
        jQuery('.open.custom-select-drop .dropdown-menu li.active').removeClass(
          'active'
        )
        jQuery(this)
          .parent('li')
          .addClass('active')
        jQuery('.open #selected').text(jQuery(this).text())
      })

      jQuery('.tbl_drop_down').on('click', function () {
        jQuery('.form-table').addClass('ovrFlwRmve')
      })
    })
  }

  update_search_keyword = event => {
    this.setState({ search_key_gst: event.target.value }, () => {
      this.get_gst_list()
    })
  }

  get_gst_list = () => {
    let country_code = this.state.country_code
    //alert(country_code)
    let keyword = this.state.search_key_gst
    FetchAllApi.get_gst_list(country_code, keyword, (err, response) => {
      console.log('defaultcategorylist', response)
      //alert(response.message)
      if (response.status === 1) {
        this.setState({
          gst_list: response.list
        })
      } else {
        this.setState({
          gst_list: []
        })
      }
    })
  }

  removeOverFlow (e) {
    jQuery('.form-table').addClass('ovrFlwRmve')
  }
  control_addButton = () => {
    let item_check = jQuery(
      `#coulmn${this.state.rows.length - 1}${this.state.coulmns.length - 1}`
    ).val()

    if (
      this.state.myarray.length > this.state.rows.length - 1 &&
      this.state.myarray[this.state.myarray.length - 1].price > 0 &&
      this.state.myarray[this.state.myarray.length - 1].item_name.length > 0 &&
      this.state.myarray[this.state.myarray.length - 1].category_id.length >
        0 &&
      item_check != ''
    ) {
      return (
        <div class='form-group col-md-12 mar-b-no pad-no'>
          <a href='javascript:;' class='add-input' onClick={this.addRow}>
            ADD ROW
          </a>
        </div>
      )
    }
  }

  addRow = () => {
    if (
      this.state.myarray.length > this.state.rows.length - 1 &&
      this.state.myarray[this.state.myarray.length - 1].price > 0 &&
      this.state.myarray[this.state.myarray.length - 1].item_name.length > 0 &&
      this.state.myarray[this.state.myarray.length - 1].category_id.length > 0
    ) {
      var rows = this.state.rows
      rows.push('row' + (this.state.initial_value + 1))

      this.setState({ isAdd: false, initial_value: this.state.initial_value + 1 })

      this.setState({ rows: rows })
    } else {
      this.setState({ isAdd: true })
    }
  }

  add_coulmn = () => {
    let item_check = jQuery(
      `#coulmn${this.state.rows.length - 1}${this.state.coulmns.length - 1}`
    ).val()
    if (item_check != '') {
      var coulmns = this.state.coulmns
      coulmns.push('new row')
      this.setState({ coulmns: coulmns })
    }
  }
  handleChange_gst_type = event => {
    if (this.state.selected_rate_type != 'Fixed price') {
      let entered_value = event.target.value
      // alert(entered_value)
      if (isNaN(entered_value)) {
        jQuery('#tax').val('')
      } else {
        if (entered_value > 100) {
          jQuery('#tax').val('')
        } else {
          this.setState({ rate_entered: entered_value })
        }
      }
    } else {
      let entered_value = event.target.value
      if (isNaN(entered_value)) {
        jQuery('#tax').val('')
      } else {
        this.setState({ rate_entered: entered_value })
      }
    }
  }

  defaultcategorylist_onchange = event => {
    let keyy = event.target.value
    FetchAllApi.defaultcategorylist_onchange(keyy, (err, response) => {
      console.log('defaultcategorylist', response)
      if (response.status === 1) {
        this.setState({
          default_category_list: response.list
        })
      } else {
        this.setState({
          default_category_list: []
        })
      }
    })
  }
  onChange_filter_balancesheet = event => {
    let search_key = event.target.value
    //alert(search_key)
    FetchAllApi.balancesheetlist_onchange(search_key, (err, response) => {
      console.log('defaultcategorylist', response)
      if (response.status === 1) {
        this.setState({
          balancesheetlist: response.list
        })
      } else {
        this.setState({
          balancesheetlist: []
        })
      }
    })
  }

  deafultCategoryList (e) {
    FetchAllApi.defaultcategorylist((err, response) => {
      console.log('defaultcategorylist', response)
      if (response.status === 1) {
        this.setState({
          default_category_list: response.list
        })
      } else {
      }
    })
  }
  delete_Rows = () => {
    var itemid=this.state.specific_id_delete
    var rows_actual = this.state.rows
    var myarray = this.state.myarray
    console.log('jkdghkshakd', this.state.myarray)
    if (this.state.rows.length > 1) {
      if (itemid > -1) {
        rows_actual.splice(itemid, 1)
        // myarray.splice(itemid, 1)
      }

      this.setState({ rows: rows_actual}, () => {
        this.handleChangeItems(0, this.state.rows.length - 1)
      })

      console.log('After_delete_par_row',this.state.rows)
    } else {
      jQuery('#item0').val('')
      jQuery('#quantity0').val('')
      jQuery('#unit_price0').val('')
      jQuery('.no-bg').val('')
      this.setState({ myarray: [] }, () => {
        this.handleChangeItems(0, this.state.rows.length - 1)
      })
    }
    window.jQuery('#modal_delete').modal('hide');


  }




  get_currencies = () => {
    fetch('https://api.exchangerate-api.com/v4/latest/SGD')
      .then(response => response.json())
      .then(data => {
        const currencyAr = []
        let first = data.rates
        for (const key in first) {
          currencyAr.push(key)
        }
        this.setState({ currencies: currencyAr, currency_clone: currencyAr })
      })
  }

  UNSAFE_componentWillMount () {
    let client_Id = this.state.logged_client_id
    //alert(client_Id)
    FetchAllApi.get_country_id(client_Id, (err, response) => {
      // alert(response.country_id)
      if (response.status === 1) {
        this.setState(
          {
            country_code: response.country_id
          },
          () => this.get_gst_list()
        )
      }
    })
    this.get_currencies()

    FetchAllApi.get_categories((err, response) => {
      console.log('add comment', response.list)
      if (response.status === 1) {
        // alert('Got list :)');
        this.setState({ categorylist: response.list })
      } else {
      }
    })
    this.deafultCategoryList(this)

    FetchAllApi.balancesheetlist((err, response) => {
      console.log('defaultcategorylist', response)
      if (response.status === 1) {
        this.setState({
          balancesheetlist: response.list
        })
      } else {
      }
    })

    jQuery('title').html('Data Tagging | GBSC')

    if (
      this.state.logged_user_id === '' ||
      this.state.logged_user_id === 'null' ||
      this.state.logged_user_id === 'undefined'
    ) {
      this.props.history.push('/')
    }

    var file_id = this.props.match.params.file_id

    FetchAllApi.getFilePath(file_id, (err, response) => {
      //console.log('\Article Data', response);
      if (response.status === 1) {
        this.setState({
          get_file_path: response.file_path
        })
      } else {
      }
    })

    this.getCommments(file_id)
  }

  getCommments (file_id) {
    FetchAllApi.getFileCmnts(file_id, (err, response) => {
      //console.log('\Article Data', response);
      if (response.status === 1) {
        this.setState({
          file_comments: response.details
        })
      } else {
      }
    })
  }

  handleClick (e, data) {
    console.log(data.foo)
  }

  cancel_gst_modal=() => {
    jQuery('#sales_tax_code').val('')
    jQuery('#sales_tax_name').val('')
    jQuery('#sales_tax_name').val('')
    jQuery('#tax').val('')

    this.setState({
      modal_info_msg: '',
      selectedOption: 'option2'
    })
  }

  loadFile (e) {
    var files = e.target.files
    this.setState({ attachment_file_length: files.length })

    if (files.length > 0) {
      var fileArra = this.state.attachment_file
      //var fileThumbArra = this.state.imgThumb;

      for (var i = 0; i < files.length; i++) {
        fileArra.push(e.target.files[i])
        this.setState({
          //   selectedFile:URL.createObjectURL(e.target.files[i]),
          attachment_file: fileArra
        })
      }
    }

    console.log('attachments_length', this.state.attachment_file.length)
  }

  addCommentFunc (e) {
    e.preventDefault()
    var pstCommnt = jQuery('#comment_text').val()
    var user_id = parseFloat(this.state.logged_user_id)
    var list_id = parseFloat(this.props.match.params.list_id)
    var file_id = parseFloat(this.props.match.params.file_id)
    var parent_comment_id = 0
    var attachments = ''

    FetchAllApi.addComment(
      pstCommnt,
      user_id,
      list_id,
      file_id,
      attachments,
      parent_comment_id,
      (err, response) => {
        console.log('add scomment', response.status)
        if (response.status === 1) {
          // alert('success');
          // window.location.reload();
          this.setState({
            add_cmnt_msg: response.message
          })
          jQuery('.comment-sec')[0].reset()

          jQuery('.resp_msg').fadeIn(2000)
          setTimeout(function () {
            jQuery('.resp_msg').fadeOut(2000)
          }, 8000)

          this.getCommments(file_id)
        } else {
          this.setState({
            add_cmnt_msg: response.message
          })
          jQuery('.resp_msg').fadeIn(2000)
          setTimeout(function () {
            jQuery('.resp_msg').fadeOut(2000)
          }, 8000)
        }
      }
    )
  }
  update_rate_type=() => {
    jQuery(
      '#tax'
    ).val('')
    this.setState({
      selected_rate_type:
        '%',
      maximum_chr_lngth: 4
    })
  }
  update_rate_fixed=() => {
    jQuery(
      '#tax'
    ).val('')
    this.setState({
      selected_rate_type:
        'Fixed price',
      maximum_chr_lngth: 100
    })
  }
  addSubCommentFunc (e) {
    e.preventDefault()
    var pstCommnt = e.target.elements.reply_txt.value
    var user_id = parseFloat(this.state.logged_user_id)
    var list_id = parseFloat(this.props.match.params.list_id)
    var file_id = parseFloat(this.props.match.params.file_id)
    var parent_comment_id = e.target.elements.parent_comment_id.value
    if (this.state.attachment_file.length > 0) {
      var attachments = this.state.attachment_file
    } else {
      var attachments = ''
    }

    this.addSubComment(
      pstCommnt,
      user_id,
      list_id,
      file_id,
      attachments,
      parent_comment_id
    )
  }

  addSubComment (
    pstCommnt,
    user_id,
    list_id,
    file_id,
    attachments,
    parent_comment_id
  ) {
    console.log('attachments', attachments)

    FetchAllApi.addComment(
      pstCommnt,
      user_id,
      list_id,
      file_id,
      attachments,
      parent_comment_id,
      (err, response) => {
        console.log('add comment', response.status)
        if (response.status === 1) {
          this.setState({
            add_cmnt_msg: response.message,
            attachment_file: []
          })
          jQuery('.reply_txt_cls').val('')
          jQuery('.add_img').val('')
          jQuery('.files_txt').html('')

          jQuery('.resp_msg').fadeIn(2000)
          setTimeout(function () {
            jQuery('.resp_msg').fadeOut(2000)
          }, 8000)

          this.getCommments(file_id)
          this.replyLink(parent_comment_id, 'replyBtn')
        } else {
          this.setState({
            add_cmnt_msg: response.message
          })
          jQuery('.resp_msg').fadeIn(2000)
          setTimeout(function () {
            jQuery('.resp_msg').fadeOut(2000)
          }, 8000)
        }
      }
    )
  }

  dataTaggingFunc (list_id, file_id) {
    this.props.history.push('/data_tagging/' + list_id + '/' + file_id)
    window.scrollTo(0, 0)
  }

  logoutLink () {
    localStorage.setItem('logged_user_id', '')
    localStorage.setItem('logged_client_id', '')
    localStorage.setItem('logged_role_id', '')
    localStorage.setItem('logged_user_name', '')
    localStorage.setItem('logged_user_email', '')
    localStorage.setItem('logged_user_phone', '')
    localStorage.setItem('logged_user_image', '')
    localStorage.setItem('logged_company_name', '')

    this.props.history.push('/')
  }

  routedChange (parameter) {
    this.props.history.push('/' + parameter)
    window.scrollTo(0, 0)
  }

  pageLink (page_slug) {
    this.props.history.push('/' + page_slug)
  }

  updateCmmnt (e) {
    e.preventDefault()
    var comment_id = e.target.elements.comment_id.value
    var comment_text = e.target.elements.comment_text.value
    var user_id = this.state.logged_user_id
    var file_id = this.props.match.params.file_id
    var parent_comment_id = e.target.elements.parent_comment_id.value

    this.updateCommentApi(
      comment_id,
      comment_text,
      user_id,
      file_id,
      parent_comment_id
    )
  }

  updateCommentApi (
    comment_id,
    comment_text,
    user_id,
    file_id,
    parent_comment_id
  ) {
    FetchAllApi.updateComment(
      comment_id,
      comment_text,
      user_id,
      (err, response) => {
        console.log('update comment', response.status)
        if (response.status === 1) {
          this.setState({
            add_cmnt_msg: response.message
          })

          jQuery('.resp_msg').fadeIn(2000)
          setTimeout(function () {
            jQuery('.resp_msg').fadeOut(2000)
          }, 8000)

          jQuery('.comment-txt').removeClass('hide')
          jQuery('.update_cmnt').addClass('hide')

          this.getCommments(this.props.match.params.file_id)
          // if(parseFloat(parent_comment_id) > 0){
          //     this.replyFunc(parent_comment_id);
          // }
          this.replyLink(parent_comment_id, 'replyUpdt')
        } else {
          this.setState({
            add_cmnt_msg: response.message
          })
          jQuery('.resp_msg').fadeIn(2000)
          setTimeout(function () {
            jQuery('.resp_msg').fadeOut(2000)
          }, 8000)
        }
      }
    )
  }

  filter_currenciess = e => {
  // alert(e.target.value)
    var matched_terms = []
    var search_term = e.target.value
    if (search_term != '') {
      search_term = search_term.toLowerCase()
      this.state.currencies.forEach(item => {
        if (item.toLowerCase().indexOf(search_term) !== -1) {
          console.log(item)
          matched_terms.push(item)
        }
console.log('iuuuuuuuyuuuu',matched_terms)
        this.setState({ currencies: matched_terms })
      })
    } else {
      this.get_currencies()
    }
  }

  handleOptionChange = changeEvent => {
    this.setState({
      selectedOption: changeEvent.target.value
    })
  }

 modal_cancel= () => {
    jQuery('#sales_tax_code').val('')
    jQuery('#sales_tax_name').val('')
    jQuery('#sales_tax_name').val('')
    jQuery('#tax').val('')
    this.setState({ modal_info_msg: '' })
    window.jQuery('#pop-modal-1').modal('hide');

  }
  deleteComment (comment_id, list_id, parent_comment_id) {
    FetchAllApi.deleteComment(comment_id, list_id, (err, response) => {
      console.log('delete comment', response.status)
      if (response.status === 1) {
        this.setState({
          add_cmnt_msg: response.message
        })

        //jQuery(".comment-txt").removeClass('hide');
        //jQuery(".reply-form").addClass('hide');

        jQuery('.resp_msg').fadeIn(2000)
        setTimeout(function () {
          jQuery('.resp_msg').fadeOut(2000)
        }, 8000)

        //this.getCommments(this.props.match.params.file_id);
        if (parseFloat(parent_comment_id) > 0) {
          console.log('parent_comment_id123', parent_comment_id)
          this.replyLink(parseFloat(parent_comment_id), 'replyDel')
        } else {
          this.getCommments(this.props.match.params.file_id)
        }
      } else {
        this.setState({
          add_cmnt_msg: response.message
        })
        jQuery('.resp_msg').fadeIn(2000)
        setTimeout(function () {
          jQuery('.resp_msg').fadeOut(2000)
        }, 8000)
      }
    })
  }

  addNewRole = inputfromuser => {
    let input = inputfromuser
    // alert(input);
    this.setState({
      newrole: input
    })
  }
  componentDidUpdate () {
    var THIS = this
    jQuery('.edit_cmnt').click(function () {
      var text_cmnt = jQuery(this)
        .closest('.prnt_cmnt')
        .next('.comment-txt')
        .text()
      var this_cmnt_id = jQuery(this).attr('data-comment-id')
      jQuery(this)
        .closest('.prnt_cmnt')
        .next('.comment-txt')
        .addClass('hide')
      jQuery(this)
        .closest('.prnt_cmnt')
        .next('.comment-txt')
        .next('.reply-form')
        .removeClass('hide')
      jQuery('#cmmnt_txt_id' + this_cmnt_id).val(text_cmnt)
    })

    jQuery('.cancel_button').click(function () {
      jQuery(this)
        .closest('.reply-form')
        .addClass('hide')
      jQuery(this)
        .closest('.reply-form')
        .prev('.comment-txt')
        .removeClass('hide')
    })
  }
  clear_tagged_items=() => {
    this.callme()
    window.jQuery('#successModal').modal('hide');
    this.setState({isAdd:false})
    // jQuery('#successModal').removeClass('in')
    // jQuery('body').removeClass('modal-open')
    // jQuery('.modal-backdrop').removeClass(
    //   'in'
    // )
  }
  replyLink (parent_comment_id, replyToggele) {
    var THIS = this
    //alert(replyToggele+' '+typeof(parent_comment_id));
    if (replyToggele === 'replyLink') {
      jQuery('#reply_cnt' + parent_comment_id).toggleClass('in')
    }

    console.log('parent_comment_id', parent_comment_id)

    FetchAllApi.getSubCmmnts(parent_comment_id, (err, response) => {
      //console.log('\Article Data', response);
      if (response.status === 1) {
        this.setState({
          sub_comments: response.details
        })

        var comments_length = response.details.length
        var comment_list = []
        if (comments_length > 0) {
          for (var i = 0; i < comments_length; i++) {
            if (response.details[i].user_image) {
              var user_img = response.details[i].user_image
            } else {
              var user_img = '../../images/user-img-1.png'
            }

            var comment_id = response.details[i].comment_id

            if (response.details[i].sub_comment_count > 0) {
              var replyTxt =
                'Replies (' + response.details[i].sub_comment_count + ')'
            } else {
              var replyTxt = 'Reply'
            }
            var dis_file_path = ''
            var get_file_path = response.details[i].file_path
            console.log('get_file_path_' + i, response.details[i].file_path)
            if (get_file_path.length > 0) {
              var dis_file_path = []
              var attach_file_path = []
              var split_file_path = get_file_path.toString().split(',')

              var split_file_id = response.details[i].attachments
                .toString()
                .split(',')

              if (get_file_path.length > 1) {
                for (var j = 0; j < get_file_path.length; j++) {
                  var get_file_name = split_file_path[j]
                  var split_file_name = split_file_path[j].toString().split('/')
                  var arr_reverse = split_file_name.reverse()

                  var get_file_ext = arr_reverse[0].substring(
                    arr_reverse[0].lastIndexOf('.') + 1,
                    arr_reverse[0].length
                  )
                  if (get_file_ext === 'pdf') {
                    var file_icon = '../../images/pdf-icon.png'
                  } else {
                    var file_icon = '../../images/img-icon.png'
                  }

                  dis_file_path.push(
                    '<a onClick="dataTaggingFunc(' + this,
                    this.state.list_id,
                    split_file_id[j] +
                      ')"><img src="' +
                      file_icon +
                      '" alt="' +
                      get_file_ext +
                      '" /><span>' +
                      arr_reverse[0].substring(
                        arr_reverse[0].length - 15,
                        arr_reverse[0].length
                      ) +
                      '</span></a>'
                  )
                }
              } else {
                var split_file_name = response.details[i].file_path[0]
                  .toString()
                  .split('/')
                var arr_reverse = split_file_name.reverse()

                var get_file_ext = arr_reverse[0].substring(
                  arr_reverse[0].lastIndexOf('.') + 1,
                  arr_reverse[0].length
                )
                if (get_file_ext === 'pdf') {
                  var file_icon = '../../images/pdf-icon.png'
                } else {
                  var file_icon = '../../images/img-icon.png'
                }
                dis_file_path.push(
                  '<a onClick="dataTaggingFunc(' + this,
                  this.state.list_id,
                  split_file_id[0] +
                    ')"><img src="' +
                    file_icon +
                    '" alt="' +
                    get_file_ext +
                    '" /><span>' +
                    arr_reverse[0].substring(
                      arr_reverse[0].length - 15,
                      arr_reverse[0].length
                    ) +
                    '</span></a>'
                )
              }
            }

            var replyFrm =
              '<div className="reply_cnt collapse" id=reply_cnt' +
              comment_id +
              '><form action="javascript:;" className="col-md-12 col-xs-12 pad-no reply-form reply-frm"><input type="hidden" className="parent_comment_id" name="parent_comment_id" value="' +
              comment_id +
              '" /><textarea className="col-md-12 col-xs-12 reply_txt_cls" name="reply_txt" placeholder="Reply..." required></textarea><div className="pull-right"><div className="files_txt"></div><a href="javascript:;" className=" btn btn-empty"><input type="file" name="imgInp[]" id="imgInp2" className="add_img replyAttach" id="upload_file_' +
              comment_id +
              '" multiple accept="image/*,application/pdf" /><img src="../../images/attach-icon.svg" alt="icon"/></a><button type="submit" className="btn btn-green replySubmit"><img src="../../images/reply-icon.svg" alt="icon"/>Reply</button></div></form><div className="subCmnt"></div></div>'

            comment_list.push(
              '<div className="reply_cmment_row col-md-12 col-xs-12 pad-no" id="' +
                response.details[i].comment_id +
                '"><div className="reply-cont col-md-12 col-xs-12"><div className="col-md-12 col-xs-12 pad-no"><div className="avatar-img"><img className="img-responsive" src="' +
                user_img +
                '" alt="AvatarIMG"/></div><div className="reply-user"><span className="col-md-12 col-xs-12 pad-no user-name">' +
                response.details[i].comment_user +
                '</span><span className="col-md-12 col-xs-12 pad-no date">' +
                response.details[i].ago_value +
                '</span></div></div><div className="dropdown menu-item prnt_cmnt sub_cmnt"><a href="javascript" className="dropdown-toggle" data-toggle="dropdown"><img src="../../images/menu-dot.svg" alt="icon"/></a><ul className="dropdown-menu"><li><a href="javascript:;" data-comment-id="' +
                response.details[i].comment_id +
                '" className="edit_cmnt">Edit</a></li><li><a href="javascript:;" data-comment-id="' +
                response.details[i].comment_id +
                '" data-parent-id="' +
                parent_comment_id +
                '" className="del_cmnt">Delete</a></li></ul></div><p className="col-md-12 col-xs-12 pad-no comment-txt">' +
                response.details[i].comment_text +
                '</p><form action="javascript:;" className="col-md-12 col-xs-12 pad-no reply-form update_cmnt hide"><input type="hidden" name="comment_id" className="comment_id" value="' +
                response.details[i].comment_id +
                '" /><input type="hidden" name="parent_comment_id" className="parent_comment_id" value="' +
                response.details[i].comment_id +
                '" /><textarea className="col-md-12 col-xs-12 cmnt_txt" id="cmmnt_txt_id' +
                response.details[i].comment_id +
                '" name="comment_text" placeholder="" required></textarea><div className="pull-right"><button type="button" className="btn btn-default cancel_button">Cancel</button><button type="submit" className="btn btn-green updateCmnt">Update</button></div></form><div className="attachment-item col-md-12 col-xs-12 pad-no">' +
                dis_file_path +
                '</div></div><div className="col-md-12 col-xs-12 pad-no"><button className="btn btn-lightgray">Resolved</button><a href="javascript:;" className="reply-link" data-comment-id="' +
                response.details[i].comment_id +
                '" onclick="return replyLink(' +
                response.details[i].comment_id +
                ');">' +
                replyTxt +
                '</a></div>' +
                replyFrm +
                '</div>'
            )
          }

          jQuery('#reply_cnt' + parent_comment_id)
            .children('.subCmnt')
            .html(comment_list)

          jQuery('.edit_cmnt').click(function () {
            var text_cmnt = jQuery(this)
              .closest('.prnt_cmnt')
              .next('.comment-txt')
              .text()
            var this_cmnt_id = jQuery(this).attr('data-comment-id')
            jQuery(this)
              .closest('.prnt_cmnt')
              .next('.comment-txt')
              .addClass('hide')
            jQuery(this)
              .closest('.prnt_cmnt')
              .next('.comment-txt')
              .next('.reply-form')
              .removeClass('hide')
            jQuery('#cmmnt_txt_id' + this_cmnt_id).val(text_cmnt)
          })

          jQuery('.cancel_button').click(function () {
            jQuery(this)
              .closest('.reply-form')
              .addClass('hide')
            jQuery(this)
              .closest('.reply-form')
              .prev('.comment-txt')
              .removeClass('hide')
          })

          jQuery('.updateCmnt').click(function (e) {
            e.preventDefault()
            var parent_comment_id = jQuery(this)
              .closest('.reply-form')
              .children('.parent_comment_id')
              .val()
            var comment_text = jQuery(this)
              .closest('.pull-right')
              .prev('.cmnt_txt')
              .val()
            var user_id = THIS.state.logged_user_id
            var file_id = THIS.props.match.params.file_id

            THIS.updateCommentApi(
              comment_id,
              comment_text,
              user_id,
              file_id,
              parent_comment_id
            )
            jQuery(this)
              .closest('.reply-form')
              .prev('.comment-txt')
              .text(comment_text)
          })

          jQuery('.del_cmnt').click(function (e) {
            e.preventDefault()
            var comment_id = jQuery(this).attr('data-comment-id')
            var parent_comment_id = jQuery(this).attr('data-parent-id')
            var user_id = parseFloat(THIS.state.logged_user_id)
            var list_id = parseFloat(THIS.props.match.params.list_id)
            var file_id = parseFloat(THIS.props.match.params.file_id)

            THIS.deleteComment(comment_id, list_id, parent_comment_id)
          })

          jQuery('.replySubmit').click(function (e) {
            e.preventDefault()

            var pstCommnt = jQuery(this)
              .closest('.pull-right')
              .prev('.reply_txt_cls')
              .val()
            var user_id = parseFloat(THIS.state.logged_user_id)
            var list_id = parseFloat(THIS.props.match.params.list_id)
            var file_id = parseFloat(THIS.props.match.params.file_id)
            var parent_comment_id = jQuery(this)
              .closest('.reply-form')
              .children('.parent_comment_id')
              .val()
            if (THIS.state.attachment_file_jquery.length > 0) {
              var attachments = THIS.state.attachment_file_jquery
            } else {
              var attachments = ''
            }

            console.log('jquery_attachments', attachments)

            THIS.addSubComment(
              pstCommnt,
              user_id,
              list_id,
              file_id,
              attachments,
              parent_comment_id
            )
          })

          jQuery('.replyAttach').on('change', function (e) {
            var files = e.target.files
            THIS.setState({ attachment_file_length: files.length })

            if (files.length > 0) {
              var fileArra = THIS.state.attachment_file_jquery
              //var fileThumbArra = this.state.imgThumb;

              for (var i = 0; i < files.length; i++) {
                fileArra.push(e.target.files[i])
                THIS.setState({
                  //   selectedFile:URL.createObjectURL(e.target.files[i]),
                  attachment_file_jquery: fileArra
                })
              }
            }

            jQuery(this)
              .closest('.pull-right')
              .children('.files_txt')
              .html('2 files are attached')

            console.log(
              'attachments_length',
              THIS.state.attachment_file_jquery.length
            )
            console.log('attachment_file', THIS.state.attachment_file_jquery)
          })
        }
      } else {
        jQuery('#reply_cnt' + parent_comment_id)
          .children('.subCmnt')
          .html('')
      }
    })
  }

  selectHandlerBalancelist = event => {
    event.preventDefault()
    let balance_list_selected = event.target.value
    this.setState({ balance_list_selected: event.target.value })
    // this.convertHandler(ToCurrency);
  }
  selectHandler = event => {
    event.preventDefault()
    let ToCurrency = event.target.value
    this.setState({ ToCurrency: event.target.value })
    // this.convertHandler(ToCurrency);
  }

  add_gst_details = () => {
    let sales_tax_code = this.state.sales_tax_code
    let sales_tax_name = this.state.salesTax_name_entered
    let show_on_list = 1
    let tax_type = this.state.selectedOption === 'option1' ? 1 : 2
    let rate = this.state.rate_entered

    if (
      this.state.selected_rate_type != 'Fixed price' &&
      this.state.selected_rate_type === '%'
    ) {
      var rate_type = 1
    } else {
      var rate_type = 2
    }

    let country = this.state.country_code
    let items = {
      sales_tax_code: sales_tax_code,
      sales_tax_name: sales_tax_name,
      show_on_list: show_on_list,
      tax_type: tax_type,
      rate: rate,
      rate_type: rate_type,
      country: country
    }
    console.log('hjagsjkhlkasjh', items)
    FetchAllApi.add_gst_details(items, (err, response) => {
      console.log('add comment', response.status)

      if (response.status === 1) {
        jQuery('#sales_tax_code').val('')
        jQuery('#sales_tax_name').val('')
        jQuery('#sales_tax_name').val('')
        jQuery('#tax').val('')
        this.setState({ show_succes: true })
        this.get_gst_list()
        var THIS = this
        setTimeout(function () {
          THIS.setState({ show_succes: false })
        }, 4000)
        // jQuery('.modal fade pop-modal-in').fadeIn(2000)
        // setTimeout(function () {
        //   jQuery('.modal fade pop-modal-in').fadeOut(2000)
        // }, 8000)

        // jQuery('#pop-modal-1').removeClass('in')
        // jQuery('body').removeClass('modal-open')
        // jQuery('.modal-backdrop').removeClass('in')

        // setTimeout(function () {
        //  jQuery('#pop-modal-1').removeClass('in')
        // jQuery('body').removeClass('modal-open')
        // jQuery('.modal-backdrop').removeClass('in')
        // }, 3000)
        window.jQuery('#pop-modal-1').modal('hide');

      } else {
        this.setState({ modal_info_msg: response.message })
        jQuery('.mymsg').fadeIn(2000)
        setTimeout(function () {
          jQuery('.mymsg').fadeOut(2000)
        }, 8000)
      }
    })
  }
convert_date_format=()=>{
  var convert=(str)=> {
    var date = new Date(str),
         mnth = ("0" + (date.getMonth() + 1)).slice(-2),
         day = ("0" + date.getDate()).slice(-2);
       return [  date.getFullYear(),mnth,day].join("-");
     }
  return  convert(this.state.date)
}
  saveAndContinue = () => {
    var selected_date=this.convert_date_format();
    
    let items = {
      client_id: 1,
      item_total_foreign_currency: this.state.item_total_foreign_currency,
      tax_amount_home_currency: this.state.tax_amount_home_currency,
      grand_total_home_currency: this.state.grand_total_home_currency,
      item_total_home_currency: this.state. item_total_home_currency,
      tax_amount_foreign_currency: this.state.tax_amount_foreign_currency,
      grand_total_foreign_currency: this.state.grand_total_foreign_currency,
      currency: this.state.ToCurrency,
      exchange_rate: this.state.exchange_value,
      type: 1,
      list_id: this.props.match.params.list_id,
      tagged_user_id: 33,
      invoice_date:selected_date,
      company_name: this.state.company_name,
      // invoice_no: this.state.invoice_no,
      invoice_number:this.state.invoice_no ,
      company_address: this.state.address,
      incorport_date:selected_date,
      account_category: this.state.account_category,
      item_list: this.state.myarray,
      balance_sheet_category_id: this.state.balance_sheet_category_id
    }
    console.log('nike', items)
    //alert(item_check)
    if (
      this.Chk_table_validation()  
      &&
      this.state.balance_sheet_category_id != ''
    ) {
      FetchAllApi.saveAndContinue(items, (err, response) => {
        console.log('add comment', response.status)
        if (response.status === 1) {
          this.handleOnClick();
          this.setState({ isClose: true ,isAdd:false,sales_tax_name: 'Choose',sales_tax_rate: 0,selected:'Choose',rows: ['row 1'],balance_sheet_category_name:'',balance_sheet_category_id:'',isChecked: false,coulmns:[],isCompany_name:true,company_name:'',invoice_no:'',
          isInvoice_no:true,
          isBalance_sheet_category_name:true,
          isTable_notEmpty:false

        })
          jQuery('#closeme').fadeIn(12)
          setTimeout(function () {
            if (jQuery('#closeme').fadeOut(2000));
          }, 8000)
          this.callme();
          document.getElementById('selectednow0').innerHTML = 'choose'

        } else {
         // alert(response.message)
          this.setState({
            isAdd: true
            //  add_cmnt_msg:
          })
 

        }
      })
    } else {
      this.validation_msg();
      this.setState({ isAdd: true })
      this.handleOnClick();

    }
  }
  validation_msg= ()=>{
   // alert('worling')
    this.state.company_name!=""?this.setState({isCompany_name:true}):this.setState({isCompany_name:false})
    this.state.invoice_no!=""?this.setState({isInvoice_no:true}):this.setState({isInvoice_no:false})
    this.state.balance_sheet_category_id!=""?this.setState({isBalance_sheet_category_name:true}):this.setState({isBalance_sheet_category_name:false})
    this.val_me_Check()    

}
validation_clean= ()=>{
  this.state.company_name!=""?this.setState({isCompany_name:true}):this.setState({});
  this.state.invoice_no!=""?this.setState({isInvoice_no:true}):this.setState({});
  this.state.balance_sheet_category_id!=""?this.setState({isBalance_sheet_category_name:true}):this.setState({});  
  }
  save_draft = () => {
    var selected_date=this.convert_date_format()
    let items = {
      client_id: 1,
      item_total_foreign_currency: this.state.item_total_foreign_currency ,
      tax_amount_home_currency:   this.state.tax_amount_foreign_currency,
      grand_total_home_currency: this.state.grand_total_home_currency,
      item_total_home_currency:this.state.item_total_home_currency,
      tax_amount_foreign_currency: this.state.tax_amount_home_currency,
      grand_total_foreign_currency: this.state.grand_total_foreign_currency,
      currency: this.state.ToCurrency,
      exchange_rate: this.state.exchange_value,
      type: 1,
      list_id: this.props.match.params.list_id,
      tagged_user_id: 33,
      invoice_date: selected_date,
      company_name: this.state.company_name,
      // invoice_no: this.state.invoice_no,
      invoice_number: '0004',
      company_address: this.state.address,
      incorport_date: selected_date,
      account_category: this.state.account_category,
      item_list: this.state.myarray,
      balance_sheet_category_id: this.state.balance_sheet_category_id
    }
    console.log('nike', items)
    if (
      this.Chk_table_validation()&&
      this.state.balance_sheet_category_id != ''
    ) {
      FetchAllApi.save_tagged_item_draft(items, (err, response) => {
        console.log('add comment', response.status)
        if (response.status === 1) {
          this.handleOnClick();
          this.setState({ isClose: true ,sales_tax_rate: 0,coulmns:[],balance_sheet_category_id:'',isChecked: false,isClose1:true,isAdd:false,sales_tax_name: 'Choose',selected:'Choose',rows: ['row 1'],balance_sheet_category_name:'',company_name:'',invoice_no:'',
         })
          jQuery('#closeme1').fadeIn(10)
          setTimeout(function () {
            if (jQuery('#closeme').fadeOut(2000));
          }, 8000)
          this.callme();
          document.getElementById('selectednow0').innerHTML = 'choose'

        } else {
          this.validation_msg();

          this.setState({
            isAdd: true
          })

          // jQuery(".resp_msg").fadeIn(2);
          // setTimeout(function() {
          //   jQuery(".resp_msg").fadeOut(20);
          // }, 8000);
        }
      })
    } else {
      this.validation_msg()
      this.setState({ isAdd: true })
      this.handleOnClick();

      setTimeout(()=>{this.setState({ isAdd: false})
    },5000)
    }
  }
  saveNew_Account = e => {
    let account_name = this.state.account_name
    let category_id = this.state.category_id
    let sub_category_id = this.state.sub_category_id
    let account_type_id = this.state.Account_type_id
    let currency = this.state.Currency_name
    if (
      account_name &&
      category_id &&
      sub_category_id &&
      account_type_id &&
      currency != ''
    ) {
      let items = {
        account_name: account_name,
        category_id: category_id,
        sub_category_id: sub_category_id,
        account_type_id: account_type_id,
        currency: currency
      }

      FetchAllApi.save_NewAccountName(items, (err, response) => {
        // alert()
        //
        // alert(response.message);

        console.log('add comment', response.status)
        if (response.status === 1) {
          //  alert(response.message)
          this.setState({
            add_cmnt_msg: response.message
          })

          jQuery('.resp_msg').fadeIn(0.0)
          setTimeout(function () {
            jQuery('.resp_msg').fadeOut(2000)
          }, 8000)
          // this.callme()
        } else {
          this.setState({})
        }
      })
    } else {
      //  jQuery(".htttt").fadeIn(20);
      //   setTimeout(function() {
      //     jQuery(".htttt").fadeOut(200);
      //   }, 2000);
    }
  }

  callme = () => {
    jQuery('.form-control').val('')
    this.handleChangeItems(0, this.state.rows.length - 1)
  }

  handleChange (event) {
    this.setState({
      [event.target.name]: event.target.value
    },()=>this.validation_clean())

  }
  handleChangeItems (e, itemid) {
    
    var result = []
    var itemprice = []
    for (var i = itemid; i >= 0; i--) {
     // alert(i)
      if(document.getElementById('selectednow_id' + i)!=null){
      var item_name =jQuery('#item' + i).val() != '' ? jQuery('#item' + i).val() : 0
      var quantity_check =jQuery('#quantity' + i).val() != '' ? jQuery('#quantity' + i).val() : 0
      var quantity = isNaN(quantity_check) ? jQuery('#quantity' + i).val(''): quantity_check
      var unit_price_check =jQuery('#unit_price' + i).val() != ''? jQuery('#unit_price' + i).val(): 0
      var unit_price = isNaN(unit_price_check)? jQuery('#unit_price' + i).val(''): unit_price_check
      var price = quantity * unit_price
      var selectednow_id = document.getElementById('selectednow_id' + i).innerText
      var category_id =selectednow_id != 'NO_VALUE'? selectednow_id: this.state.selectedindex != ''? this.state.selectedindex: ''
      }
      var custom_details = {}
      var coulmn_index = this.state.coulmns.length;

      for (let k = coulmn_index - 1; k >= 0; k--) {
        var header_nlame_check = document.getElementById('header' + k).innerHTML
        var header_nlame = header_nlame_check
          .toLowerCase()
          .replace(/\s+/g, '_')
          .trimRight()

        var coulmn_value = jQuery('#coulmn' + i + k).val()
        custom_details[header_nlame.replace(/&.*;/g, '')] = coulmn_value
      }

      let item_list = {
        item_name: item_name,
        quantity: quantity,
        price: price,
        unit_price: unit_price,
        category_id: category_id,
        ...custom_details
      }
      result.push(item_list)
      console.log('item_list', result)

      itemprice.push(parseFloat(price))
    }

    const add = (a, b) => a + b
    const trial = itemprice.length > 0 ? itemprice.reduce(add) : 0
    if (isNaN(trial)) {
      var sum = 0
    } else {
      var sum = trial
    }

    console.log('itemprice', sum)

    if (this.state.isChecked) {
      let foreign_currency = this.state.ToCurrency
      let value = sum
      let nope = 'https://api.exchangerate-api.com/v4/latest/'
      let res = nope.concat(foreign_currency)
      fetch(res)
        .then(response => response.json())
        .then(data => {
          let todayValue = data.rates
          let exchange_value = todayValue['SGD']
          if (this.state.sales_tax_type != 2) {
            var item_total_foreign_currency = (value / (parseFloat(this.state.sales_tax_rate) + 100)) * 100
            var tax_amount_foreign_currency =(item_total_foreign_currency *  parseFloat(this.state.sales_tax_rate)) /100
            var item_total_home_currency = todayValue['SGD'] * item_total_foreign_currency
            var tax_amount_home_currency = todayValue['SGD'] * tax_amount_foreign_currency
            var grand_total_home_currency = todayValue['SGD'] * value
          } else {          
            var item_total_foreign_currency_check = value - parseFloat(this.state.sales_tax_rate)
            var item_total_foreign_currency = item_total_foreign_currency_check
            var tax_amount_foreign_currency_check = parseFloat(this.state.sales_tax_rate)
            var tax_amount_foreign_currency = tax_amount_foreign_currency_check
            var item_total_home_currency = todayValue['SGD'] * item_total_foreign_currency
            var tax_amount_home_currency = todayValue['SGD'] * tax_amount_foreign_currency
            var grand_total_home_currency = todayValue['SGD'] * value
            var grand_total_foreign_currency_check = item_total_foreign_currency + tax_amount_foreign_currency
            var grand_total_foreign_currency = grand_total_foreign_currency_check
          }

          this.setState({
            myarray: result.reverse(),
            exchange_value: exchange_value.toFixed(2),
            item_total_foreign_currency: item_total_foreign_currency.toFixed(2),
            tax_amount_foreign_currency: tax_amount_foreign_currency.toFixed(2),
            grand_total_foreign_currency: this.state.sales_tax_type != 1 ? grand_total_foreign_currency.toFixed(2): sum.toFixed(2),
            item_total_home_currency: item_total_home_currency.toFixed(2),
            tax_amount_home_currency: tax_amount_home_currency.toFixed(2),
            grand_total_home_currency: grand_total_home_currency.toFixed(2)
          },()=>{this.val_me()})
        })
    } else {
      let foreign_currency = this.state.ToCurrency
      let value = sum
      let nope = 'https://api.exchangerate-api.com/v4/latest/'
      let res = nope.concat(foreign_currency)
      fetch(res)
        .then(response => response.json())
        .then(data => {
          let todayValue = data.rates
          let exchange_value = todayValue['SGD']
          if (this.state.sales_tax_type != 2) {
            var item_total_foreign_currency = value
            var tax_amount_foreign_currency =(item_total_foreign_currency * this.state.sales_tax_rate) / 100
            var grand_total_foreign_currency = value + tax_amount_foreign_currency
            var item_total_home_currency = todayValue['SGD'] * item_total_foreign_currency
            var tax_amount_home_currency = todayValue['SGD'] * tax_amount_foreign_currency
            var grand_total_home_currency = todayValue['SGD'] * grand_total_foreign_currency
          } else {
            var item_total_foreign_currency = value
            var tax_amount_foreign_currency = parseFloat(this.state.sales_tax_rate )
            var grand_total_foreign_currency = item_total_foreign_currency + tax_amount_foreign_currency
            var item_total_home_currency = todayValue['SGD'] * item_total_foreign_currency
            var tax_amount_home_currency = todayValue['SGD'] * tax_amount_foreign_currency
            var grand_total_home_currency = todayValue['SGD'] * grand_total_foreign_currency
          }

          this.setState({
            myarray: result.reverse(),
            exchange_value: exchange_value.toFixed(2),
            item_total_foreign_currency: item_total_foreign_currency.toFixed(2),
            tax_amount_foreign_currency: tax_amount_foreign_currency.toFixed(2),
            grand_total_foreign_currency: grand_total_foreign_currency.toFixed(2),
            item_total_home_currency: item_total_home_currency.toFixed(2),
            tax_amount_home_currency: tax_amount_home_currency.toFixed(2),
            grand_total_home_currency: grand_total_home_currency.toFixed(2)
          },()=>{this.val_me()})
        })
    }


  }

Chk_table_validation=()=>{
  var item_check = jQuery(
    `#coulmn${this.state.rows.length - 1}${this.state.coulmns.length - 1}`
  ).val()
 if(this.state.myarray.length > this.state.rows.length - 1 &&
  this.state.myarray[this.state.myarray.length - 1].price > 0 &&
  this.state.myarray[this.state.myarray.length - 1].item_name.length > 0 &&
  this.state.myarray[this.state.myarray.length - 1].category_id.length > 0 &&item_check != ''){
    return true
  }else{return false}
  }

val_me=()=>{
  if(
  this.Chk_table_validation()  
  ){
    this.setState({isTable_notEmpty:false})
  }else{    
}
}
val_me_Check=()=>{
  if(
    this.Chk_table_validation()  

  ){
  }else{this.setState({isTable_notEmpty:true})
   
}
}
  toggleChange = () => {

    this.setState(
      {
        isChecked: !this.state.isChecked
      },
      () => {this.handleChangeItems(0, this.state.rows.length - 1)}
    )
  }

  handleCheck (e) {
    this.setState({
      selected: e.currentTarget.dataset.id,
      selectedindex: e.currentTarget.dataset.the
    });
    this.handleChangeItems(0, this.state.rows.length - 1)
  }

  handleCheck_currency (e) {
    this.setState({ ToCurrency: e.currentTarget.dataset.namee }, () => {
      this.handleChangeItems(0, this.state.rows.length - 1)
    })
  }
  handleCheck_get_selected_tax (e) {
    
    this.setState(
      {
        sales_tax_name: e.currentTarget.dataset.name,
        sales_tax_rate: e.currentTarget.dataset.rate,
        sales_tax_type: e.currentTarget.dataset.type
      },
      () => {
        this.handleChangeItems(0, this.state.rows.length - 1)
      }
    )
  }

  handleCheck_currency_modal = e => {
    this.setState({ Currency_name: e })
  }
  handleCheck_balanceSheet_id (e) {
    this.setState({
      balance_sheet_category_name: e.currentTarget.dataset.namee,
      balance_sheet_category_id: e.currentTarget.dataset.id
    },()=>{      this.state.balance_sheet_category_id!=""?this.setState({isBalance_sheet_category_name:true}):this.setState({isBalance_sheet_category_name:false})
  })
  }
  changetext1 = (selectednow_id, itemid, id, valueres) => {
    document.getElementById(id).innerHTML = valueres
    if (selectednow_id > 0) {
      document.getElementById(
        'selectednow_id' + itemid
      ).innerHTML = selectednow_id
      this.handleChangeItems(valueres, this.state.rows.length - 1)

      jQuery('.form-table').removeClass('ovrFlwRmve')
    }else{alert('sorry fault is here only')}
  }
  fetchSubCategeory = (name, val) => {
    let category_id = val
    let categoryname = name
    FetchAllApi.get_SubCategory(category_id, (err, response) => {
      console.log('loklkk', response)
      if (response.status === 1) {
        console.log('add======comment', response.status)
        this.setState({
          sub_categorylist: response.list,
          categoryname: categoryname,
          category_id: category_id
        })
      } else {
      }
    })
  }
  fetchAccount_type = (name, val) => {
    let sub_categoryname = name
    let sub_category_id = val

    FetchAllApi.get_Accounttype(sub_category_id, (err, response) => {
      console.log('loklkk', response)
      if (response.status === 1) {
        console.log('add======comment', response.status)
        this.setState({
          Accounttype: response.list,
          sub_categoryname: sub_categoryname,
          sub_category_id: sub_category_id
        })
      } else {
      }
    })
  }

  render () {
    if (
      this.state.myarray.length > 0 &&
      this.state.myarray[0].price != '' &&
      this.state.myarray[0].price != 0
    ) {
      console.log('hyyyyy', this.state.myarray[0].price)
    }
    console.log('balance_sheet_category_list', this.state.Account_type_name)
    let THIS = this
    let file_path = [],
      file_path_list = '',
      scanned_div = [],
      comment_list = []

    file_path_list = this.state.get_file_path.toString()
    if (file_path_list !== '') {
      var get_file_ext = file_path_list.substring(
        file_path_list.lastIndexOf('.') + 1,
        file_path_list.length
      )
      if (
        get_file_ext === 'png' ||
        get_file_ext === 'jpg' ||
        get_file_ext === 'jpeg'
      ) {
        file_path.push(
          <li>
            <a href='javascript:;' className='active'>
              <img src={file_path_list} className='img-responsive' />
            </a>
          </li>
        )

        scanned_div.push(
          <div className='scanned-file'>
            <img src={file_path_list} alt='Scanned-file' />
          </div>
        )
      } else {
        var pdf_file_url = file_path_list + '#toolbar=0&navpanes=0'
        file_path.push(
          <li>
            <a href='javascript:;' className='active'>
              {/* <img src={file_path_list} className="img-responsive" /> */}
              <iframe
                src={pdf_file_url}
                className='data_tagging_thumb'
                frameborder='0'
                scrolling='no'
              ></iframe>
            </a>
          </li>
        )

        scanned_div.push(
          <div className='scanned-file'>
            {/* <img src={file_path_list} alt="Scanned-file" /> */}
            <iframe
              src={pdf_file_url}
              className='data_tagging_large'
              frameborder='0'
              scrolling='no'
            ></iframe>
          </div>
        )
      }
    }

    return (
      <div>
      <div></div>
        <div className='container-fluid'>
          <div className='row'>
            <LeftSidebar pageSubmit={e => this.pageLink(e)} />

            <div className='main-wrap col-md-12 col-xs-12 pad-r-no'>
              <div className='top-bar col-md-12 col-xs-12 pad-r-no'>
                <div className='nav-brand-res visible-xs'>
                  <img
                    className='img-responsive'
                    src='../../images/logo-icon.png'
                    alt='LogoIcon'
                  />
                </div>
                <a
                  onClick={this.routedChange.bind(this, 'user_inbox')}
                  className='back hidden-xs'
                >
                  <svg
                    xmlns='http://www.w3.org/2000/svg'
                    width='18.5'
                    height='14.249'
                    viewBox='0 0 18.5 14.249'
                  >
                    <g
                      id='left-arrow_2_'
                      data-name='left-arrow (2)'
                      transform='translate(0 -58.83)'
                    >
                      <g
                        id='Group_25'
                        data-name='Group 25'
                        transform='translate(0 65.207)'
                      >
                        <g
                          id='Group_24'
                          data-name='Group 24'
                          transform='translate(0 0)'
                        >
                          <path
                            id='Path_19'
                            data-name='Path 19'
                            d='M17.753,235.318H.747a.747.747,0,0,0,0,1.495H17.753a.747.747,0,0,0,0-1.495Z'
                            transform='translate(0 -235.318)'
                          />
                        </g>
                      </g>
                      <g
                        id='Group_27'
                        data-name='Group 27'
                        transform='translate(0 58.83)'
                      >
                        <g
                          id='Group_26'
                          data-name='Group 26'
                          transform='translate(0 0)'
                        >
                          <path
                            id='Path_20'
                            data-name='Path 20'
                            d='M1.8,65.954l5.849-5.849A.747.747,0,1,0,6.6,59.049L.219,65.426a.747.747,0,0,0,0,1.057L6.6,72.86A.747.747,0,1,0,7.653,71.8Z'
                            transform='translate(0 -58.83)'
                          />
                        </g>
                      </g>
                    </g>
                  </svg>
                </a>
                <span className='page-title hidden-xs'>
                  Inbox / <span>Data Tagging</span>
                </span>

                <Topbar logoutSubmit={e => this.logoutLink()} />
              </div>

              <div className='main-content col-md-12 col-xs-12'>
                <div className='resp_msg'>{this.state.add_cmnt_msg}</div>

                <input
                  type='hidden'
                  id='logged_user_id'
                  value={this.state.logged_user_id}
                />
                <input
                  type='hidden'
                  id='file_id'
                  value={this.props.match.params.file_id}
                />
                <input
                  type='hidden'
                  id='list_id'
                  value={this.props.match.params.list_id}
                />

                <a
                  onClick={this.routedChange.bind(this, 'user_inbox')}
                  className='back visible-xs'
                >
                  {/* <img src="../../images/back-arrow-dark.svg"> */}
                  <svg
                    xmlns='http://www.w3.org/2000/svg'
                    width='18.5'
                    height='14.249'
                    viewBox='0 0 18.5 14.249'
                  >
                    <g
                      id='left-arrow_2_'
                      data-name='left-arrow (2)'
                      transform='translate(0 -58.83)'
                    >
                      <g
                        id='Group_25'
                        data-name='Group 25'
                        transform='translate(0 65.207)'
                      >
                        <g
                          id='Group_24'
                          data-name='Group 24'
                          transform='translate(0 0)'
                        >
                          <path
                            id='Path_19'
                            data-name='Path 19'
                            d='M17.753,235.318H.747a.747.747,0,0,0,0,1.495H17.753a.747.747,0,0,0,0-1.495Z'
                            transform='translate(0 -235.318)'
                          />
                        </g>
                      </g>
                      <g
                        id='Group_27'
                        data-name='Group 27'
                        transform='translate(0 58.83)'
                      >
                        <g
                          id='Group_26'
                          data-name='Group 26'
                          transform='translate(0 0)'
                        >
                          <path
                            id='Path_20'
                            data-name='Path 20'
                            d='M1.8,65.954l5.849-5.849A.747.747,0,1,0,6.6,59.049L.219,65.426a.747.747,0,0,0,0,1.057L6.6,72.86A.747.747,0,1,0,7.653,71.8Z'
                            transform='translate(0 -58.83)'
                          />
                        </g>
                      </g>
                    </g>
                  </svg>
                </a>
                <span className='page-title visible-xs'>
                  Inbox / <span>Data Tagging</span>
                </span>
                <div className='content-top col-md-12 col-xs-12 pad-no'>
                  <select className='select-dropdown selectpicker'>
                    <option>Bill-Payment.pdf</option>
                    <option>Taxi-bill.jpg</option>
                    <option>Stationary.jpg</option>
                  </select>
                </div>

                <div className='content-sec col-md-12 col-xs-12 pad-no inbox-listing'>
                  <div className='col-md-12 col-xs-12 scanned-wrap'>
                    <p className='visible-xs note-content'>
                      This feature cant use in mobile. Please use Desktop
                    </p>
                    <div className='col-md-6 col-md-12 scanned-left hidden-xs'>
                      <div className='file-thumbnail'>
                        <ul className='list-unstyled'>{file_path}</ul>
                      </div>
                      <div className='doc-wrap'>
                        <div className='zoom-btn'>
                          <a href='javascript:;' className='plus'>
                            <img src='../../images/zoom-in.svg' alt='icon' />
                          </a>
                          <a href='javascript:;' className='minus'>
                            <img src='../../images/zoom-out.svg' alt='icon' />
                          </a>
                        </div>

                        <ContextMenuTrigger id='some_unique_identifier'>
                          {scanned_div}
                        </ContextMenuTrigger>

                        <ContextMenu id='some_unique_identifier'>
                          <MenuItem
                            data={{ foo: 'bar1' }}
                            onClick={this.handleClick}
                          >
                            Copy
                          </MenuItem>
                          <MenuItem
                            data={{ foo: 'bar2' }}
                            onClick={this.handleClick}
                          >
                            Add to First Name
                          </MenuItem>
                          <MenuItem divider />
                          <MenuItem
                            data={{ foo: 'bar3' }}
                            onClick={this.handleClick}
                          >
                            Add to Last Name
                          </MenuItem>
                        </ContextMenu>

                        <form
                          className='comment-sec'
                          method='post'
                          onSubmit={this.addCommentFunc.bind(this)}
                        >
                          <textarea
                            cols='3'
                            rows='5'
                            name='comment_text'
                            id='comment_text'
                            className='form-control'
                            placeholder='Comments'
                            required
                          ></textarea>
                          <button className='btn btn-green' type='submit'>
                            Send
                          </button>
                        </form>

                        <div className='comments-wrap'>
                          {this.state.file_comments.map(
                            (comments_data, index) => {
                              if (comments_data.user_image) {
                                var user_img = comments_data.user_image
                              } else {
                                var user_img = '../../images/user-img-1.png'
                              }

                              if (
                                comments_data.user_id ===
                                parseFloat(this.state.logged_user_id)
                              ) {
                                var disp_cmnt = 'show'
                              } else {
                                var disp_cmnt = 'hide'
                              }

                              var get_file_path = comments_data.file_path
                              if (get_file_path.length > 0) {
                                var dis_file_path = []
                                var attach_file_path = []
                                var split_file_path = get_file_path
                                  .toString()
                                  .split(',')

                                var split_file_id = comments_data.attachments
                                  .toString()
                                  .split(',')

                                if (get_file_path.length > 1) {
                                  for (var i = 0; i < 2; i++) {
                                    var get_file_name = split_file_path[i]
                                    var split_file_name = split_file_path[i]
                                      .toString()
                                      .split('/')
                                    var arr_reverse = split_file_name.reverse()

                                    var get_file_ext = arr_reverse[0].substring(
                                      arr_reverse[0].lastIndexOf('.') + 1,
                                      arr_reverse[0].length
                                    )
                                    if (get_file_ext === 'pdf') {
                                      var file_icon = 'images/pdf-icon.png'
                                    } else {
                                      var file_icon = 'images/img-icon.png'
                                    }

                                    dis_file_path.push(
                                      <a
                                        onClick={this.dataTaggingFunc.bind(
                                          this,
                                          this.state.list_id,
                                          split_file_id[i]
                                        )}
                                      >
                                        <img
                                          src={file_icon}
                                          alt={get_file_ext}
                                        />
                                        <span>
                                          {arr_reverse[0].substring(
                                            arr_reverse[0].length - 15,
                                            arr_reverse[0].length
                                          )}
                                        </span>
                                      </a>
                                    )
                                  }

                                  if (get_file_path.length > 2) {
                                    var more_div = (
                                      <span className='etc'>+2 more</span>
                                    )
                                  }
                                } else {
                                  var split_file_name = comments_data.file_path[0]
                                    .toString()
                                    .split('/')
                                  var arr_reverse = split_file_name.reverse()

                                  var get_file_ext = arr_reverse[0].substring(
                                    arr_reverse[0].lastIndexOf('.') + 1,
                                    arr_reverse[0].length
                                  )
                                  if (get_file_ext === 'pdf') {
                                    var file_icon = '../../images/pdf-icon.png'
                                  } else {
                                    var file_icon = '../../images/img-icon.png'
                                  }
                                  var dis_file_path = (
                                    <a
                                      onClick={this.dataTaggingFunc.bind(
                                        this,
                                        this.state.list_id,
                                        split_file_id[0]
                                      )}
                                    >
                                      <img src={file_icon} alt={get_file_ext} />
                                      <span>
                                        {arr_reverse[0].substring(
                                          arr_reverse[0].length - 15,
                                          arr_reverse[0].length
                                        )}
                                      </span>
                                    </a>
                                  )
                                }

                                var replyTxt = 'Reply'
                                console.log(
                                  'sub_comment_count',
                                  comments_data.sub_comment_count
                                )
                                if (comments_data.sub_comment_count > 0) {
                                  var replyTxt =
                                    'Replies(' +
                                    comments_data.sub_comment_count +
                                    ')'
                                } else {
                                  var replyTxt = 'Reply'
                                }
                              }
                              return (
                                <div
                                  className='comment-sec col-md-12 col-xs-12 pad-no'
                                  data-comment-id={comments_data.comment_id}
                                >
                                  <div className='avatar-img'>
                                    <img
                                      className='img-responsive'
                                      src={user_img}
                                      alt='AvatarIMG'
                                    />
                                  </div>
                                  <div className='comment-cont'>
                                    <span className='col-md-12 col-xs-12 pad-no user-name'>
                                      {comments_data.comment_user}
                                    </span>
                                    <span className='col-md-12 col-xs-12 pad-no date'>
                                      {comments_data.ago_value}
                                    </span>

                                    <div
                                      className={`dropdown menu-item prnt_cmnt ${disp_cmnt}`}
                                    >
                                      <a
                                        href='javascript'
                                        className='dropdown-toggle'
                                        data-toggle='dropdown'
                                      >
                                        <img
                                          src='../../images/menu-dot.svg'
                                          alt='icon'
                                        />
                                      </a>
                                      <ul className='dropdown-menu'>
                                        <li>
                                          <a
                                            href='javascript:;'
                                            data-comment-id={
                                              comments_data.comment_id
                                            }
                                            className='edit_cmnt'
                                          >
                                            Edit
                                          </a>
                                        </li>
                                        <li>
                                          <a
                                            onClick={this.deleteComment.bind(
                                              this,
                                              comments_data.comment_id,
                                              this.props.match.params.list_id,
                                              0
                                            )}
                                          >
                                            Delete
                                          </a>
                                        </li>
                                      </ul>
                                    </div>

                                    <p className='col-md-12 col-xs-12 pad-no comment-txt'>
                                      {comments_data.comment_text}
                                    </p>

                                    <form
                                      className='col-md-12 col-xs-12 pad-no reply-form update_cmnt hide'
                                      onSubmit={this.updateCmmnt.bind(this)}
                                    >
                                      <input
                                        type='hidden'
                                        name='comment_id'
                                        value={comments_data.comment_id}
                                      />
                                      <input
                                        type='hidden'
                                        name='parent_comment_id'
                                        value='0'
                                      />
                                      <textarea
                                        className='col-md-12 col-xs-12 cmnt_txt'
                                        id={`cmmnt_txt_id${comments_data.comment_id}`}
                                        name='comment_text'
                                        placeholder=''
                                        required
                                      ></textarea>
                                      <div className='pull-right'>
                                        <button
                                          type='button'
                                          className='btn btn-default cancel_button'
                                        >
                                          Cancel
                                        </button>
                                        <button
                                          type='submit'
                                          className='btn btn-green'
                                        >
                                          Update
                                        </button>
                                      </div>
                                    </form>

                                    <div className='attachment-item col-md-12 col-xs-12 pad-no'>
                                      {dis_file_path} {more_div}
                                    </div>

                                    {/* <a href="javascript:;" data-target={`#reply_frm${comments_data.comment_id}`} className="pull-left reply-link" onClick={this.replyFunc.bind(this,comments_data.comment_id)}>Reply</a> */}

                                    <a
                                      href='javascript:;'
                                      data-target={`#reply_frm${comments_data.comment_id}`}
                                      className='pull-left reply-link'
                                      data-comment-id={comments_data.comment_id}
                                      onClick={this.replyLink.bind(
                                        this,
                                        comments_data.comment_id,
                                        'replyLink'
                                      )}
                                    >
                                      {comments_data.sub_comment_count
                                        ? 'Replies (' +
                                          comments_data.sub_comment_count +
                                          ')'
                                        : 'Reply'}
                                    </a>

                                    <div
                                      className='reply_cnt collapse'
                                      id={`reply_cnt${comments_data.comment_id}`}
                                    >
                                      <form
                                        className='col-md-12 col-xs-12 pad-no reply-form reply-frm'
                                        onSubmit={this.addSubCommentFunc.bind(
                                          this
                                        )}
                                        ref='form'
                                      >
                                        <input
                                          type='hidden'
                                          name='parent_comment_id'
                                          value={comments_data.comment_id}
                                        />

                                        <textarea
                                          className='col-md-12 col-xs-12 reply_txt_cls'
                                          name='reply_txt'
                                          placeholder='Reply...'
                                          required
                                        ></textarea>
                                        <div className='pull-right'>
                                          <div className='files_txt'>
                                            {this.state.attachment_file.length
                                              ? this.state.attachment_file
                                                  .length +
                                                ' files are attached'
                                              : ''}
                                          </div>
                                          <a
                                            href='javascript:;'
                                            className=' btn btn-empty'
                                          >
                                            <input
                                              type='file'
                                              name='imgInp[]'
                                              id='imgInp2'
                                              className='add_img replyAttach'
                                              multiple
                                              onChange={this.loadFile.bind(
                                                this
                                              )}
                                              accept='image/*,application/pdf'
                                            />
                                            <img
                                              src='../../images/attach-icon.svg'
                                              alt='icon'
                                            />
                                          </a>
                                          <button
                                            type='submit'
                                            className='btn btn-green'
                                          >
                                            <img
                                              src='../../images/reply-icon.svg'
                                              alt='icon'
                                            />
                                            Reply
                                          </button>
                                        </div>
                                      </form>

                                      <div className='subCmnt'></div>
                                    </div>
                                  </div>
                                </div>
                              )
                            }
                          )}
                        </div>
                      </div>
                    </div>

                    <div className='col-lg-6 col-md-12 col-xs-12 scanned-right hidden-xs'>
                      <form
                        id='form_save'
                        className='data-feed'
                        onSubmit={e => {
                          e.preventDefault()
                          this.saveAndContinue()
                        }}
                      >
                        <div className='form-group col-md-12 col-xs-12'>
                          <label>Company Name</label><span class="astrick">*</span>
                          <input
                            type='text'
                            name='company_name'
                            className='form-control'
                            autoComplete='off'
                            onChange={event => this.handleChange(event)}
                            required
                          />
                          {!this.state.isCompany_name ? (
                            <div style={{ float: 'left' }}>
                              <small style={{ color: 'red' }}>
                                *Please fill out company name.
                              </small>
                            </div>
                          ) : (
                            ''
                          )}
                        </div>
                        <div className='form-group col-md-6 col-sm-6'>
                          <label>invoice No</label><span class="astrick">*</span>
                          <input
                            type='text'
                            name='invoice_no'
                            autoComplete='off'
                            className='form-control'
                            onChange={event => this.handleChange(event)}
                            required
                          />
                          {!this.state.isInvoice_no ? (
                            <div style={{ float: 'left' }}>
                              <small style={{ color: 'red' }}>
                                *Please fill out invoice number.
                              </small>
                            </div>
                          ) : (
                            ''
                          )}
                        </div>
                        <div className='form-group col-md-6 col-sm-6'>
                          <label>Date (DD-MM-YYYY)</label>
                          {/* <input
                            type="date"
                            name="incorport_date"
                            id="incorport_date"
                            onChange={event => this.handleChange(event)}
                            className="form-control"
                            required
                          /> */}
                          <div>
                            <DatePicker
                              onChange={this.onChange}
                              value={this.state.date}
                              className='form-control'
                              clearIcon={null}
                              clearAriaLabel='aria-label'
                              style={{ paddingLeft: 30 }}
                              format='dd-MM-yyyy'
                              placeholder='dd-mm-yyyy'
                            />
                          </div>
                        </div>
                        <div className='form-group col-md-12 col-xs-12'>
                          <label>Address</label>
                          <input
                            type='text'
                            cols='3'
                            rows='5'
                            name='address'
                            autoComplete='off'
                            className='form-control'
                            onChange={event => this.handleChange(event)}
                          />
                        </div>
                        <div className='form-group col-md-6 col-sm-6'>
                          <label>Currency</label>
                          <div className='custom-select-drop dropdown'>
                            <a
                              aria-expanded='false'
                              aria-haspopup='true'
                              role='button'
                              data-toggle='dropdown'
                              className='dropdown-toggle btn'
                              href='javascript:;'
                              value={this.state.selected}
                              required
                            >
                              <span
                                id='selected'
                                onChange={event => this.handleChange(event)}
                              >
                                {this.state.ToCurrency}
                              </span>
                              <span className='caret'></span>
                            </a>
                            <ul
                              className='dropdown-menu category'
                              style={{
                                height: 213,
                                overflow: 'scroll',
                                width: 'auto'
                              }}
                            >
                              <li>
                                <input
                                  type='text'
                                  name='search'
                                  className='form-control'
                                  placeholder='Search'
                                  id='_search'
                                  autoComplete='off'
                                  // onBlur={(e)=>{jQuery('#_search').val('');this.filter_currenciess(e)}}
                                  onChange={e => this.filter_currenciess(e)}
                                  required
                                />
                               
                              </li>
                              <li>
                                <ul className='list-unstyled'>
                                  {this.state.currencies.map((item, index) => {
                                    return (
                                      <li
                                        key={index}
                                        onClick={this.handleCheck_currency.bind(
                                          this
                                        )}
                                        name={item}
                                        data-namee={item}
                                        data-id={index}
                                      >
                                        <a
                                          href='javascript:;'
                                          value={item.name}
                                        >
                                          {item}
                                        </a>
                                      </li>
                                    )
                                  })}
                                </ul>
                              </li>
                            </ul>
                          </div>
                        </div>

                        <div className='form-group col-md-6 col-sm-6'>
                          <label>Default Category</label>
                          <div className='custom-select-drop dropdown'>
                            <a
                              aria-expanded='false'
                              aria-haspopup='true'
                              role='button'
                              data-toggle='dropdown'
                              className='dropdown-toggle btn'
                              href='javascript:;'
                              value={this.state.selected}
                              required
                            >
                              <span
                                id='selected'
                                onChange={event => this.handleChange(event)}
                              >
                                {this.state.selected != ''
                                  ? this.state.selected
                                  : 'Choose Category'}
                              </span>
                              <span className='caret'></span>
                            </a>
                            <ul
                              className='dropdown-menu category'
                              style={{
                                height: 213,
                                overflow: 'scroll',
                                width: 'auto'
                              }}
                            >
                              <li>
                                <input
                                  type='text'
                                  name='search'
                                  id='_search_def'
                                  className='form-control'
                                  placeholder='Search'
                                  // onBlur={(event)=>{jQuery('_search_def').val('');THIS.defaultcategorylist_onchange(event)}}
                                  autocomplete='off'
                                  onInput={event =>
                                    THIS.defaultcategorylist_onchange(event)
                                  }
                                  required
                                />
                                <button
                                  type='button'
                                  className='btn btn-rounded btn-blue'
                                  data-toggle='modal'
                                  data-target='#pop-modal'
                                >
                                  Add New
                                  <img
                                    className='arrow-icon'
                                    src='../../images/right-arrow.svg'
                                    alt='icon'
                                  />
                                </button>
                              </li>
                              <li>
                                <ul className='list-unstyled'>
                                  {this.state.default_category_list.map(
                                    (item, index) => {
                                      return (
                                        <li
                                          key={index}
                                          onClick={this.handleCheck.bind(this)}
                                          name={item}
                                          data-id={item.name}
                                          data-the={item.id}
                                        >
                                          <a
                                            href='javascript:;'
                                            value={item.name}
                                          >
                                            {item.name}
                                          </a>
                                        </li>
                                      )
                                    }
                                  )}
                                </ul>
                              </li>
                            </ul>
                          </div>
                        </div>

                        <div className='form-group col-md-6 col-sm-6'>
                          <label>GST</label>
                          <div className='custom-select-drop dropdown'>
                            <a
                              aria-expanded='false'
                              aria-haspopup='true'
                              role='button'
                              data-toggle='dropdown'
                              className='dropdown-toggle btn'
                              href='javascript:;'
                              value={this.state.selected}
                              required
                            >
                              <span
                                id='selected'
                                // onChange={event => this.handleChange(event)}
                              >
                                {this.state.sales_tax_name}
                              </span>
                              <span className='caret'></span>
                            </a>
                            <ul
                              className='dropdown-menu category'
                              style={{
                                height: 213,
                                overflow: 'scroll',
                                width: 'auto'
                              }}
                            >
                              <li>
                                <input
                                  type='text'
                                  name='search'
                                  autoComplete='off'
                                  className='form-control'
                                  placeholder='Search'
                                  onInput={event =>
                                    THIS.update_search_keyword(event)
                                  }
                                  required
                                />
                                <button
                                  type='button'
                                  className='btn btn-rounded btn-blue'
                                  data-toggle='modal'
                                  data-target='#pop-modal-1'
                                >
                                  Add New
                                  <img
                                    className='arrow-icon'
                                    src='../../images/right-arrow.svg'
                                    alt='icon'
                                  />
                                </button>
                              </li>
                              <li>
                               
                                <ul className='list-unstyled'>
                                  {this.state.gst_list.map((item, index) => {
                                    return (
                                      <li
                                        key={index}
                                        onClick={this.handleCheck_get_selected_tax.bind(
                                          this
                                        )}
                                        data-name={item.sales_tax_name}
                                        data-rate={item.rate}
                                        data-type={item.rate_type}
                                      >
                                        <a
                                          href='javascript:;'
                                          value={item.name}
                                        >
                                          {item.sales_tax_name}
                                        </a>
                                      </li>
                                    )
                                  })}
                                </ul>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <div
                          className='modal fade pop-modal'
                          id='pop-modal-1'
                          role='dialog'
                        >
                          <div className='modal-dialog modal-md custom-modal'>
                            <button
                              type='button'
                              className='close hidden-xs'
                              data-dismiss='modal'
                              onClick={this.cancel_gst_modal}
                            >
                              <img
                                className='img-responsive'
                                src='../../images/close-red.svg'
                                alt='icon'
                              />
                            </button>
                            <div className='modal-content'>
                              <div className='modal-body text-center'>
                                <h3>Add New GST</h3>
                                <form className='custom-form row'>
                                  <div className='form-group col-md-12 col-xs-12 pad-no'>
                                    <div className='col-md-4 col-sm-4 col-xs-12'>
                                      <label>Sales Tax Code</label>
                                    </div>
                                    <div className='col-md-8 col-sm-8 col-xs-12'>
                                      <input
                                        type='text'
                                        name='sales_tax_code'
                                        id='sales_tax_code'
                                        autoComplete='off'
                                        maxlength='4'
                                        className='form-control'
                                        onChange={event =>
                                          this.handleChange(event)
                                        }
                                        required
                                      />
                                     
                                      <p className='input-info'>
                                        (Maximum 4 characters)
                                      </p>
                                    </div>
                                  </div>
                                  <div className='form-group col-md-12 col-xs-12 pad-no'>
                                    <div className='col-md-4 col-sm-4 col-md-12'>
                                      <label>Sales Tax Name</label>
                                    </div>
                                    <div className='col-md-8 col-sm-8 col-xs-12'>
                                      <input
                                        type='text'
                                        name='salesTax_name_entered'
                                        autoComplete='off'
                                        id='sales_tax_name'
                                        className='form-control'
                                        onChange={event =>
                                          this.handleChange(event)
                                        }
                                        required
                                      />
                                    </div>
                                  </div>
                                  <div className='form-group col-md-12 col-xs-12 pad-no mar-b-no'>
                                    <div className='col-md-4 col-sm-4 col-xs-12'>
                                      <label>Tax Type</label>
                                    </div>
                                    <div className='col-md-8 col-sm-8 col-xs-12'>
                                      <label className='custom-checkbox radio mar-rgt taxable'>
                                        <input
                                          type='radio'
                                          name='tax-item'
                                          value='option1'
                                          checked={
                                            THIS.state.selectedOption ===
                                            'option1'
                                          }
                                          onChange={THIS.handleOptionChange}
                                        />
                                        Taxable
                                        <span className='checkmark'></span>
                                      </label>
                                      <label className='custom-checkbox radio non-taxable'>
                                        <input
                                          type='radio'
                                          name='tax-item'
                                          value='option2'
                                          checked={
                                            THIS.state.selectedOption ===
                                            'option2'
                                          }
                                          onChange={THIS.handleOptionChange}
                                        />{' '}
                                        Non-Taxable/Exempt
                                        <span className='checkmark'></span>
                                      </label>
                                      {THIS.state.selectedOption ===
                                      'option1' ? (
                                        <div className='hidden-field col-md-12 col-xs-12'>
                                          <div className='form-group'>
                                            <label className='mar-t-no mar-btm'>
                                              Tax item for purchases & sales
                                            </label>
                                            <div className='col-md-12'>
                                              <div className='row'>
                                                <label
                                                  className='mar-rgt'
                                                  style={{ marginTop: 8 }}
                                                >
                                                  Rate
                                                </label>
                                                {}
                                                <div className='input-group rate-input'>
                                                  <input
                                                    className='form-control'
                                                    type='text'
                                                    name='tax'
                                                    id='tax'
                                                    autoComplete='off'
                                                    required
                                                    onInput={event =>
                                                      THIS.handleChange_gst_type(
                                                        event
                                                      )
                                                    }
                                                  
                                                  />
                                                  <div className='input-group-btn'>
                                                    <div className='custom-select-drop dropdown'>
                                                      <a
                                                        aria-expanded='false'
                                                        aria-haspopup='true'
                                                        role='button'
                                                        data-toggle='dropdown'
                                                        className='dropdown-toggle btn'
                                                        href='javascript:;'
                                                      >
                                                        <span id='selected'>
                                                          {
                                                            this.state
                                                              .selected_rate_type
                                                          }
                                                        </span>
                                                        <span className='caret'></span>
                                                      </a>
                                                      <ul className='dropdown-menu'>
                                                        <li className='active'>
                                                          <a
                                                            href='javascript:;'
                                                            onClick={this.update_rate_type}
                                                          >
                                                            %
                                                          </a>
                                                        </li>
                                                        <li>
                                                          <a
                                                            href='javascript:;'
                                                            onClick={this.update_rate_fixed}
                                                          >
                                                            Fixed price
                                                          </a>
                                                        </li>
                                                      </ul>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      ) : (
                                        ''
                                      )}
                                    </div>
                                  </div>
                                  {/* <div className='mymsg'>{this.state.modal_info_msg}</div> */}
                                  <small
                                    style={{ color: 'red' }}
                                    className='mymsg'
                                  >
                                    {this.state.modal_info_msg}{' '}
                                  </small>

                                  <div className='form-group col-md-12 col-xs-12 btn-sec pad-no mar-b-no'>
                                    {this.state.show_succes ? (
                                      <div class='alert alert-success'>
                                        <strong>Success!</strong> Your new GST
                                        is added.
                                      </div>
                                    ) : (
                                      ''
                                    )}
                                    <button
                                      className='btn btn-lightgray'
                                      data-dismiss='modal'
                                      onClick={this.modal_cancel}
                                    >
                                      Cancel
                                    </button>
                                    <span>{'   '}</span>
                                    <button
                                      className='btn btn-green'
                                      type='submit'
                                      onClick={this.add_gst_details}
                                    >
                                      Save
                                    </button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div className='form-group col-md-6 col-sm-6'  >
                          <span className='form-label clearfix'>Taxes</span>
                          <label className='custom-checkbox'>
                            <input
                              type='checkbox'
                              checked={this.state.isChecked}
                              onChange={this.toggleChange}
                            />{' '}
                            Including Tax
                            <span className='checkmark'></span>
                          </label>
                        </div>
                        <form name='myform' id='myform'>
                          <div className='form-table' style={{width:'96%'}}>
                            <table id="table_custom" >
                              <thead>
                                <tr>
                                  <th>No</th>
                                  <th>Items</th>
                                  <th>Qty</th>
                                  <th>Unit price</th>
                                  <th>Sub Total</th>
                                  <th>Category</th>
                                  {this.state.coulmns &&
                                    this.state.coulmns.map((coulmn, index) => {
                                      return (
                                        <th
                                          key={coulmn}
                                          id={`header${index}`}
                                          contentEditable
                                          onInput={event =>
                                            THIS.handleChangeItems(
                                              event,
                                              THIS.state.rows.length - 1
                                            )
                                          }
                                          required
                                          placeholder='Editable'
                                        >
                                                     
                                          Editable
 
                                        </th>
                                      )
                                    })}
                                  <th></th>
                                </tr>
                              </thead>

                              <tbody>
                                {this.state.rows.map(function (row, index) {
                                  let itemid = index
                                  let myvalue =
                                    THIS.state.myarray.length > 0 &&
                                    THIS.state.myarray[itemid] &&
                                    THIS.state.myarray[itemid].price != '' &&
                                    THIS.state.myarray[itemid].price != 0
                                      ? THIS.state.myarray[
                                          itemid
                                        ].price
                                      : ''

                                  let selected_categeory =
                                    THIS.state.myarray.length > 0 &&
                                    THIS.state.myarray[itemid] &&
                                    THIS.state.myarray[itemid].category_id !=
                                      '' &&
                                    THIS.state.myarray[itemid].category_id != 0
                                      ? THIS.state.myarray[itemid].category_id
                                      : THIS.state.selected
                                  //   alert(itemid)
                                  return (
                                    <tr key={row} id={itemid}>
                                      <td> {itemid + 1} </td>
                                      <td>
                                        <input
                                          type='text'
                                          className='form-control no-bg'
                                          name='item_name'
                                          style={{ width: 50 }}
                                          autoComplete='off'
                                          id={`item${itemid}`}
                                          onInput={event =>
                                            THIS.handleChangeItems(
                                              event,
                                              THIS.state.rows.length-1
                                            )
                                          }
                                          required
                                        />
                                      </td>
                                      <td>
                                        <input
                                          type='text'
                                          className='form-control no-bg'
                                          name='quantity'
                                          style={{ width: 50 }}
                                          autoComplete='off'
                                          id={`quantity${itemid}`}
                                          onInput={event =>
                                            THIS.handleChangeItems(
                                              event,
                                              THIS.state.rows.length-1

                                            )
                                          }
                                          required
                                        />
                                      </td>
                                      <td>
                                        <input
                                          type='text'
                                          className='form-control no-bg'
                                          style={{ width: 50 }}
                                          name='unit_price'
                                          autoComplete='off'
                                          id={`unit_price${itemid}`}
                                          onInput={event =>
                                            THIS.handleChangeItems(
                                              event,
                                              THIS.state.rows.length-1

                                            )
                                          }
                                          required
                                        />
                                      </td>
                                      <td>
                                        <span
                                          type='text'
                                          className='form-control no-bg'
                                          name='sub_total'
                                          id={`subtotal${itemid}`}
                                          style={{
                                            textAlign: 'end',
                                            backgroundColor: 'none'
                                          }}
                                        >
                                          {isNaN(myvalue) ? '' : myvalue}
                                        </span>
                                      </td>
                                      <td>
                                        <div className='custom-select-drop dropdown tbl_drop_down'>
                                          <a
                                            aria-expanded='false'
                                            aria-haspopup='true'
                                            role='button'
                                            data-toggle='dropdown'
                                            className='dropdown-toggle btn'
                                            onClick={THIS.removeOverFlow.bind(
                                              THIS
                                            )}
                                            href='javascript:;'
                                            // value={THIS.state.selected}
                                            value={THIS.state.selected}

                                            required
                                          >
                                            <span
                                              id={`selectednow${itemid}`}
                                            >
                                              {THIS.state.selected != ''
                                                ? THIS.state.selected
                                                : 'choose'}{' '}
                                            </span>
                                            <span
                                              id={`selectednow_id${itemid}`}
                                              style={{ display: 'none' }}
                                            >
                                              NO_VALUE
                                            </span>
                                            <span className='caret'></span>
                                          </a>
                                          <ul
                                            className='dropdown-menu category'
                                            style={{
                                              height: 213,
                                              overflow: 'scroll',
                                              minWidth: '217%',
                                              left: -99
                                            }}
                                          >
                                            <li>
                                              <input
                                                type='text'
                                                name='search'
                                                id='_search_deff'
                                                className='form-control category_srch'
                                                // onBlur={(event)=>{jQuery('_search_deff').val(''); THIS.defaultcategorylist_onchange(
                                                //   event
                                                // )}}
                                                placeholder='Search'
                                                autocomplete='off'
                                                onInput={event =>
                                                  THIS.defaultcategorylist_onchange(
                                                    event
                                                  )
                                                }
                                                required
                                              />
                                              <button
                                                type='button'
                                                className='btn btn-rounded btn-blue'
                                                data-toggle='modal'
                                                data-target='#pop-modal'
                                              >
                                                Add New
                                                <img
                                                  className='arrow-icon'
                                                  src='../../images/right-arrow.svg'
                                                  alt='icon'
                                                />
                                              </button>
                                            </li>
                                            <li>
                                              <ul className='list-unstyled'>
                                                {THIS.state.default_category_list.map(
                                                  (item, index) => {
                                                    let myId = item.name
                                                    // alert(myId)
                                                    return (
                                                      <li
                                                        key={index}
                                                        onClick={e =>
                                                          THIS.changetext1(
                                                            item.id,
                                                            itemid,
                                                            'selectednow' +
                                                              itemid,
                                                            item.name
                                                          )
                                                        }
                                                        name={item}
                                                      >
                                                        <a
                                                          href='javascript:;'
                                                          value={item.name}
                                                        >
                                                          {item.name}
                                                        </a>
                                                      </li>
                                                    )
                                                  }
                                                )}
                                              </ul>
                                            </li>
                                          </ul>
                                        </div>
                                      </td>

                                      {THIS.state.coulmns!=undefined &&
                                        THIS.state.coulmns.map(
                                          (coulmn, index) => {
                                            return (
                                              <td key={coulmn}>
                                                <input
                                                  type='text'
                                                  className='form-control no-bg'
                                                  style={{ width: 50 }}
                                                  name='unit_price'
                                                  autoComplete='off'
                                                  id={`coulmn${itemid}${index}`}
                                                  onInput={event =>
                                                    THIS.handleChangeItems(
                                                      index,
                                                      THIS.state.rows.length - 1
                                                    )
                                                  }
                                                  required
                                                />{' '}
                                              </td>
                                            )
                                          }
                                        )}
                                      <td>
                                        {' '}
                                        <a
                                          href='javascript:;'
                                          class='add-col'
                                          data-toggle='modal'
                                          data-target='#modal_delete'
                                          // onClick={() =>
                                          //   THIS.delete_Rows(itemid)
                                          // }
                                          onClick={()=>{THIS.setState({specific_id_delete:itemid})}}
                                        >
                                          <img
                                            src='../../images/delete-icon.svg'
                                            alt='icon'
                                          />
                                        </a>
                                      </td>
                                    </tr>
                                  )
                                })}
                              </tbody>
                            </table>
                            <div>
                            <div
                            class='modal fade in'
                            id='modal_delete'
                            role='dialog'
                            style={{ paddingLeft: 15 }}
                          >
                            <div
                              class='modal-dialog modal-md'
                              style={{ width: 440 }}
                            >
                              <button
                                type='button'
                                class='close hidden-xs'
                                data-dismiss='modal'
                              >
                                <img
                                  class='img-responsive'
                                  src='../../images/close-red.svg'
                                  alt='icon'
                                />
                              </button>
                              <div class='modal-content'>
                                <div class='modal-body text-center success-modal'>
                                  <div class='pop-icon'>
                                    {/* <img src="images/template-success-icon.png" alt="icon"/> */}
                                  </div>
                                  <h3>Are you sure?</h3>

                                  <p class='fw-500'>
                                    Selected item will be deleted.
                                  </p>
                                  <button
                                    className='btn btn-lightgray'
                                    data-dismiss='modal'
                                  >
                                    Cancel
                                  </button>
                                  <span>{'   '}</span>
                                  <button
                                    class='btn btn-green'
                                    type='button'
                                    onClick={this.delete_Rows}
                                  >
                                    OK
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                            </div>
                            {/* <div style={{float:'right'}}><a
                              href='javascript:;'
                              class='add-col'
                              // id="btnAddCol"
                              onClick={() => THIS.add_coulmn()}
                            >
                              <img
                                src='../../images/plus-icon.svg'
                                alt='icon'
                              />
                            </a></div> */}
                            {/* <a
                              href='javascript:;'
                              class='add-col'
                              // id="btnAddCol"
                              onClick={() => THIS.add_coulmn()}
                            >
                              <img
                                src='../../images/plus-icon.svg'
                                alt='icon'
                              />
                            </a> */}
                            {/* <a href="javascript:;" className="add-col"><img src="../../images/plus-icon.svg" alt="icon"/></a> */}
                          </div>
                         
                          <a
                              href='javascript:;'
                              class='add-col'
                              // id="btnAddCol"
                              style={{float:'right'}}
                              onClick={() => THIS.add_coulmn()}
                            >
                              <img
                                src='../../images/plus-icon.svg'
                                alt='icon'
                              />
                            </a>
                        </form>
                        <div class='form-group'>
                          {' '}
                          {this.control_addButton()}
                          {this.state.isTable_notEmpty ? (
                            <div style={{ float: 'left' }}>
                              <small style={{ color: 'red' }}>
                                *Please fill out all table fields.
                              </small>
                            </div>
                          ) : (
                            ''
                          )}
                        </div>

                        <div className='form-group currency-label'>
                          <span>Foreign Currency({this.state.ToCurrency})</span>
                          <span> Home Currency(SGD) </span>
                        </div>
                        <div className='form-group total-input'>
                          <label>Item Total</label>
                          <div>
                            <span
                              type='text'
                              name='item_total_foreign_currency'
                              className='form-control'
                              onChange={this.convertCurrency}
                              style={{ textAlign: 'end' }}
                            >
                              {this.state.item_total_foreign_currency > 0
                                ? this.state.item_total_foreign_currency
                                : '0.00'}
                            </span>
                            <span
                              type='text'
                              name='item_total_home_currency'
                              className='form-control'
                              style={{ textAlign: 'end' }}
                            >
                              {this.state.item_total_foreign_currency > 0
                                ? this.state.item_total_home_currency
                                : '0.00'}
                            </span>
                          </div>
                        </div>
                        <div className='form-group total-input'>
                          <label>Tax</label>
                          <div>
                            <span
                              type='text'
                              name='tax_amount_foreign_currency'
                              className='form-control'
                              style={{ textAlign: 'end' }}
                            >
                              {this.state.item_total_foreign_currency > 0? this.state.tax_amount_foreign_currency: '0.00'}
                            </span>
                            <span
                              type='text'
                              name='tax_amount_home_currency'
                              className='form-control'
                              onChange={this.convertCurrency}
                              style={{ textAlign: 'end' }}
                            >
                              {this.state.item_total_foreign_currency > 0
                                ? this.state.tax_amount_home_currency
                                : '0.00'}
                            </span>
                          </div>
                        </div>

                        <div className='form-group total-input'>
                          <label>Grand Total</label>
                          <div>
                            <span
                              name='grand_total_foreign_currency'
                              className='form-control'
                              style={{ textAlign: 'end' }}
                            >
                              {this.state.item_total_foreign_currency > 0
                                ? this.state.grand_total_foreign_currency
                                : '0.00'}
                            </span>
                            <span
                              name='grand_total_home_currency'
                              className='form-control'
                              style={{ textAlign: 'end' }}
                              // onChange={this.convertCurrency}
                            >
                              {this.state.item_total_foreign_currency > 0
                                ? this.state.grand_total_home_currency
                                : '0.00'}
                            </span>
                          </div>
                        </div>
                        <div ref={this.myDivToFocus}></div>

                        <div className='form-group total-input' >
                          <label>Balance sheet category</label>
                          <div>
                            <div className='custom-select-drop dropdown'>
                              <a
                                aria-expanded='false'
                                aria-haspopup='true'
                                role='button'
                                data-toggle='dropdown'
                                className='dropdown-toggle btn'
                                href='javascript:;'
                                value={this.state.selected}
                                required
                              >
                                <span
                                  id='selected'
                                >
                                  {this.state.balance_sheet_category_name != ''
                                    ? this.state.balance_sheet_category_name
                                    : 'Choose'}{' '}
                                </span>
                                <span className='caret'></span>
                              </a>
                              <ul
                                className='dropdown-menu category'
                                style={{
                                  height: 213,
                                  overflow: 'scroll',
                                  width: 'auto'
                                }}
                              >
                                <li>
                                  <input
                                    type='text'
                                    name='search'
                                    className='form-control customInput_width'
                                    placeholder='Search'
                                    style={{ width: '100%' }}
                                    autocomplete='off'
                                    onInput={event =>
                                      THIS.onChange_filter_balancesheet(event)
                                    }
                                    required
                                  />
                                  <button
                                    type='button'
                                    className='btn btn-rounded btn-blue'
                                    data-toggle="modal"
                                    data-target="#pop-modal"
                                  >
                                    Add New
                                    <img
                                      className='arrow-icon'
                                      src='../../images/right-arrow.svg'
                                      alt='icon'
                                    />
                                  </button>
                                </li>
                                <li>
                                  <ul className='list-unstyled'>
                                    {THIS.state.balancesheetlist.map(
                                      (item, index) => {
                                        return (
                                          <li
                                            key={index}
                                            onClick={this.handleCheck_balanceSheet_id.bind(
                                              this
                                            )}
                                            data-namee={item.name}
                                            data-id={item.id}
                                            name={item}
                                          >
                                            <a
                                              href='javascript:;'
                                              value={item.name}
                                            >
                                              {item.name}
                                            </a>
                                          </li>
                                        )
                                      }
                                    )}
                                  </ul>
                                 
                                </li>
                              </ul>
                            </div>

                            {!this.state.isBalance_sheet_category_name ? (
                            <div style={{ float: 'left' }}>
                              <small style={{ color: 'red' }}>
                                *Please choose balance sheet categeory.
                              </small>
                            </div>
                          ) : (
                            ''
                          )}
                          </div>
                        </div>
                        {this.state.isClose ? (
                          <div
                            class='alert alert-card success alert-dismissible fade in'
                            id='closeme'
                          >
                            <a
                              href='#'
                              class='close'
                              data-dismiss='alert'
                              aria-label='close'
                            >
                              &times;
                            </a>
                            <div class='img-wrap'>
                              <img
                                class='img-responsive'
                                src='../../images/alert-success.svg'
                                alt='icon'
                              />
                            </div>
                            <div class='alert-cont'>
                              <strong class='title'>success!</strong>Tagged
                              items saved successfully.
                            </div>
                          </div>
                        ) : (
                          ''
                        )}
                        {this.state.isClose1 ? (
                          <div
                            class='alert alert-card success alert-dismissible fade hide'
                            id='closeme1'
                          >
                            <a
                              href='#'
                              class='close'
                              data-dismiss='alert'
                              aria-label='close'
                            >
                              &times;
                            </a>
                            <div class='img-wrap'>
                              <img
                                class='img-responsive'
                                src='../../images/alert-success.svg'
                                alt='icon'
                              />
                            </div>
                            <div class='alert-cont'>
                              <strong class='title'>success!</strong>Tagged
                              items saved in Draft successfully.
                            </div>
                          </div>
                        ) : (
                          ''
                        )}

                        <div className='form-group exchange-rate'>
                          <label>Exchange Rate 1 {this.state.ToCurrency}</label>
                          <div>
                            <span
                              type='text'
                              name='exchangeRate'
                              className='form-control'
                            >
                              {this.state.exchange_value}
                            </span>
                            <span className='label'>SGD</span>
                          </div>
                        </div>
                        <div className='form-group total-input text-right'>
                          {/* {this.state.isAdd ? (
                            <div style={{ float: 'left' }}>
                              <small style={{ color: 'red' }}>
                                *Please fill out all the fields.
                              </small>
                            </div>
                          ) : (
                            ''
                          )} */}
                          <div className='submit-enclose'>
                            <button
                              className='btn btn-gray'
                              type='button'
                              data-toggle='modal'
                              data-target='#successModal'
                            >
                              Clear
                            </button>
                            <span> </span>
                            <button
                              className='btn btn-yellow'
                              type='submit'
                              onClick={this.save_draft}
                            >
                              Save Draft
                            </button>
                            <span> </span>

                            <button
                              className='btn btn-green'
                              type='submit'
                              onClick={this.saveAndContinue}
                            >
                              Save & Continue
                            </button>
                          </div>
                          <div
                            class='modal fade in'
                            id='successModal'
                            role='dialog'
                            style={{ paddingLeft: 15 }}
                          >
                            <div
                              class='modal-dialog modal-md'
                              style={{ width: 440 }}
                            >
                              <button
                                type='button'
                                class='close hidden-xs'
                                data-dismiss='modal'
                              >
                                <img
                                  class='img-responsive'
                                  src='../../images/close-red.svg'
                                  alt='icon'
                                />
                              </button>
                              <div class='modal-content'>
                                <div class='modal-body text-center success-modal'>
                                  <div class='pop-icon'>
                                    {/* <img src="images/template-success-icon.png" alt="icon"/> */}
                                  </div>
                                  <h3>Are you sure?</h3>

                                  <p class='fw-500'>
                                    Your all tagged items will be cleared
                                  </p>
                                  <button
                                    className='btn btn-lightgray'
                                    data-dismiss='modal'
                                  >
                                    Cancel
                                  </button>
                                  <span>{'   '}</span>
                                  <button
                                    class='btn btn-green'
                                    type='button'
                                    onClick={this.clear_tagged_items}
                                  >
                                    OK
                                  </button>
                                </div>
                              </div>

                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer logoutSubmit={e => this.logoutLink()} />
      </div>
    )
  }
}
export default data_tagging
