import React from 'react';
//import './App.css';

import Commonlinks from './commonlinks.js';

function App() {
  return (
    <div className="App">
      <Commonlinks />
    </div>
  );
}

export default App;
