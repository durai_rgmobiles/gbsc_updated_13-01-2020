import React from 'react';

import jQuery from 'jquery';

class left_sidebar extends React.Component {

    constructor(props) {
        super(props);
        //const { history } = this.props;
        this.state = {initialActiveInbox:'active',
             logged_user_id: localStorage.getItem("logged_user_id"), logged_client_id: localStorage.getItem("logged_client_id"), logged_role_id: localStorage.getItem("logged_role_id"), logged_user_name: localStorage.getItem("logged_user_name"), logged_user_email: localStorage.getItem("logged_user_email"), logged_user_phone: localStorage.getItem("logged_user_phone"), logged_user_image: localStorage.getItem("logged_user_image"), logged_company_name: localStorage.getItem("logged_company_name"), dropdown: '', inbox_list: [], response_stus: 0, response_msg: 'No data found', item_details: '', item_file_path: '' };
    }

    componentDidMount(){
        require('jquery-mousewheel');
        require('malihu-custom-scrollbar-plugin');

        jQuery(".mscroll-y").mCustomScrollbar({
            axis:"y",
            scrollEasing:"linear",
            scrollInertia: 600,
            autoHideScrollbar: "true",
            autoExpandScrollbar: "true"
        });
    }

    componentWillMount(){

        var currentLocation = window.location.pathname; 
        this.setState({
           currentLocation:currentLocation
        }) }

    // routedChange(parameter){
    //     this.props.history.push('/'+parameter);
    //     window.scrollTo(0,0);
    // }

    routedChange(page_slug) {
        this.props.pageSubmit(page_slug);
    }

    render() {
    let roleid = this.state.logged_role_id;
    //console.log('role_id', roleid);
    return (
    <div>

        <div className="left-navbar" data-user-id={this.state.logged_user_id} data-client-id={this.state.logged_client_id}>
            <div className="mscroll-y">
                <div className="nav-brand text-center hidden-xs"><img src="../../images/nav-brand-transparent.png" alt="Genie" /></div>
                <ul className="left-navmenu list-unstyled">
                    {roleid === "1" ? (
                    <li><a href="javascript:;">Dashboard</a></li>
                    ) : (<li><a href="javascript:;">Dashboard</a></li>)}
                    
                    {roleid === "1" ? (
                    <li><span className="item-head">Recently Used</span></li>
                    ) : ''}
                    
                    {roleid === "1" ? (
                    <li><a href="javascript:;">Profit & Loss</a></li>
                    ) : ''}

                    {roleid === "1" ? (
                    <li><a href="javascript:;">General Ledger</a></li>
                    ) : ''}

                    {roleid === "1" ? (
                    <li><a href="javascript:;">Balance Sheet</a></li>
                    ) : ''}

                    {roleid === "1" ? (
                    <li><a href="javascript:;">Customers</a></li>
                    ) : ''}

                    {roleid === "1" ? (
                    <li><span className="item-head">Documents</span></li>
                    ) : ''}

                    {roleid === "1" ? (
                    <li><a onClick={this.routedChange.bind(this,'user_inbox')} className={(this.state.currentLocation == '/user_inbox' ? 'active' : '')} >Inbox</a></li>
                    ) : (<li><a onClick={this.routedChange.bind(this,'inbox')}  className={(this.state.currentLocation == '/inbox' ? 'active' : '')}>Inbox</a></li>)}

                    {roleid === "2" ? (
                    <li><a onClick={this.routedChange.bind(this,'sent_items')}  className={(this.state.currentLocation == '/sent_items' ? 'active' : '')}>Sent Items</a></li>
                    ) : ''}   
                    
                    {roleid === "1" ? (
                    <li><a onClick={this.routedChange.bind(this,'reviewed_items')} className={(this.state.currentLocation == '/reviewed_items' ? 'active' : '')} >Reviewed</a></li>
                    ) : (<li><a onClick={this.routedChange.bind(this,'reviewed_items')}  className={(this.state.currentLocation == '/reviewed_items' ? 'active' : '')}>Reviewed</a></li>
                    )}

                    {roleid === "1" ? (
                    <li><span className="item-head">Accounting</span></li>
                    ) : ''}

                    {roleid === "1" ? (
                    <li>
                        <a href="javascript:;" className="has-sub">Lists</a>
                        <ul className="list-unstyled sub-menu">
                            <li><a href="javascript:;">Chart of Accounts</a></li>
                            <li><a href="javascript:;">Item List</a></li>
                            <li><a href="javascript:;">Fixed Asset Item List</a></li>
                            <li><a href="javascript:;">Currency List</a></li>
                        </ul>
                    </li>
                    ) : ''}

                    {roleid === "1" ? (
                    <li><a href="javascript:;">Accountant</a></li>
                    ) : ''}

                    {roleid === "1" ? (
                    <li><a href="javascript:;">Company</a></li>
                    ) : ''}

                    {roleid === "1" ? (
                    <li><a href="javascript:;">Sales Tax</a></li>
                    ) : ''}

                    {roleid === "1" ? (
                    <li><a href="javascript:;">Customers</a></li>
                    ) : ''}

                    {roleid === "1" ? (
                    <li><a href="javascript:;">Vendors</a></li>
                    ) : ''}

                    {roleid === "1" ? (
                    <li><a href="javascript:;">Employees</a></li>
                    ) : ''}

                    {roleid === "1" ? (
                    <li><a href="javascript:;">Banking</a></li>
                    ) : ''}

                    {roleid === "1" ? (
                    <li><a href="javascript:;">Reports</a></li>
                    ) : ''}
                </ul>
            </div>
        </div>
        <div className="menu-close visible-xs">&nbsp;</div>

    </div>
    );
  }
}
export default left_sidebar;