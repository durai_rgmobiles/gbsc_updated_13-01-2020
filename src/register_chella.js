import React, { Component } from "react";
import jQuery from "jquery";
import DatePicker from "react-datepicker";
import FetchAllApi from "./api_links/fetch_all_api.js";

import "react-datepicker/dist/react-datepicker.css";

export default class register extends React.Component {
  constructor(props) {
    super(props);
    //const { history } = this.props;
    this.state = {fname: "",lname: "",email: "",contact: "",password: "",cpassword: "",entityname: "",entitynum: "",new: "",date: "",
      contact: "",
      PrincipleActivities: "",
      home_currency:"",
      phnum: "",
      Email: "",
      Address: "",
      country: "",
      stateSelected: "",
      startDate: "",
      selectedDate: "",
      formIsnotEmpty: false,
      notNullfname: false,
      Isfname: false,
      province: "",
      Islname: false,
      Isemail: false,
      Iscontact: false,
      Ispassword: false,
      IsmatchPassword: false,
      Isprovince: false,
      Iscountry: false,
      Isentityname: false,
      Isentitynum: false,
      IsAddress: false,
      Isphnum: false,
      IsPrincipleActivities: false,
      IsEmail: false,
      IsEverythingOk: false,
      IsreadytoFetch: false,currencytype:""
    };
  }

  componentDidMount() {
    jQuery("a.back").click(function() {
      jQuery(".form-step2").hide();

      jQuery(".form-step1").show();
    });
    //    this. fnameValidate();
    var THIS = this;
    jQuery("#picker1").change(function() {
      var selectedItem = jQuery("#picker1").val();
      THIS.setState({ country: selectedItem });
    });

    jQuery("#currencytype").change(function() {
      var selectedItem = jQuery("#currencytype").val();
      THIS.setState({ currencytype: selectedItem });
    });

    
    jQuery("#picker2").change(function() {
      var selectedItem = jQuery("#picker2").val();
      //alert(selectedItem);
      THIS.setState({ stateSelected: selectedItem });
    });
    jQuery(document).ready(function() {
      jQuery(".form-step1 .next").click(function() {
        THIS.formValidation();

        //   if (THIS.state.lname.length > 0) {
        if (THIS.state.formIsnotEmpty) {
          jQuery(".form-step1").hide();
          jQuery(".form-step2").show();
        }
      });

      jQuery(".pass-visible").click(function() {
        jQuery(this).toggleClass("off");
        var input = jQuery(jQuery(this).attr("toggle"));
        if (input.attr("type") == "password") {
          input.attr("type", "text");
        } else {
          input.attr("type", "password");
        }
      });

      jQuery(".dropdown-menu li a").on("click", function() {
        var getValue = jQuery(this).text();
        jQuery(".dropdown-select .btn-value").text(getValue);
      });

      // jQuery('.datepicker').datepicker();
    });

    jQuery(".add-input").click(function() {
      jQuery(
        "<div class='form-group clone'><input class='form-control'/><a href='javascript:;' class='remove-input'><img class='img-responsive' src='images/close-icon-red.svg'/></a></div>"
      ).insertBefore(this);
    });

    jQuery(".form-group").on("click", ".remove-input", function() {
      jQuery(this)
        .parent(".form-group")
        .remove();
    });
  }

  handleChange(event) {
    // alert();
    this.setState({ [event.target.name]: event.target.value });
    // this.clearValidation({ [event.target.name]: event.target.value});
    this.formValidation();
  }

  formValidation = () => {
    if (this.state.fname != "") {
      this.setState({ Isfname: false });
    } else {
      this.setState({ Isfname: true });
    }

    if (this.state.lname != "") {
      this.setState({ Islname: false });
    } else {
      this.setState({ Islname: true });
    }

    if (this.state.email != "") {
      this.setState({ Isemail: false });
    } else {
      this.setState({ Isemail: true });
    }

    if (this.state.contact != "") {
      this.setState({ Iscontact: false });
    } else {
      this.setState({ Iscontact: true });
    }

    if (this.state.country != "") {
      this.setState({ Iscountry: false });
    } else {
      this.setState({ Iscountry: true });
    }

    if (this.state.stateSelected != "") {
      this.setState({ Isprovince: false });
    } else {
      this.setState({ Isprovince: true });
    }

    if (this.state.password != "") {
      this.setState({ Ispassword: false });
    } else {
      this.setState({ Ispassword: true });
    }

    if (this.state.password === this.state.cpassword) {
      this.setState({ IsmatchPassword: false });
    } else {
      this.setState({ IsmatchPassword: true });
    }
    if (
      this.state.fname &&
      this.state.lname &&
      this.state.email &&
      this.state.contact &&
      this.state.country &&
      this.state.stateSelected &&
      this.state.password != "" &&
      this.state.password == this.state.cpassword
    ) {
      this.setState({ formIsnotEmpty: true });
    }
  };

  onRegister = () => {
    // let entity_name = "DAR technoligies";
    // let address = "third street";
    // let state = "Tamilnadu";
    // let country = "India";
    // let email = "sakthii@mailinator.com";
    // let phone = "9845454521";
    let home_currency = this.state.currencytype;
    let country_code = "+91";
    // let entity_number = "WQ232323";
    let entity_type = 1;

    // let principle_activities = "Testing";
    // let first_name = "raj";
    // let last_name = "kkkk";
    // let company_email = ["sassi9@rgmobiles.com"];
    // let company_phone = ["4545478787455", "78787878787"];
    // let password = "12345689";
    let plan_id = 1;
    let subscription_start_date = "2019-11-01";
    let subscription_end_date = "2019-11-30";
    this.formValidateNext();
    let registerDetails = {
      entity_name: this.state.entityname,
      address: this.state.Address,
      state: this.state.stateSelected,
      country: this.state.country,
      email: this.state.email,
      phone: this.state.contact,
      home_currency: home_currency,
      country_code: country_code,
      entity_number: this.state.entitynum,
      incorporation_date: "2019-01-30",
      principle_activities: this.state.PrincipleActivities,
      entity_type: entity_type,
      first_name: this.state.fname,
      last_name: this.state.lname,
      company_email: this.state.Email,
      company_phone: [this.state.phnum],
      password: this.state.password,
      plan_id: plan_id,
      subscription_start_date: subscription_start_date,
      subscription_end_date: subscription_end_date
    };

    console.log("registerDetails", registerDetails);
    if (this.state.IsEverythingOk) {
      FetchAllApi.registerNewCompany(registerDetails, (err, response) => {
        //alert(response)
        console.log("userdajjjjjjjjjjjjjjjjjjta", response);

        console.log("erwrjhgjkghkasdjgjkgasdgajkdghjkdg", response.message);
        if (response.status === 1) {
          this.props.history.push("/register_Payment");
          alert(response.message);
        } else {
          alert(response.message);
        }
      });
    }
  };
  formValidateNext = () => {
    if (this.state.entityname != "") {
      this.setState({ Isentityname: false });
    } else {
      this.setState({ Isentityname: true });
    }

    if (this.state.Address != "") {
      this.setState({ IsAddress: false });
    } else {
      this.setState({ IsAddress: true });
    }

    if (this.state.PrincipleActivities != "") {
      this.setState({ IsPrincipleActivities: false });
    } else {
      this.setState({ IsPrincipleActivities: true });
    }

    if (this.state.entitynum != "") {
      this.setState({ Isentitynum: false });
    } else {
      this.setState({ Isentitynum: true });
    }

    if (this.state.Email != "") {
      this.setState({ IsEmail: false });
    } else {
      this.setState({ IsEmail: true });
    }

    if (this.state.phnum != "") {
      this.setState({ Isphnum: false });
    } else {
      this.setState({ Isphnum: true });
    }
    if(this.state.currencytype !=""){
      this.setState({ Iscurrency: false });
    } else {
      this.setState({ Iscurrency: true });
    }
    if (
      this.state.Address &&
      this.state.PrincipleActivities &&
      this.state.entitynum &&
      this.state.entityname &&
      this.state.Email &&
      this.state.currencytype&&
      this.state.phnum != ""
    ) {
      this.setState({ IsEverythingOk: true });
    } else {
      this.setState({ IsEverythingOk: false });
    }
  };
  fnameValidate1 = () => {
    this.setState({ Isfname: false });
  };
  fnameValidate2 = () => {
    this.setState({ Islname: false });
  };
  fnameValidate3 = () => {
    this.setState({ Iscountry: false });
  };
  fnameValidate4 = () => {
    this.setState({ Isprovince: false });
  };
  fnameValidate5 = () => {
    this.setState({ Isemail: false });
  };
  fnameValidate6 = () => {
    this.setState({ Iscontact: false });
  };
  fnameValidate7 = () => {
    this.setState({ Ispassword: false });
  };
  fnameValidate8 = () => {
    this.setState({ IsmatchPassword: false });
  };

  render() {
    console.log("qwerty", this.state.entitynum);
    return (
      <div>
        <div class="container-fluid">
          <div class="row dflex">
            <div class="col-md-4 col-sm-4 register-left hidden-xs">
              <div class="nav-brand text-center">
                <img
                  class="img-responsive"
                  src="images/nav-brand-transparent.png"
                  alt="Logo"
                />
              </div>
              <h1>Genie lorem ipsum dolor</h1>
              <ul class="list-unstyled">
                <li>
                  <img src="images/ease-icon.svg" alt="icon" />
                  <span>
                    Ease of deployment of customised process solutions
                  </span>
                </li>
                <li>
                  <img src="images/performance-icon.svg" alt="icon" />
                  <span>Process performance visibilty</span>
                </li>
                <li>
                  <img src="images/increase-icon.svg" alt="icon" />
                  <span>Finance process flexibility to scale</span>
                </li>
                <li>
                  <img src="images/accuracy-icon.svg" alt="icon" />
                  <span>Data accuracy</span>
                </li>
                <li>
                  <img src="images/fulltime-icon.svg" alt="icon" />
                  <span>24/7 Operational capability</span>
                </li>
                <li>
                  <img src="images/cost-icon.svg" alt="icon" />
                  <span>Affordable cost</span>
                </li>
              </ul>
            </div>

            <div class="col-md-8 col-sm-8 register-right">
              <div class="register-form col-md-12 col-xs-12">
                <div class="nav-brand text-center visible-xs">
                  <img
                    class="img-responsive"
                    src="images/logo-genie.png"
                    alt="Logo"
                  />
                </div>
                <div class="register-head col-md-12 col-xs-12">
                  <a href="javascript:;" class="back regBack">
                    <img src="images/back-arrow-blue.svg" />
                  </a>
                  <span>
                    Ready to Get Started?
                    <br />
                    <small>Please fill with your details</small>
                  </span>
                </div>
                <div class="formstep-enclose step-1 col-md-12 col-xs-12 pad-no">
                  <div class="form-step1 col-md-12 col-xs-12 pad-no">
                    <form action="#" class="custom-form">
                      <div class="form-group col-md-6">
                        <label>
                          First Name<span class="astrick">*</span>
                        </label>
                        <input
                          type="text"
                          name="fname"
                          onChange={event => this.handleChange(event)}
                          class="form-control"
                          onClick={this.fnameValidate1}
                        />
                        {this.state.Isfname ? (
                          <small className="text-red">
                            *Please enter your first name
                          </small>
                        ) : (
                          ""
                        )}
                      </div>
                      <div class="form-group col-md-6">
                        <label>
                          Last Name<span class="astrick">*</span>
                        </label>
                        <input
                          type="text"
                          name="lname"
                          onChange={event => this.handleChange(event)}
                          class="form-control"
                          onClick={this.fnameValidate2}
                        />{" "}
                        {this.state.Islname ? (
                          <small className="text-red">
                            *Please enter your last name
                          </small>
                        ) : (
                          ""
                        )}
                      </div>
                      <div
                        class="form-group col-md-6"
                        // onClick={this.formValidation}
                        onClick={this.fnameValidate3}
                      >
                        <label>Country / Region</label>
                        <select
                          class="select-dropdown selectpicker"
                          data-live-search="true"
                          title="Choose..."
                          id="picker1"
                        >
                          <option value="Singapore">Singapore</option>
                          <option value="India">India</option>
                          <option value="United States">United States</option>
                          <option value="Australia">Australia</option>
                          <option value="Japanese">Japanese</option>
                          <option value="Mexicon">Mexicon</option>
                          <option value="Brazil">Brazil</option>
                        </select>
                        {this.state.Iscountry ? (
                          <small className="text-red">
                            *Please select your country
                          </small>
                        ) : (
                          ""
                        )}
                      </div>
                      <div
                        class="form-group col-md-6"
                        onClick={this.fnameValidate4}
                      >
                        <label>State / Province</label>
                        <select
                          class="select-dropdown selectpicker"
                          data-live-search="true"
                          name="province"
                          title="Choose..."
                          id="picker2"
                        >
                          <option value="Tamilnadu">Tamilnadu</option>
                          <option value="Bukit Merah">Bukit Merah</option>
                          <option value="Bukit Tima">Bukit Timah</option>
                          <option value="Downtown Core">Downtown Core</option>
                          <option value="Geylang">Geylang</option>
                          <option value="Kallang">Kallang</option>
                          <option value="Marina East">Marina East</option>
                        </select>
                        {this.state.Isprovince ? (
                          <small className="text-red">
                            *Please select your state
                          </small>
                        ) : (
                          ""
                        )}
                      </div>
                      <div class="form-group col-md-6">
                        <label>Email</label>
                        <input
                          type="text"
                          name="email"
                          onChange={event => this.handleChange(event)}
                          class="form-control"
                          onClick={this.fnameValidate5}
                        />{" "}
                        {this.state.Isemail ? (
                          <small className="text-red">
                            *Please enter your email
                          </small>
                        ) : (
                          ""
                        )}
                      </div>
                      <div class="form-group col-md-6">
                        <label>Phone</label>
                        <input
                          type="tel"
                          name="contact"
                          onChange={event => this.handleChange(event)}
                          class="form-control"
                          onClick={this.fnameValidate6}
                        />{" "}
                        {this.state.Iscontact ? (
                          <small className="text-red">
                            *Please enter your phone number
                          </small>
                        ) : (
                          ""
                        )}
                      </div>
                      <div class="form-group col-md-6">
                        <label>Password</label>
                        <i class="pass-visible" toggle="#password-field">
                          <img
                            class="off"
                            src="images/visibility-off.svg"
                            alt="icon"
                          />
                          <img
                            class="on"
                            src="images/visibility.svg"
                            alt="icon"
                          />
                        </i>
                        <input
                          id="password-field"
                          type="password"
                          name="password"
                          onChange={event => this.handleChange(event)}
                          class="form-control"
                          onClick={this.fnameValidate7}
                        />
                        {this.state.Ispassword ? (
                          <small className="text-red">
                            *Please enter your password
                          </small>
                        ) : (
                          ""
                        )}
                      </div>
                      <div class="form-group col-md-6">
                        <label>Confirm Password</label>
                        <i class="pass-visible" toggle="#password-fieldc">
                          <img
                            class="off"
                            src="images/visibility-off.svg"
                            alt="icon"
                          />
                          <img
                            class="on"
                            src="images/visibility.svg"
                            alt="icon"
                          />
                        </i>
                        <input
                          id="password-fieldc"
                          type="password"
                          name="cpassword"
                          onChange={event => this.handleChange(event)}
                          class="form-control"
                          onClick={this.fnameValidate}
                        />
                        {this.state.IsmatchPassword ? (
                          <small className="text-red">
                            *Password don't match
                          </small>
                        ) : (
                          ""
                        )}
                      </div>

                      <div class="col-md-12 col-xs-12 text-right">
                        <button
                          type="button"
                          class="btn btn-blue btn-rounded next"
                        >
                          Next
                        </button>
                      </div>
                    </form>
                  </div>
                  <div class="form-step2 col-md-12 col-xs-12 pad-no">
                    <form action="#" class="custom-form">
                      <div class="form-group col-md-6">
                        <label>Entity Name</label>
                        <input
                          type="text"
                          name="entityname"
                          onChange={event => this.handleChange(event)}
                          class="form-control"
                        />
                        {this.state.Isentityname ? (
                          <small className="text-red">
                            *Please enter your Entity Name
                          </small>
                        ) : (
                          ""
                        )}
                      </div>
                      <div class="form-group col-md-6">
                        <label>Unique Entity Number (UEN)</label>
                        <input
                          type="text"
                          name="entitynum"
                          onChange={event => this.handleChange(event)}
                          class="form-control"
                        />
                        {this.state.Isentitynum ? (
                          <small className="text-red">
                            *Please enter your Unique Entity Number
                          </small>
                        ) : (
                          ""
                        )}
                      </div>
                      <div class="form-group col-md-6">
                        <label>Entity Type</label>
                        {/* <select class="select-dropdown selectpicker">
                                    <option>Sole proprietor</option>
                                    <option>Corporation, One or more shareholders </option>
                                    <option>Small business corporation, Two or more owners</option>
                                    <option>Partnership</option>
                                    <option>Limited Partnership</option>
                                    <option>Limited liability partnership</option>
                                    <option>Others</option>
                                </select>  */}
                        <div class="dropdown custom-select-drop">
                          <button
                            id="dLabel"
                            class=" form-control dropdown-select"
                            type="button"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                          >
                            <span class="btn-value">Choose...</span>
                            <span class="bs-caret">
                              <span class="caret"></span>
                            </span>
                          </button>
                          <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li class="create-new">
                              <input
                                type="text"
                                name="new"
                                onChange={event => this.handleChange(event)}
                                class="form-control"
                                placeholder="Create New..."
                              />
                              <button class="btn btn-blue btn-rounded">
                                Create New <strong>"Loan"</strong>
                                <img
                                  src="images/btn-arrow-white.svg"
                                  alt="Icon"
                                />
                              </button>
                            </li>
                            <li>
                              <a href="javascript:;">Sole proprietor</a>
                            </li>
                            <li>
                              <a href="javascript:;">
                                Corporation, One or more shareholders
                              </a>
                            </li>
                            <li>
                              <a href="javascript:;">
                                Small business corporation, Two or more owners
                              </a>
                            </li>
                            <li>
                              <a href="javascript:;">Partnership</a>
                            </li>
                            <li>
                              <a href="javascript:;">Limited Partnership</a>
                            </li>
                            <li>
                              <a href="javascript:;">
                                Limited liability partnership
                              </a>
                            </li>
                            <li>
                              <a href="javascript:;">Others</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="form-group col-md-6">
                        <label>
                          Incorporation Date<span class="astrick">*</span>
                        </label>
                        <div
                          class="input-group date"
                          data-date-format="dd/mm/yyyy"
                          placeholder="yyyy/mm/dd"
                        >
                          <input type="text" class="form-control" />
                          {/* <input
                            placeholder="dd/mm/yyyy"
                            type="text"
                            name="date"
                            onChange={event => this.handleChange(event)}
                            class="form-control datepicker"
                          /> */}
                          <div class="input-group-addon">
                            <img src="images/calendar-icon.svg" alt="icon" />
                          </div>
                        </div>
                      </div>
                      <div class="form-group col-md-6">
                        <label>
                          Home Currency<span class="astrick">*</span>
                        </label>
                        <select
                          class="select-dropdown selectpicker"
                          data-live-search="true"
                          title="Choose..."
                          id="currencytype"
                        >
                          <option value="SGD">Singapore - SGD</option>
                          <option value="INR">India - INR</option>
                          <option value="USD">United States - USD</option>
                          <option value="AUD">Australia - AUD</option>
                          <option value="JPY">Japanese - JPY</option>
                          <option value="MXN">Mexicon - MXN</option>
                          <option value="BRL">Brazil - BRL</option>
                        </select>
                        {this.state.Iscurrency?<small className="text-red">
                            *Please select currency type
                          </small>:""}
                      </div>
                      <div class="form-group col-md-6">
                        <label>Principle Activities</label>
                        <input
                          type="text"
                          name="PrincipleActivities"
                          onChange={event => this.handleChange(event)}
                          class="form-control"
                        />
                        {this.state.IsPrincipleActivities ? (
                          <small className="text-red">
                            *Please enter your Principle Activities
                          </small>
                        ) : (
                          ""
                        )}
                      </div>
                      <div class="form-group col-md-6">
                        <label>Phone</label>
                        <input
                          type="tel"
                          name="phnum"
                          onChange={event => this.handleChange(event)}
                          class="form-control"
                        />{" "}
                        {this.state.Isphnum ? (
                          <small className="text-red">
                            *Please enter the Phone number
                          </small>
                        ) : (
                          ""
                        )}
                        <a href="javascript:;" class="add-input">
                          ADD MORE
                        </a>
                      </div>
                      <div class="form-group col-md-6">
                        <label>Email</label>
                        <input
                          type="text"
                          name="Email"
                          onChange={event => this.handleChange(event)}
                          class="form-control"
                        />{" "}
                        {this.state.IsEmail ? (
                          <small className="text-red">
                            *Please enter the Email
                          </small>
                        ) : (
                          ""
                        )}
                        <a href="javascript:;" class="add-input">
                          ADD MORE
                        </a>
                      </div>
                      <div class="form-group col-md-12 col-xs-12">
                        <label>Address</label>
                        <textarea
                          class="form-control"
                          cols="8"
                          rows="5"
                          name="Address"
                          onChange={event => this.handleChange(event)}
                        ></textarea>
                        {this.state.IsAddress ? (
                          <small className="text-red">
                            *Please enter your address
                          </small>
                        ) : (
                          ""
                        )}
                      </div>
                      <div class="col-md-12 col-xs-12 text-right">
                        <button
                          type="button"
                          class="btn btn-rounded btn-blue"
                          onClick={this.onRegister}
                        >
                          Next
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
