// NPM used for scroll is imported from 'npm i react-horizontal-scroll-container'
import React, { Component } from 'react'
import jQuery from 'jquery'
import LeftSidebar from './left_sidebar.js'
import HorizontalScroller from 'react-horizontal-scroll-container'
import Footer from './footer.js'
import Topbar from './topbar.js'
import DatePicker from 'react-datepicker'
import FetchAllApi from './api_links/fetch_all_api.js'

export default class rolesAndPermissions extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      responseData: [], shown: true, isChecked: true, newrole: '', item_name: '', isUpdated: ''
    }
    this.myDivToFocus = React.createRef()

  }
  componentDidMount () {
    this.callMe();
    jQuery(document).ready(function () {
      jQuery('.container').css('background', '#F8F8F8')
    })
  }

  addNewRoleAPI = () => {
    let role_name = this.state.newrole
    let permissions = [1, 3, 7, 10]

    FetchAllApi.addUserRole(role_name, permissions, (err, response) => {
      if (response.status === 1) {
        //alert(response.message)
        this.setState({ shown: !this.state.shown,isUpdated:response.message });
        this.jQueryStatusAlert();
        this.callMe();
        this.handleOnClick();
      } else {
        this.jQueryStatusAlert();
        this.setState({isUpdated:response.message})
      }
    })
  }

  addNewRole = inputfromuser => {
    let input = inputfromuser
    this.setState({
      newrole: input
    })
  }

  callMe = () => {
    FetchAllApi.getUserRoles((err, response) => {
      console.log('add comment', response)

      if (response.status === 1) {
        this.setState({ responseData: response.list })
      } else {
      }
    })
  }

  showNewField = () => {
    this.setState({ IsShow: true })
  }

  pageLink (page_slug) {
    this.props.history.push('/' + page_slug)
  }

  editUserFetchCall = (role_id, role_name, permissions) => {

    FetchAllApi.editUserRole(
      role_id,
      role_name,
      permissions,
      (err, response) => {
        if (response.status === 1) {
          this.setState({
            isUpdated: response.message
          })
          this.callMe();
          this.jQueryStatusAlert()
        } else {
          this.setState({
            isUpdated: response.message
          })

          this.jQueryStatusAlert()
        }
      }
    )
  }

  getCheckboxVal (e, name, roleid, permissionarray) {
    if (!permissionarray.includes(parseInt(e.target.value))) {
      e.target.dataset.value = e.target.value
      if (permissionarray != undefined) {
        let parsedint = parseInt(e.target.value)
        let IsAvailable = permissionarray.includes(parsedint)
        if (IsAvailable) {
        } else {
          let parsedint = parseInt(e.target.value)
          permissionarray.push(parsedint)
          console.log('afterticking=====', permissionarray)
          let role_id = roleid
          let role_name = name
          let permissions = permissionarray
          this.editUserFetchCall(role_id, role_name, permissions)
          this.callMe()
        }
      }
    } else {
      let permissions = permissionarray
      let parsedint = parseInt(e.target.value)

      function arrayRemove (permissions, parsedint) {
        return permissions.filter(function (ele) {
          return ele != parsedint
        })
      }

      var result = arrayRemove(permissions, parsedint);
      let role_id = roleid;
      let role_name = name;
      permissions = result
      FetchAllApi.editUserRole(
        role_id,
        role_name,
        permissions,

        (err, response) => {
          if (response.status === 1) {
            this.setState({
              isUpdated: response.message
            })

            this.jQueryStatusAlert()
            this.callMe()
          } else {
            this.setState({
              isUpdated: response.message
            })

            this.jQueryStatusAlert()
          }
        }
      )
    }
  }

  logoutLink () {
    localStorage.setItem('logged_user_id', '')
    localStorage.setItem('logged_client_id', '')
    localStorage.setItem('logged_role_id', '')
    localStorage.setItem('logged_user_name', '')
    localStorage.setItem('logged_user_email', '')
    localStorage.setItem('logged_user_phone', '')
    localStorage.setItem('logged_user_image', '')
    localStorage.setItem('logged_company_name', '')

    this.props.history.push('/')
  }

  jQueryStatusAlert = () => {
    jQuery('.resp_msg').fadeIn(2000)
    setTimeout(function () {
      jQuery('.resp_msg').fadeOut(2000)
    }, 8000)
  }

  handleOnClick = (event) => {
    if(this.myDivToFocus.current){
        this.myDivToFocus.current.scrollIntoView({ 
           behavior: "smooth", 
           block: "end"
        })
      }
    
}

  render () {
    console.log('qwerty', this.state.responseData)
    return (
      <div>
        <div className='container-fluid'>
          <div className='row'>
            <LeftSidebar pageSubmit={e => this.pageLink(e)} />
            pageLink
            <div className='menu-close visible-xs'>&nbsp;</div>
            <div className='main-wrap col-md-12 col-xs-12 pad-r-no'>
              <div className='top-bar col-md-12 col-xs-12 pad-r-no'>
                <div className='nav-brand-res visible-xs'>
                  <img
                    className='img-responsive'
                    src='../images/logo-icon.png'
                    alt='LogoIcon'
                  />
                </div>
                <span className='page-title hidden-xs'>Preferences</span>

                <Topbar logoutSubmit={e => this.logoutLink()} />
              </div>

              <div className='main-content col-md-12 col-xs-12'>
                <div className='resp_msg'>{this.state.isUpdated}</div>
                <a href='javascript:;' className='back visible-xs'>
                  <img src='images/back-arrow-blue.svg' />
                </a>
                <span className='page-title visible-xs'>
                  Roles & Permissions
                </span>

                <div className='content-sec col-md-12 col-xs-12 pad-no permission-enclose'>
                  <div className='role-left'>
                    <button
                      className='btn btn-blue btn-rounded add-new'
                      onClick={() =>
                        this.setState({ shown: !this.state.shown })
                      }
                      // onClick={this.addNewRoleAPI}
                    >
                      <img src='images/add-circular-icon.svg' alt='icon' />
                      Add New Role
                    </button>
                    {!this.state.shown ? (
                      <ul className='list-unstyled role-label '>
                        <li>
                          <span
                            contenteditable='true'
                            onInput={e => {
                              let input = e.currentTarget.textContent
                              this.addNewRole(input)
                            }}
                          >
                            New role
                          </span>
                          <div>
                            <span
                              className='pull-right'
                              style={{
                                height: 25,
                                paddingBottom: 4,
                                background: 'white'
                              }}
                            >
                              <button
                                style={{
                                  height: 23,
                                  width: 20,
                                  borderWidth: 1,
                                  paddingBottom: 20
                                }}
                                id='tick'
                                type='btn'
                                className='btn'
                                onClick={this.addNewRoleAPI}
                              >
                                <img
                                  style={{}}
                                  src='images/tick-green.svg'
                                  alt='icon'
                                />
                              </button>
                              <button
                                className='btn'
                                style={{
                                  height: 23,
                                  width: 20,
                                  borderWidth: 1,
                                  paddingBottom: 20
                                }}
                                // onClick={this.testFun.bind(this)}
                              >
                                <img src='images/cross-red.svg' alt='icon' />
                              </button>
                            </span>
                          </div>
                        </li>
                      </ul>
                    ) : (
                      ''
                    )}

                    <ul className='list-unstyled role-label'>
                      {this.state.responseData.map((item, index) => {
                        let name = item.name
                        let roleid = item.id
                        let permissionarray = item.permissions
                        return (
                          <div key={index} >
                            <li>
                              <span
                                contenteditable='true'
                                onInput={e => {
                                  let input = e.currentTarget.textContent
                                  this.addNewRole(input)
                                }}
                              >
                                {item.name}{' '}
                              </span>
                              <div>
                                <span
                                  className='pull-right'
                                  style={{
                                    height: 25,
                                    paddingBottom: 4,
                                    background: 'white'
                                  }}
                                >
                                  <button
                                    style={{
                                      height: 23,
                                      width: 20,
                                      borderWidth: 1,
                                      paddingBottom: 20
                                    }}
                                    id='tick'
                                    type='btn'
                                    className='btn'
                                    // onClick={this.addNewRoleAPI}
                                    onClick={e =>
                                      this.editUserFetchCall( roleid,this.state.newrole,
                                        permissionarray
                                      )
                                    }
                                  >
                                    <img
                                      style={{}}
                                      src='images/tick-green.svg'
                                      alt='icon'
                                    />
                                  </button>
                                  <button
                                    className='btn'
                                    style={{
                                      height: 23,
                                      width: 20,
                                      borderWidth: 1,
                                      paddingBottom: 20
                                    }}
                                    // onClick={this.testFun.bind(this)}
                                  >
                                    <img
                                      src='images/cross-red.svg'
                                      alt='icon'
                                    />
                                  </button>
                                </span>
                              </div>
                            </li>
                          </div>
                        )
                      })}

                      <li />
                    </ul>
                  </div>
          
                  <HorizontalScroller sensibility={300}>
                    <div className='permission-table'>
                      <div className='mscroll-x'>
                        <div className='permission-row head'>
                          <div className='permission-card'>
                            <p className='text-center col-md-12 col-xs-12'>
                              User
                            </p>
                            <span className='col-md-4 col-xs-4 text-center'>
                              View
                            </span>
                            <span className='col-md-4 col-xs-4 text-center'>
                              Edit
                            </span>
                            <span className='col-md-4 col-xs-4 text-center'>
                              Delete
                            </span>
                          </div>
                          <div className='permission-card'>
                            <p className='text-center col-md-12 col-xs-12'>
                              Client
                            </p>
                            <span className='col-md-4 col-xs-4 text-center'>
                              View
                            </span>
                            <span className='col-md-4 col-xs-4 text-center'>
                              Edit
                            </span>
                            <span className='col-md-4 col-xs-4 text-center'>
                              Delete
                            </span>
                          </div>
                          <div className='permission-card'>
                            <p className='text-center col-md-12 col-xs-12'>
                              Inbox
                            </p>
                            <span className='col-md-4 col-xs-4 text-center'>
                              View
                            </span>
                            <span className='col-md-4 col-xs-4 text-center'>
                              Edit
                            </span>
                            <span className='col-md-4 col-xs-4 text-center'>
                              Delete
                            </span>
                          </div>
                          <div className='permission-card'>
                            <p className='text-center col-md-12 col-xs-12'>
                              Report
                            </p>
                            <span className='col-md-4 col-xs-4 text-center'>
                              View
                            </span>
                            <span className='col-md-4 col-xs-4 text-center'>
                              Edit
                            </span>
                            <span className='col-md-4 col-xs-4 text-center'>
                              Delete
                            </span>
                          </div>
                          <div className='permission-card'>
                            <p className='text-center col-md-12 col-xs-12'>
                              Invoice
                            </p>
                            <span className='col-md-4 col-xs-4 text-center'>
                              View
                            </span>
                            <span className='col-md-4 col-xs-4 text-center'>
                              Edit
                            </span>
                            <span className='col-md-4 col-xs-4 text-center'>
                              Delete
                            </span>
                          </div>
                        </div>

                        {this.state.responseData.map((item, index) => {
                          let name = item.name
                          let roleid = item.id
                          let permissionarray = item.permissions
                          console.log('item', item)
                          return (
                            <div className='permission-row' key={index}>
                              <div className='permission-card'>
                                <div className='col-md-4 col-xs-4'>
                                  <div className='check-wrap'>
                                    <label className='custom-checkbox'>
                                      <input
                                        type='checkbox'
                                        id='1'
                                        value='1'
                                        data-value='1'
                                        checked={
                                          permissionarray.includes(1)
                                            ? 'checked'
                                            : ''
                                        }
                                        onChange={e =>
                                          this.getCheckboxVal(
                                            e,
                                            name,
                                            roleid,
                                            permissionarray
                                          )
                                        }
                                      />
                                      <span className='checkmark' />
                                    </label>
                                  </div>
                                </div>
                                <div className='col-md-4 col-xs-4'>
                                  <div className='check-wrap'>
                                    <label className='custom-checkbox'>
                                      <input
                                        type='checkbox'
                                        id='2'
                                        value='2'
                                        data-value='2'
                                        checked={
                                          permissionarray.includes(2)
                                            ? 'checked'
                                            : ''
                                        }
                                        onChange={e =>
                                          this.getCheckboxVal(
                                            e,
                                            name,
                                            roleid,
                                            permissionarray
                                          )
                                        }
                                      />
                                      <span className='checkmark' />
                                    </label>
                                  </div>
                                </div>
                                <div className='col-md-4 col-xs-4'>
                                  <div className='check-wrap'>
                                    <label className='custom-checkbox'>
                                      <input
                                        type='checkbox'
                                        id='3'
                                        value='3'
                                        data-value='3'
                                        checked={
                                          permissionarray.includes(3)
                                            ? 'checked'
                                            : ''
                                        }
                                        onChange={e =>
                                          this.getCheckboxVal(
                                            e,
                                            name,
                                            roleid,
                                            permissionarray
                                          )
                                        }
                                      />
                                      <span className='checkmark' />
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <div className='permission-card'>
                                <div className='col-md-4 col-xs-4'>
                                  <div className='check-wrap'>
                                    <label className='custom-checkbox'>
                                      <input
                                        type='checkbox'
                                        id='4'
                                        value='4'
                                        data-value='4'
                                        checked={
                                          permissionarray.includes(4)
                                            ? 'checked'
                                            : ''
                                        }
                                        onChange={e =>
                                          this.getCheckboxVal(
                                            e,
                                            name,
                                            roleid,
                                            permissionarray
                                          )
                                        }
                                      />
                                      <span className='checkmark' />
                                    </label>
                                  </div>
                                </div>
                                <div className='col-md-4 col-xs-4'>
                                  <div className='check-wrap'>
                                    <label className='custom-checkbox'>
                                      <input
                                        type='checkbox'
                                        id='5'
                                        value='5'
                                        data-value='5'
                                        checked={
                                          permissionarray.includes(5)
                                            ? 'checked'
                                            : ''
                                        }
                                        onChange={e =>
                                          this.getCheckboxVal(
                                            e,
                                            name,
                                            roleid,
                                            permissionarray
                                          )
                                        }
                                      />
                                      <span className='checkmark' />
                                    </label>
                                  </div>
                                </div>
                                <div className='col-md-4 col-xs-4'>
                                  <div className='check-wrap'>
                                    <label className='custom-checkbox'>
                                      <input
                                        type='checkbox'
                                        id='6'
                                        value='6'
                                        data-value='6'
                                        checked={
                                          permissionarray.includes(6)
                                            ? 'checked'
                                            : ''
                                        }
                                        onChange={e =>
                                          this.getCheckboxVal(
                                            e,
                                            name,
                                            roleid,
                                            permissionarray
                                          )
                                        }
                                      />
                                      <span className='checkmark' />
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <div className='permission-card'>
                                <div className='col-md-4 col-xs-4'>
                                  <div className='check-wrap'>
                                    <label className='custom-checkbox'>
                                      <input
                                        type='checkbox'
                                        id='7'
                                        value='7'
                                        data-value='7'
                                        checked={
                                          permissionarray.includes(7)
                                            ? 'checked'
                                            : ''
                                        }
                                        onChange={e =>
                                          this.getCheckboxVal(
                                            e,
                                            name,
                                            roleid,
                                            permissionarray
                                          )
                                        }
                                      />
                                      <span className='checkmark' />
                                    </label>
                                  </div>
                                </div>
                                <div className='col-md-4 col-xs-4'>
                                  <div className='check-wrap'>
                                    <label className='custom-checkbox'>
                                      <input
                                        type='checkbox'
                                        id='8'
                                        value='8'
                                        data-value='8'
                                        checked={
                                          permissionarray.includes(8)
                                            ? 'checked'
                                            : ''
                                        }
                                        onChange={e =>
                                          this.getCheckboxVal(
                                            e,
                                            name,
                                            roleid,
                                            permissionarray
                                          )
                                        }
                                      />
                                      <span className='checkmark' />
                                    </label>
                                  </div>
                                </div>
                                <div className='col-md-4 col-xs-4'>
                                  <div className='check-wrap'>
                                    <label className='custom-checkbox'>
                                      <input
                                        type='checkbox'
                                        id='9'
                                        value='9'
                                        data-value='9'
                                        checked={
                                          permissionarray.includes(9)
                                            ? 'checked'
                                            : ''
                                        }
                                        onChange={e =>
                                          this.getCheckboxVal(
                                            e,
                                            name,
                                            roleid,
                                            permissionarray
                                          )
                                        }
                                      />
                                      <span className='checkmark' />
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <div className='permission-card'>
                                <div className='col-md-4 col-xs-4'>
                                  <div className='check-wrap'>
                                    <label className='custom-checkbox'>
                                      <input
                                        type='checkbox'
                                        id='10'
                                        value='10'
                                        data-value='10'
                                        checked={
                                          permissionarray.includes(10)
                                            ? 'checked'
                                            : ''
                                        }
                                        onChange={e =>
                                          this.getCheckboxVal(
                                            e,
                                            name,
                                            roleid,
                                            permissionarray
                                          )
                                        }
                                      />
                                      <span className='checkmark' />
                                    </label>
                                  </div>
                                </div>
                                <div className='col-md-4 col-xs-4'>
                                  <div className='check-wrap'>
                                    <label className='custom-checkbox'>
                                      <input
                                        type='checkbox'
                                        id='11'
                                        value='11'
                                        data-value='11'
                                        checked={
                                          permissionarray.includes(11)
                                            ? 'checked'
                                            : ''
                                        }
                                        onChange={e =>
                                          this.getCheckboxVal(
                                            e,
                                            name,
                                            roleid,
                                            permissionarray
                                          )
                                        }
                                      />
                                      <span className='checkmark' />
                                    </label>
                                  </div>
                                </div>
                                <div className='col-md-4 col-xs-4'>
                                  <div className='check-wrap'>
                                    <label className='custom-checkbox'>
                                      <input
                                        type='checkbox'
                                        id='12'
                                        value='12'
                                        data-value='12'
                                        checked={
                                          permissionarray.includes(12)
                                            ? 'checked'
                                            : ''
                                        }
                                        onChange={e =>
                                          this.getCheckboxVal(
                                            e,
                                            name,
                                            roleid,
                                            permissionarray
                                          )
                                        }
                                      />
                                      <span className='checkmark' />
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <div className='permission-card'>
                                <div className='col-md-4 col-xs-4'>
                                  <div className='check-wrap'>
                                    <label className='custom-checkbox'>
                                      <input
                                        type='checkbox'
                                        id='13'
                                        value='13'
                                        data-value='13'
                                        checked={
                                          permissionarray.includes(13)
                                            ? 'checked'
                                            : ''
                                        }
                                        onChange={e =>
                                          this.getCheckboxVal(
                                            e,
                                            name,
                                            roleid,
                                            permissionarray
                                          )
                                        }
                                      />
                                      <span className='checkmark' />
                                    </label>
                                  </div>
                                </div>
                                <div className='col-md-4 col-xs-4'>
                                  <div className='check-wrap'>
                                    <label className='custom-checkbox'>
                                      <input
                                        type='checkbox'
                                        id='14'
                                        value='14'
                                        data-value='14'
                                        checked={
                                          permissionarray.includes(14)
                                            ? 'checked'
                                            : ''
                                        }
                                        onChange={e =>
                                          this.getCheckboxVal(
                                            e,
                                            name,
                                            roleid,
                                            permissionarray
                                          )
                                        }
                                      />
                                      <span className='checkmark' />
                                    </label>
                                  </div>
                                </div>
                                <div className='col-md-4 col-xs-4'>
                                  <div className='check-wrap'>
                                    <label className='custom-checkbox'>
                                      <input
                                        type='checkbox'
                                        id='15'
                                        value='15'
                                        data-value='15'
                                        checked={
                                          permissionarray.includes(15)
                                            ? 'checked'
                                            : ''
                                        }
                                        onChange={e =>
                                          this.getCheckboxVal(
                                            e,
                                            name,
                                            roleid,
                                            permissionarray
                                          )
                                        }
                                      />
                                      <span className='checkmark' />
                                    </label>
                                  </div>
                                </div>
                              </div>
                            </div>
                          )
                        })}
                      </div>
                    </div>
                  </HorizontalScroller>
                </div>
              </div>
            </div>
          </div>
        </div>

        <Footer logoutSubmit={e => this.logoutLink(e)}/>
        <div  ref={this.myDivToFocus} ></div>
      </div>
    )
  }
}