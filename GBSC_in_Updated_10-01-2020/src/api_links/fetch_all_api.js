import React from 'react'
import config from './api_links.js'

var authorization_key = 'O5mGIP3VNia0JvPH2IBiwA=='

class FetchAllApi extends React.Component {
  static userLogin (email_id, password, callback) {
    fetch(config.login_link, {
      method: 'POST',
      body: JSON.stringify({
        email_id: email_id,
        password: password
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static forgotPwd (email_id, callback) {
    fetch(config.forgot_password_link, {
      method: 'POST',
      body: JSON.stringify({
        email_id: email_id
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static inboxList (page, limit, status, client_id, callback) {
 

    fetch(config.inbox_list_link, {
      method: 'POST',
      body: JSON.stringify({
        page: page,
        limit: limit,
        status: status,
        // client_id: client_id
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        console.log("objehjhjhjct",data)
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
        
      })
  }
  
  
  static getItemDetails (list_id, status, client_id, callback) {
    fetch(config.get_item_details_link, {
      method: 'POST',
      body: JSON.stringify({
        list_id: list_id,
        status: status,
        client_id: client_id
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static getFilePath (file_id, callback) {
    fetch(config.get_file_path_link, {
      method: 'POST',
      body: JSON.stringify({
        file_id: file_id
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static addComment (
    comment_text,
    user_id,
    list_id,
    file_id,
    attachments,
    parent_comment_id,
    callback
  ) {
    console.log('attachments_before', attachments)
    let formData = new FormData()
    formData.append('comment_text', comment_text)
    formData.append('user_id', user_id)
    formData.append('list_id', list_id)
    formData.append('file_id', file_id)
    formData.append('parent_comment_id', parent_comment_id)
    if (attachments.length > 0) {
      for (var i = 0; i < attachments.length; i++) {
        formData.append('attachments', attachments[i])
      }
    } else {
      formData.append('attachments', '')
    }

    console.log('attachments', attachments)

    fetch(config.add_comment_link, {
      method: 'POST',
      body: formData,
      headers: {
        //"Content-type": "application/json; charset=UTF-8",
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static saveNewDocument (
    client_id,
    user_id,
    title,
    description,
    attachments,
    callback
  ) {
    let formData = new FormData()
    formData.append('client_id', client_id)
    formData.append('user_id', user_id)
    formData.append('title', title)
    formData.append('description', description)
    for (var i = 0; i < attachments.length; i++) {
      formData.append('attachments', attachments[i])
    }

    console.log('attachments', attachments)

    fetch(config.save_new_document, {
      method: 'POST',
      body: formData,
      headers: {
        //"Content-type": "multipart/form-data; charset=UTF-8",
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static sentItems (client_id, page, limit, callback) {
    fetch(config.sent_items, {
      method: 'POST',
      body: JSON.stringify({
        client_id: client_id,
        page: page,
        limit: limit
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static registerNewCompany (coreData, callback) {
    fetch(config.register_new_company, {
      method: 'POST',
      body: JSON.stringify({
        entity_name: coreData.entity_name,
        address: coreData.address,
        state: coreData.state,
        country: coreData.country,
        email: coreData.email,
        phone: coreData.phone,
        home_currency: coreData.home_currency,
        country_code: coreData.country_code,
        entity_number: coreData.entity_number,
        incorporation_date: coreData.incorporation_date,
        principle_activities: coreData.principle_activities,
        entity_type: coreData.entity_type,
        first_name: coreData.first_name,
        last_name: coreData.last_name,
        company_email: coreData.company_email,
        company_phone: coreData.company_phone,
        password: coreData.password,
        plan_id: coreData.plan_id,
        subscription_start_date: coreData.subscription_start_date,
        subscription_end_date: coreData.subscription_end_date
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static get_entity_types (callback) {
    fetch(config.get_entity_types, {
      method: 'GET',
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static defaultcategorylist (callback) {
    fetch(config.default_category_list, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static get_edit_invoice_html (client_id,callback) {
    fetch(config.invoice_template_list, {
      method: 'POST',
      body: JSON.stringify({
        client_id:client_id
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }
  static defaultcategorylist_onchange (search_key, callback) {
    fetch(config.default_category_list, {
      method: 'POST',
      body: JSON.stringify({
        search_key: search_key
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        console.log('jhsgdjsadhg', data.status)
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static get_gst_list (search_key, keyword, callback) {
    fetch(config.get_gst_list, {
      method: 'POST',
      body: JSON.stringify({
        country_id: search_key,
        keyword: keyword
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        console.log('jhsgdjsadhg', data.status)
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static balancesheetlist (callback) {
    fetch(config.balance_sheet_category_list, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static balancesheetlist_onchange (search_key, callback) {
    fetch(config.balance_sheet_category_list, {
      method: 'POST',
      body: JSON.stringify({
        search_key: search_key
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static get_categories (callback) {
    fetch(config.category_list, {
      method: 'POST',
      body: '',
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static get_SubCategory (category_id, callback) {
    fetch(config.sub_category_list, {
      method: 'POST',
      body: JSON.stringify({
        category_id: category_id
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static get_Accounttype (sub_category_id, callback) {
    fetch(config.account_type_list, {
      method: 'POST',
      body: JSON.stringify({
        sub_category_id: sub_category_id
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static get_sub_Accounttype (sub_Account_id, callback) {
    fetch(config.sub_account_list, {
      method: 'POST',
      body: JSON.stringify({
        account_type_id: sub_Account_id
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static save_NewAccountName (coreData, callback) {
    fetch(config.add_new_account_name, {
      method: 'POST',
      body: JSON.stringify({
        client_id: 1,
        status: 2,
        account_name: coreData.account_name,
        account_type_id: coreData.account_type_id,
        sub_category_id: coreData.sub_category_id,
        sub_account_id: coreData.sub_account_id,
        currency: coreData.currency
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }
  static get_countries (callback) {
    fetch(config.get_countries, {
      method: 'GET',
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static get_states (country_id, callback) {
    fetch(config.get_states, {
      method: 'POST',
      body: JSON.stringify({
        country_id: country_id
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static add_new_entity (create_entity, callback) {
    fetch(config.add_new_entity, {
      method: 'POST',
      body: JSON.stringify({
        entity_name: create_entity
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static addUserRole (role_name, permissions, callback) {
    fetch(config.add_new_user_role, {
      method: 'POST',
      body: JSON.stringify({
        role_name: role_name,
        permissions: permissions
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  // Get user role details

  static getUserRoles (callback) {
    fetch(config.get_user_role_list, {
      method: 'GET',

      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static editUserRole (role_id, role_name, permissions, callback) {
    fetch(config.edit_user_role, {
      method: 'POST',
      body: JSON.stringify({
        role_id: role_id,
        role_name: role_name,
        permissions: permissions
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static getFileCmnts (file_id, callback) {
    fetch(config.get_file_comments, {
      method: 'POST',
      body: JSON.stringify({
        file_id: file_id
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static updateComment (comment_id, comment_text, user_id, callback) {
    fetch(config.update_comment, {
      method: 'POST',
      body: JSON.stringify({
        comment_id: comment_id,
        comment_text: comment_text,
        user_id: user_id
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static deleteComment (comment_id, list_id, callback) {
    fetch(config.delete_comment, {
      method: 'POST',
      body: JSON.stringify({
        comment_id: comment_id,
        list_id: list_id
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static saveAndContinue (coreData, callback) {
    fetch(config.save_tagged_item, {
      method: 'POST',
      body: JSON.stringify({
        client_id: coreData.client_id,
        item_total_home_currency: coreData.item_total_home_currency,
        tax_amount_home_currency: coreData.tax_amount_home_currency,
        grand_total_home_currency: coreData.grand_total_home_currency,
        item_total_foreign_currency: coreData.item_total_foreign_currency,
        tax_amount_foreign_currency: coreData.tax_amount_foreign_currency,
        grand_total_foreign_currency: coreData.grand_total_foreign_currency,
        currency: coreData.currency,
        exchange_rate: coreData.exchange_rate,
        type: coreData.type,
        list_id: coreData.list_id,
        tagged_user_id: coreData.tagged_user_id,
        company_name: coreData.company_name,
        invoice_number: coreData.invoice_number,
        company_address: coreData.company_address,
        invoice_date: coreData.invoice_date,
        account_category: coreData.account_category,
        item_list: coreData.item_list
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static save_tagged_item_draft (coreData, callback) {
    fetch(config.save_tagged_item_draft, {
      method: 'POST',
      body: JSON.stringify({
        client_id: coreData.client_id,
        item_total_home_currency: coreData.item_total_home_currency,
        tax_amount_home_currency: coreData.tax_amount_home_currency,
        grand_total_home_currency: coreData.grand_total_home_currency,
        item_total_foreign_currency: coreData.item_total_foreign_currency,
        tax_amount_foreign_currency: coreData.tax_amount_foreign_currency,
        grand_total_foreign_currency: coreData.grand_total_foreign_currency,
        currency: coreData.currency,
        exchange_rate: coreData.exchange_rate,
        type: coreData.type,
        list_id: coreData.list_id,
        tagged_user_id: coreData.tagged_user_id,
        company_name: coreData.company_name,
        invoice_number: coreData.invoice_number,
        company_address: coreData.company_address,
        invoice_date: coreData.invoice_date,
        account_category: coreData.account_category,
        item_list: coreData.item_list
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static add_gst_details (coreData, callback) {
    fetch(config.add_gst_details, {
      method: 'POST',
      body: JSON.stringify({
        sales_tax_code: coreData.sales_tax_code,
        sales_tax_name: coreData.sales_tax_name,
        show_on_list: coreData.show_on_list,
        tax_type: coreData.tax_type,
        rate: coreData.rate,
        rate_type: coreData.rate_type,
        country: coreData.country,
        // country_code: coreData.country_code

      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }

  static getSubCmmnts (comment_id, callback) {
    fetch(config.get_sub_comments, {
      method: 'POST',
      body: JSON.stringify({
        comment_id: comment_id
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }


  
  static get_country_id (client_id, callback) {
    fetch(config.get_client_country, {
      method: 'POST',
      body: JSON.stringify({
        client_id: client_id
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Authorization: authorization_key
      }
    })
      .then(response => {
        return response.json()
      })
      .then(data => {
        if (data.status === 1) {
          callback(null, data)
        } else {
          callback(null, data)
        }
      })
  }
}

export default FetchAllApi
