import React, { Component } from 'react'
import FetchAllApi from './api_links/fetch_all_api.js'
import parse from 'html-react-parser'
import jQuery from 'jquery'

export class custom_editable_template extends Component {
  constructor (props) {
    super(props)
    this.state = {  
      logged_user_id: localStorage.getItem('logged_user_id'),
      logged_client_id: localStorage.getItem('logged_client_id'),
      logged_role_id: localStorage.getItem('logged_role_id'),
      logged_user_name: localStorage.getItem('logged_user_name'),
      logged_user_email: localStorage.getItem('logged_user_email'),
      logged_user_phone: localStorage.getItem('logged_user_phone'),
      logged_user_image: localStorage.getItem('logged_user_image'),
      logged_company_name: localStorage.getItem('logged_company_name'),
      html_content: ''
    }
  }
  UNSAFE_componentWillMount () {
    jQuery(document.body).removeClass('minimize_leftbar');
    //console.log("logged_user_id", this.state.logged_user_id);

    jQuery('title').html('Template Editor | GBSC')

    if (
      this.state.logged_user_id === '' ||
      this.state.logged_user_id === 'null' ||
      this.state.logged_user_id === 'undefined'
    ) {
      this.props.history.push('/')
    }
  }
  increase_logo_size = (range) => {
    jQuery('#logo_img').width(range+"%");
  }

  componentDidMount () {
    require('jquery-mousewheel')
    require('malihu-custom-scrollbar-plugin')

    jQuery('.item-listwrap').mCustomScrollbar({
      scrollEasing: 'linear',
      scrollInertia: 600,
      scrollbarPosition: 'outside'
    })
    var client_id = 1;
    FetchAllApi.get_edit_invoice_html(client_id, (err, response) => {
      console.log('defaultcategorylist', response)
      //alert(response.message)
      console.log('html content', response.list[0].html_content)
      if (response.status === 1) {
        this.setState({
          html_content: response.list[0].html_content
        })
      } else {
        this.setState({})
      }
    })
  }
  setlogo(logo_value){
    var checkBox = document.getElementById("logo_onoff").checked;
    if(checkBox)
    jQuery("#logo_parnt_container").css("display","block");
    else
    jQuery("#logo_parnt_container").css("display","none");
   // this.setState({isLogoparent:!this.state.isLogoparent})
  }
  stripeBackground(result,value){
    if(result === "addstrip"){
    var checkBox = document.getElementById("stripe_background").checked;
    if(checkBox)
    jQuery("#stripe").css("display","block");
    else
    jQuery("#stripe").css("display","none");
    }
    else if(result === "changecolor"){
      jQuery("#stripe").css("background",value);
    }
    else if(result === "textsize")
    {
      jQuery("#invoice_stripe").css("font-size",value+"pt");
    }
    else if(result === "changecolor_text"){
      jQuery("#invoice_stripe").css("color",value);
    }
  }
  
  item_header(value,result){
    if(result === "textsize"){
      jQuery("#header_content").css("font-size",value+"pt");
    }else if(result === "textcolor"){
      jQuery("#header_content").css("color",value);
    }else if(result === "invoiceto_checkbox"){
      if(document.getElementById(result).checked)
      jQuery("#header_txt_container_invoice").css("display","block");
      else
      jQuery("#header_txt_container_invoice").css("display","none");
    }else if(result === "invoicetovalue"){
      jQuery("#invoice_to_content").html(value);
    }else if(result === "billto_checkbox"){
      if(document.getElementById(result).checked)
      jQuery("#header_txt_container_billto").css("display","block");
      else
      jQuery("#header_txt_container_billto").css("display","none");
    }else if(result === "billtovalue"){
      jQuery("#billed_to_content").html(value);
    }else if(result === "invoicenumber_checkbox"){
      if(document.getElementById(result).checked)
      jQuery("#invoice_no_content").css("display","block");
      else
      jQuery("#invoice_no_content").css("display","none");
      
    }else if(result === "invoicenumbervalue"){
      jQuery("#invoice_value").html(value);
    }else if(result === "date_value_checkbox"){
      if(document.getElementById(result).checked)
      jQuery("#date_content").css("display","block");
      else
      jQuery("#date_content").css("display","none");
    }else if(result === "date_value_text"){
      jQuery("#date_value").html(value);
    }
  }
  entity_update(value,id){
    var result=document.getElementById(id).checked;
    if(result)
    jQuery("#"+value).css("display","block");
    else
    jQuery("#"+value).css("display","none");
  }
  footer_functions(value,result){
    if(result === "termsandcond"){
      jQuery("#terms_cond_msg_tag").html(value);
    }else if(result === "terms"){
      jQuery("#terms_condLabel_tag").html(value);
    }else if(result === "terms_checkbox"){
      if(document.getElementById(result).checked)
      jQuery("#terms_cond_container").css("display","block");
      else
      jQuery("#terms_cond_container").css("display","none");
    }else if(result === "textSize"){
      jQuery("#footer").css("font-size",value+"pt");
    }else if(result === "textcolor"){
      jQuery("#footer").css("color",value);
    }else if(result === "thankmessage_checkbox"){
      if(document.getElementById(result).checked)
      jQuery("#thank_u_msg_tag").css("display","block");
      else
      jQuery("#thank_u_msg_tag").css("display","none");
    }else if(result === "thankmessage"){
      jQuery("#thank_u_msg_tag").html(value);
    }
  }
  render () {
    return (
      <div>
        {/* Main Wrapper Starts here */}
        <div className='container-fluid invoice-editor'>
          <div className='row'>
            {/* Builder Left Starts here */}
            <div className='builder-left'>
              <div
                className='mscroll-y mCustomScrollbar _mCS_1 mCS-autoHide'
                style={{ position: 'relative', overflow: 'visible' }}
              >
                <div
                  id='mCSB_1'
                  className='mCustomScrollBox mCS-light mCSB_vertical mCSB_outside'
                  style={{ maxHeight: 'none' }}
                  tabIndex={0}
                >
                  <div
                    id='mCSB_1_container'
                    className='mCSB_container'
                    style={{ position: 'relative', top: 0, left: 0 }}
                    dir='ltr'
                  >
                    <h5>Template Properties</h5>
                    <ul className='nav nav-pills'>
                      <li className='active'>
                        <a data-toggle='pill' href='#tab-header'>
                          Header
                        </a>
                      </li>
                      <li>
                        <a data-toggle='pill' href='#tab-item'>
                          Item
                        </a>
                      </li>
                      <li>
                        <a data-toggle='pill' href='#tab-footer'>
                          Footer
                        </a>
                      </li>
                    </ul>
                    <div className='tab-content'>
                      <div id='tab-header' className='tab-pane fade in active'>
                        <form className='custom-form'>
                          <div className='form-group clearfix'>
                            <span className='editor-label'>
                              <label className='switch'>
                                <input
                                  type='checkbox'
                                  id="logo_onoff"
                                  defaultChecked='checked'
                                  onChange={(e)=>this.setlogo(e.target.value)}
                                />
                                <span className='drag-ball'>
                                  <span className='off' />
                                  <span className='on' />
                                </span>
                              </label>
                              <span className='form-label'>
                                Entity Logo
                                <i
                                  data-toggle='tooltip'
                                  data-placement='right'
                                  tabIndex={0}
                                  title
                                  data-original-title='You can change the logo from Organization settings'
                                >
                                  <img
                                    src='images/round-info-icon.svg'
                                    alt='icon'
                                    className='mCS_img_loaded'
                                  />
                                </i>
                              </span>
                            </span>
                            <div className='logo-wrap'>
                              <img
                                className='img-responsive mCS_img_loaded'
                                src='images/form-sample-logo.png'
                                alt='icon'
                              />
                            </div>
                            <div className='range-encl'>
                              <span className='form-label'>Logo Size</span>
                              <input type='range' name='img-size' onChange={(e)=>this.increase_logo_size(e.target.value)} />
                            </div>
                          </div>
                          <div className='form-group clearfix'>
                            <span className='form-label fw-sbold w-100 mar-btm'>
                              Header Options
                            </span>
                            <div className='clearfix'>
                              <span className='editor-label'>
                                <label className='switch'>
                                  <input
                                    type='checkbox'
                                    defaultChecked='checked'
                                    onChange={(e)=>this.stripeBackground("addstrip",1)}
                                    id="stripe_background"
                                  />
                                  <span className='drag-ball'>
                                    <span className='off' />
                                    <span className='on' />
                                  </span>
                                </label>
                                <span className='form-label'>
                                  Stripe Background
                                </span>
                                <div className='input-group'>
                                  <input
                                    type='text'
                                    className='form-control'
                                    name='text-color'
                                    onChange={(e)=>this.stripeBackground("changecolor",e.target.value)}
                                  />
                                  <span className='input-group-addon'>
                                    <img
                                      src='images/color-wheel-icon.svg'
                                      alt='icon'
                                      className='mCS_img_loaded'
                                    />
                                  </span>
                                </div>
                              </span>
                            </div>
                            <div className='clearfix mar-btm'>
                              <span className='form-label'>Text Size</span>
                              <div className='input-group'>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='text-size'
                                  onChange={(e)=>this.stripeBackground("textsize",e.target.value)}
                                />
                                <span className='input-group-addon fw-500'>
                                  pt
                                </span>
                              </div>
                            </div>
                            <div className='clearfix'>
                              <span className='form-label'>Text Color</span>
                              <div className='input-group'>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='text-color'
                                  onChange={ (e)=>this.stripeBackground("changecolor_text",e.target.value)}
                                />
                                <span className='input-group-addon'>
                                  <img
                                    src='images/color-wheel-icon.svg'
                                    alt='icon'
                                    className='mCS_img_loaded'
                                  />
                                </span>
                              </div>
                            </div>
                          </div>
                          <div className='form-group clearfix'>
                            <span className='editor-label'>
                              <label className='switch'>
                                <input type='checkbox' id="entity_name_check" onChange={(e)=>this.entity_update("entity_name","entity_name_check")} />
                                <span className='drag-ball'>
                                  <span className='off' />
                                  <span className='on' />
                                </span>
                              </label>
                              <span className='form-label'>Entity Name</span>
                            </span>
                          </div>
                          <div className='form-group clearfix'>
                            <span className='editor-label'>
                              <label className='switch'>
                                <input
                                  type='checkbox'
                                  defaultChecked='checked'
                                  id="address_check"
                                  onChange={(e)=>this.entity_update("address","address_check")}
                                />
                                <span className='drag-ball'>
                                  <span className='off' />
                                  <span className='on' />
                                </span>
                              </label>
                              <span className='form-label'>Entity Address</span>
                            </span>
                          </div>
                          <div className='form-group clearfix'>
                            <span className='editor-label'>
                              <label className='switch'>
                                <input
                                  type='checkbox'
                                  defaultChecked='checked'
                                  id="mobile_number_check"
                                  onChange={(e)=>this.entity_update("mobile_number","mobile_number_check")}
                                />
                                <span className='drag-ball'>
                                  <span className='off' />
                                  <span className='on' />
                                </span>
                              </label>
                              <span className='form-label'>
                                Entity Contact No.
                              </span>
                            </span>
                          </div>
                          <div className='form-group clearfix mar-b-no'>
                            <span className='editor-label'>
                              <label className='switch'>
                                <input
                                  type='checkbox'
                                  defaultChecked='checked'
                                  id="email_value_check"
                                  onChange={(e)=>this.entity_update("email_value","email_value_check")}
                                />
                                <span className='drag-ball'>
                                  <span className='off' />
                                  <span className='on' />
                                </span>
                              </label>
                              <span className='form-label'>Entity Email</span>
                            </span>
                          </div>
                          <small>
                            <i>
                              Change header item content from organization
                              profile
                            </i>
                          </small>
                        </form>
                      </div>
                      <div id='tab-item' className='tab-pane fade'>
                        <form className='custom-form'>
                          <div className='form-group clearfix'>
                            <span className='form-label fw-sbold w-100 mar-btm'>
                              Items Options
                            </span>
                            <div className='clearfix mar-btm'>
                              <span className='form-label'>Text Size</span>
                              <div className='input-group'>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='text-size'
                                  onChange={(e)=>this.item_header(e.target.value,"textsize")}
                                />
                                <span className='input-group-addon fw-500'>
                                  pt
                                </span>
                              </div>
                            </div>
                            <div className='clearfix'>
                              <span className='form-label'>Text Color</span>
                              <div className='input-group'>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='text-color'
                                  onChange={(e)=>this.item_header(e.target.value,"textcolor")}
                                />
                                <span className='input-group-addon'>
                                  <img
                                    src='images/color-wheel-icon.svg'
                                    alt='icon'
                                    className='mCS_img_loaded'
                                  />
                                </span>
                              </div>
                            </div>
                          </div>
                          <div className='form-group clearfix'>
                            <span className='editor-label'>
                              <label className='switch'>
                                <input
                                  type='checkbox'
                                  defaultChecked='checked'
                                  id="invoiceto_checkbox"
                                  onChange={(e)=>this.item_header(e.target.value,"invoiceto_checkbox")}
                                />
                                <span className='drag-ball'>
                                  <span className='off' />
                                  <span className='on' />
                                </span>
                              </label>
                              <span className='form-label'>Invoice to</span>
                            </span>
                            <div className='input-group'>
                              <span className='input-group-addon addon-left fw-500'>
                                Label
                              </span>
                              <input
                                type='text'
                                className='form-control'
                                name='label'
                                defaultValue='Invoice to'
                                onChange={(e)=>this.item_header(e.target.value,"invoicetovalue")}
                              />
                            </div>
                          </div>
                          <div className='form-group clearfix'>
                            <span className='editor-label'>
                              <label className='switch'>
                                <input type='checkbox' id="billto_checkbox" onChange={(e)=>this.item_header(e.target.value,"billto_checkbox")}/>
                                <span className='drag-ball'>
                                  <span className='off' />
                                  <span className='on' />
                                </span>
                              </label>
                              <span className='form-label'>Bill to</span>
                            </span>
                            <div className='input-group'>
                              <span className='input-group-addon addon-left fw-500'>
                                Label
                              </span>
                              <input
                                type='text'
                                className='form-control'
                                name='label'
                                defaultValue='Bill to'
                                onChange={(e)=>this.item_header(e.target.value,"billtovalue")}
                              />
                            </div>
                          </div>
                          <div className='form-group clearfix'>
                            <span className='editor-label'>
                              <label className='switch'>
                                <input
                                  type='checkbox'
                                  defaultChecked='checked'
                                  id="invoicenumber_checkbox"
                                  onChange={(e)=>this.item_header(e.target.value,"invoicenumber_checkbox")}
                                />
                                <span className='drag-ball'>
                                  <span className='off' />
                                  <span className='on' />
                                </span>
                              </label>
                              <span className='form-label'>Invoice No</span>
                            </span>
                            <div className='input-group'>
                              <span className='input-group-addon addon-left fw-500'>
                                Label
                              </span>
                              <input
                                type='text'
                                className='form-control'
                                name='label'
                                defaultValue='Invoice no'
                                onChange={(e)=>this.item_header(e.target.value,"invoicenumbervalue")}
                              />
                            </div>
                          </div>
                          <div className='form-group clearfix'>
                            <span className='editor-label'>
                              <label className='switch'>
                                <input
                                  type='checkbox'
                                  defaultChecked='checked'
                                  id="date_value_checkbox"
                                  onChange={(e)=>this.item_header(e.target.value,"date_value_checkbox")}
                                />
                                <span className='drag-ball'>
                                  <span className='off' />
                                  <span className='on' />
                                </span>
                              </label>
                              <span className='form-label'>Date</span>
                            </span>
                            <div className='input-group'>
                              <span className='input-group-addon addon-left fw-500'>
                                Label
                              </span>
                              <input
                                type='text'
                                className='form-control'
                                name='label'
                                defaultValue='Date'
                                onChange={(e)=>this.item_header(e.target.value,"date_value_text")}
                              />
                            </div>
                          </div>
                          <div className='form-group clearfix'>
                            <span className='form-label fw-sbold w-100 mar-btm'>
                              Table Properties
                            </span>
                            <div className='clearfix mar-btm'>
                              <span className='form-label'>Text Size</span>
                              <div className='input-group'>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='text-size'
                                />
                                <span className='input-group-addon fw-500'>
                                  pt
                                </span>
                              </div>
                            </div>
                            <div className='clearfix mar-btm'>
                              <span className='form-label'>Text Color</span>
                              <div className='input-group'>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='text-color'
                                />
                                <span className='input-group-addon'>
                                  <img
                                    src='images/color-wheel-icon.svg'
                                    alt='icon'
                                    className='mCS_img_loaded'
                                  />
                                </span>
                              </div>
                            </div>
                            <div className='clearfix mar-btm'>
                              <span className='form-label'>
                                Table background
                              </span>
                              <div className='input-group'>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='text-color'
                                />
                                <span className='input-group-addon'>
                                  <img
                                    src='images/color-wheel-icon.svg'
                                    alt='icon'
                                    className='mCS_img_loaded'
                                  />
                                </span>
                              </div>
                            </div>
                            <div className='clearfix mar-btm'>
                              <span className='form-label'>
                                Table head background
                              </span>
                              <div className='input-group'>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='text-color'
                                />
                                <span className='input-group-addon'>
                                  <img
                                    src='images/color-wheel-icon.svg'
                                    alt='icon'
                                    className='mCS_img_loaded'
                                  />
                                </span>
                              </div>
                            </div>
                            <div className='clearfix mar-btm'>
                              <span className='form-label'>
                                Table row background
                              </span>
                              <div className='input-group'>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='text-color'
                                />
                                <span className='input-group-addon'>
                                  <img
                                    src='images/color-wheel-icon.svg'
                                    alt='icon'
                                    className='mCS_img_loaded'
                                  />
                                </span>
                              </div>
                            </div>
                            <div className='clearfix mar-btm'>
                              <span className='form-label'>
                                Table border color
                              </span>
                              <div className='input-group'>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='text-color'
                                />
                                <span className='input-group-addon'>
                                  <img
                                    src='images/color-wheel-icon.svg'
                                    alt='icon'
                                    className='mCS_img_loaded'
                                  />
                                </span>
                              </div>
                            </div>
                            <span className='form-label fw-sbold w-100 mar-btm'>
                              Table Label
                            </span>
                            <button className='btn btn-blue btn-img mar-btm'>
                              <img
                                src='images/add-circular-icon.svg'
                                alt='icon'
                                className='mCS_img_loaded'
                              />
                              Add Label
                            </button>
                            <div className='clearfix mar-btm'>
                              <span className='editor-label'>
                                <label className='switch'>
                                  <input
                                    type='checkbox'
                                    defaultChecked='checked'
                                  />
                                  <span className='drag-ball'>
                                    <span className='off' />
                                    <span className='on' />
                                  </span>
                                </label>
                                <span className='form-label'>No</span>
                              </span>
                              <div className='input-group'>
                                <span className='input-group-addon addon-left fw-500'>
                                  Label
                                </span>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='label'
                                  defaultValue='No'
                                />
                              </div>
                            </div>
                            <div className='clearfix mar-btm'>
                              <span className='editor-label'>
                                <label className='switch'>
                                  <input
                                    type='checkbox'
                                    defaultChecked='checked'
                                  />
                                  <span className='drag-ball'>
                                    <span className='off' />
                                    <span className='on' />
                                  </span>
                                </label>
                                <span className='form-label'>Item</span>
                              </span>
                              <div className='input-group'>
                                <span className='input-group-addon addon-left fw-500'>
                                  Label
                                </span>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='label'
                                  defaultValue='Item & Description'
                                />
                              </div>
                            </div>
                            <div className='clearfix mar-btm'>
                              <span className='editor-label'>
                                <label className='switch'>
                                  <input
                                    type='checkbox'
                                    defaultChecked='checked'
                                  />
                                  <span className='drag-ball'>
                                    <span className='off' />
                                    <span className='on' />
                                  </span>
                                </label>
                                <span className='form-label'>Price Each</span>
                              </span>
                              <div className='input-group'>
                                <span className='input-group-addon addon-left fw-500'>
                                  Label
                                </span>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='label'
                                  defaultValue='Price Each'
                                />
                              </div>
                            </div>
                            <div className='clearfix mar-btm'>
                              <span className='editor-label'>
                                <label className='switch'>
                                  <input
                                    type='checkbox'
                                    defaultChecked='checked'
                                  />
                                  <span className='drag-ball'>
                                    <span className='off' />
                                    <span className='on' />
                                  </span>
                                </label>
                                <span className='form-label'>Quantity</span>
                              </span>
                              <div className='input-group'>
                                <span className='input-group-addon addon-left fw-500'>
                                  Label
                                </span>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='label'
                                  defaultValue='Qty'
                                />
                              </div>
                            </div>
                            <div className='clearfix mar-btm'>
                              <span className='editor-label'>
                                <label className='switch'>
                                  <input
                                    type='checkbox'
                                    defaultChecked='checked'
                                  />
                                  <span className='drag-ball'>
                                    <span className='off' />
                                    <span className='on' />
                                  </span>
                                </label>
                                <span className='form-label'>Tax</span>
                              </span>
                              <div className='input-group'>
                                <span className='input-group-addon addon-left fw-500'>
                                  Label
                                </span>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='label'
                                  defaultValue='Tax'
                                />
                              </div>
                            </div>
                            <div className='clearfix'>
                              <span className='editor-label'>
                                <label className='switch'>
                                  <input
                                    type='checkbox'
                                    defaultChecked='checked'
                                  />
                                  <span className='drag-ball'>
                                    <span className='off' />
                                    <span className='on' />
                                  </span>
                                </label>
                                <span className='form-label'>Total</span>
                              </span>
                              <div className='input-group'>
                                <span className='input-group-addon addon-left fw-500'>
                                  Label
                                </span>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='label'
                                  defaultValue='Total'
                                />
                              </div>
                            </div>
                          </div>
                          <div className='form-group clearfix'>
                            <span className='form-label fw-sbold w-100 mar-btm'>
                              Total Properties
                            </span>
                            <div className='clearfix mar-btm'>
                              <span className='editor-label'>
                                <label className='switch'>
                                  <input
                                    type='checkbox'
                                    defaultChecked='checked'
                                  />
                                  <span className='drag-ball'>
                                    <span className='off' />
                                    <span className='on' />
                                  </span>
                                </label>
                                <span className='form-label'>Sub Total</span>
                              </span>
                              <div className='input-group'>
                                <span className='input-group-addon addon-left fw-500'>
                                  Label
                                </span>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='label'
                                  defaultValue='Sub Total'
                                />
                              </div>
                            </div>
                            <div className='clearfix mar-btm'>
                              <span className='editor-label'>
                                <label className='switch'>
                                  <input
                                    type='checkbox'
                                    defaultChecked='checked'
                                  />
                                  <span className='drag-ball'>
                                    <span className='off' />
                                    <span className='on' />
                                  </span>
                                </label>
                                <span className='form-label'>Tax</span>
                              </span>
                              <div className='input-group'>
                                <span className='input-group-addon addon-left fw-500'>
                                  Label
                                </span>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='label'
                                  defaultValue='Tax'
                                />
                              </div>
                            </div>
                            <div className='clearfix mar-btm'>
                              <span className='editor-label'>
                                <label className='switch'>
                                  <input
                                    type='checkbox'
                                    defaultChecked='checked'
                                  />
                                  <span className='drag-ball'>
                                    <span className='off' />
                                    <span className='on' />
                                  </span>
                                </label>
                                <span className='form-label'>Grand Total</span>
                              </span>
                              <div className='input-group'>
                                <span className='input-group-addon addon-left fw-500'>
                                  Label
                                </span>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='label'
                                  defaultValue='Grand Total'
                                />
                              </div>
                            </div>
                            <div className='clearfix'>
                              <span className='editor-label'>
                                <label className='switch'>
                                  <input
                                    type='checkbox'
                                    defaultChecked='checked'
                                  />
                                  <span className='drag-ball'>
                                    <span className='off' />
                                    <span className='on' />
                                  </span>
                                </label>
                                <span className='form-label'>
                                  Amount in Words
                                </span>
                              </span>
                              <div className='input-group'>
                                <span className='input-group-addon addon-left fw-500'>
                                  Label
                                </span>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='label'
                                  defaultValue='Amount in Words'
                                />
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                      <div id='tab-footer' className='tab-pane fade'>
                        <form className='custom-form'>
                          <div className='form-group clearfix'>
                            <span className='form-label fw-sbold w-100 mar-btm'>
                              Footer Options
                            </span>
                            <div className='clearfix mar-btm'>
                              <span className='form-label'>Text Size</span>
                              <div className='input-group'>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='text-size'
                                  onChange={(e)=>this.footer_functions(e.target.value,"textSize")}
                                />
                                <span className='input-group-addon fw-500'>
                                  pt
                                </span>
                              </div>
                            </div>
                            <div className='clearfix'>
                              <span className='form-label'>Text Color</span>
                              <div className='input-group'>
                                <input
                                  type='text'
                                  className='form-control'
                                  name='text-color'
                                  onChange={(e)=>this.footer_functions(e.target.value,"textcolor")}
                                />
                                <span className='input-group-addon'>
                                  <img
                                    src='images/color-wheel-icon.svg'
                                    alt='icon'
                                    className='mCS_img_loaded'
                                  />
                                </span>
                              </div>
                            </div>
                          </div>
                          <div className='form-group clearfix'>
                            <span className='editor-label'>
                              <label className='switch'>
                                <input
                                  type='checkbox'
                                  defaultChecked='checked'
                                  id="thankmessage_checkbox"
                                  onChange={(e)=>this.footer_functions(e.target.value,"thankmessage_checkbox")}
                                />
                                <span className='drag-ball'>
                                  <span className='off' />
                                  <span className='on' />
                                </span>
                              </label>
                              <span className='form-label'>
                                Thanking Message
                              </span>
                            </span>
                            <textarea
                              className='form-control'
                              placeholder='Thank you'
                              defaultValue={''}
                              onChange={(e)=>this.footer_functions(e.target.value,"thankmessage")}
                            />
                          </div>
                          <div className='form-group clearfix'>
                            <span className='editor-label'>
                              <label className='switch'>
                                <input
                                  type='checkbox'
                                  defaultChecked='checked'
                                  id="terms_checkbox"
                                  onChange={(e)=>this.footer_functions(e.target.value,"terms_checkbox")}
                                />
                                <span className='drag-ball'>
                                  <span className='off' />
                                  <span className='on' />
                                </span>
                              </label>
                              <span className='form-label'>
                                Terms &amp; Conditions
                              </span>
                            </span>
                            <div className='input-group mar-btm'>
                              <span className='input-group-addon addon-left fw-500'>
                                Label
                              </span>
                              <input
                                type='text'
                                onChange={(e)=>this.footer_functions(e.target.value,"terms")}
                                className='form-control'
                                name='label'
                                defaultValue='Terms & Conditions'
                              />
                            </div>
                            <textarea
                              className='form-control'
                              placeholder='Terms & Conditions'
                              defaultValue={''}
                              onChange={(e)=>this.footer_functions(e.target.value,"termsandcond")}
                            />
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  id='mCSB_1_scrollbar_vertical'
                  className='mCSB_scrollTools mCSB_1_scrollbar mCS-light mCSB_scrollTools_vertical mCSB_scrollTools_onDrag_expand'
                  style={{ display: 'block' }}
                >
                  <div className='mCSB_draggerContainer'>
                    <div
                      id='mCSB_1_dragger_vertical'
                      className='mCSB_dragger'
                      style={{
                        position: 'absolute',
                        minHeight: '30px',
                        display: 'block',
                        height: '15px',
                        maxHeight: '100px',
                        top: '0px'
                      }}
                    >
                      <div
                        className='mCSB_dragger_bar'
                        style={{ lineHeight: '30px' }}
                      />
                    </div>
                    <div className='mCSB_draggerRail' />
                  </div>
                </div>
              </div>
              <div className='btn-wrap text-right hidden-xs'>
                <button
                  className='btn btn-lightgray'
                  type='button'
                  
                >
                  Preview
                </button>
                <div className='custom-select-drop dropdown btn'>
                  <a
                    aria-expanded='false'
                    aria-haspopup='true'
                    role='button'
                    data-toggle='dropdown'
                    className='dropdown-toggle btn btn-green'
                    href='javascript:;'
                  >
                    <span id='selected'>Save</span>
                    <span className='caret' />
                  </a>
                  <ul className='dropdown-menu'>
                    <li className='active'>
                      <a
                        href='javascript:;'
                        // data-toggle='modal'
                        // data-target='#successModal'
                      >
                        Save
                      </a>
                    </li>
                    <li>
                      <a href='javascript:;'>Save &amp; Continue</a>
                    </li>
                    <li>
                      <a href='javascript:;'>Save as Copy</a>
                    </li>
                    <li>
                      <a href='javascript:;'>Save Draft</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            {/* Builder Left Ends here */}
            <div class='builder-right'>
              {this.state.html_content != ''
                ? parse(this.state.html_content)
                : ''}
            </div>
          </div>
        </div>
        {/* Main Wrapper Ends here */}
        {/* Modal Wrapper Starts here */}
        <div className='modal fade' id='successModal' role='dialog'>
          <div className='modal-dialog modal-md'>
            {/* Modal content*/}
            <button
              type='button'
              className='close hidden-xs'
              data-dismiss='modal'
            >
              <img
                className='img-responsive'
                src='images/close-red.svg'
                alt='icon'
              />
            </button>
            <div className='modal-content'>
              <div className='modal-body text-center success-modal'>
                <div className='pop-icon'>
                  <img src='images/template-success-icon.png' alt='icon' />
                </div>
                <h3>Awesome!</h3>
                <p className='fw-500'>
                  Your invoice template has been saved successfully
                </p>
                <button className='btn btn-green' data-dismiss='modal'>
                  OK
                </button>
              </div>
            </div>
          </div>
        </div>
        {/* Modal Wrapper Ends here */}
      </div>
    )
  }
}

export default custom_editable_template
