import React from 'react'
import LeftSidebar from './left_sidebar.js'
import Footer from './footer.js'
import htmlToImage from 'html-to-image'
import { PDFtoIMG } from 'react-pdf-to-image'
import { Document, Page } from 'react-pdf'
// import PDFViewer from 'pdf-viewer-reactjs'

import Topbar from './topbar.js'

import FetchAllApi from './api_links/fetch_all_api.js'
import parse from 'html-react-parser'

import jQuery from 'jquery'
// import 'bootstrap';
// import 'bootstrap-select';

class invoice_template_listing extends React.Component {
  constructor (props) {
    super(props)
    //const { history } = this.props;
    this.state = {
      dataUrl: '',
      logged_user_id: localStorage.getItem('logged_user_id'),
      logged_client_id: localStorage.getItem('logged_client_id'),
      logged_role_id: localStorage.getItem('logged_role_id'),
      logged_user_name: localStorage.getItem('logged_user_name'),
      logged_user_email: localStorage.getItem('logged_user_email'),
      logged_user_phone: localStorage.getItem('logged_user_phone'),
      logged_user_image: localStorage.getItem('logged_user_image'),
      logged_company_name: localStorage.getItem('logged_company_name'),
      logged_subscription_start_date: localStorage.getItem(
        'logged_subscription_start_date'
      ),
      logged_subscription_end_date: localStorage.getItem(
        'logged_subscription_end_date'
      ),
      logged_plan_id: localStorage.getItem('logged_plan_id'),
      dropdown: '',
      inbox_list: [],
      response_stus: 0,
      response_msg: 'No data found',
      item_details: '',
      item_file_path: '',
      list_id: '',
      response: '',
      htmlcont: ''
    }
  }

  UNSAFE_componentWillMount () {
    this.get_invoice_list()
    if (
      this.state.logged_user_id === '' ||
      this.state.logged_user_id === 'null' ||
      this.state.logged_user_id === 'undefined'
    ) {
      this.props.history.push('/')
    }

    var today = new Date()
    var dd = today.getDate()
    var mm = today.getMonth() + 1 //January is 0!
    var yyyy = today.getFullYear()
    if (dd < 10) {
      dd = '0' + dd
    }
    if (mm < 10) {
      mm = '0' + mm
    }
    var today_date = yyyy + '-' + mm + '-' + dd

    console.log('logged_user_id', this.state.logged_user_id)
    console.log(
      'logged_subscription_end_date',
      this.state.logged_subscription_end_date
    )

    if (this.state.logged_subscription_end_date < today_date) {
      //this.props.history.push('/register_Payment');
    }
  }

  get_invoice_list = () => {
    let client_id = 2
    FetchAllApi.get_edit_invoice_html(client_id, (err, response) => {
      console.log('defaultcatejhwdjkjhgorylist', response)
      if (response.status === 1) {
        this.setState({
          response: response,
          htmlcont: response.list[0].html_content
        })
      } else {
        this.setState({})
      }
    })
  }

  delete_invoice_list = id => {
    let client_id = 2
    let template_id = id
    FetchAllApi.delete_invoice_template(
      client_id,
      template_id,
      (err, response) => {
        console.log('defaultcatejhwdjkjhgorylist', response)
        alert(response.message)
        if (response.status === 1) {
          this.get_invoice_list()
          this.setState({})
        } else {
          this.setState({})
        }
      }
    )
  }

  componentDidMount () {
    //jQuery(".select-picker").selectpicker();

    require('jquery-mousewheel')
    require('malihu-custom-scrollbar-plugin')

    jQuery('.item-listwrap').mCustomScrollbar({
      scrollEasing: 'linear',
      scrollInertia: 600,
      scrollbarPosition: 'outside'
    })

    jQuery('.label-enclose .label span').click(function () {
      jQuery('.label-enclose .label').removeClass('active')
      jQuery(this)
        .parent('.label-enclose .label')
        .addClass('active')
    })
    jQuery('.label-enclose .label a').click(function () {
      jQuery(this)
        .parent('.label-enclose .label')
        .removeClass('active')
    })
  }
  onDocumentLoadSuccess = () => {
    console.log('success')
  }

  logoutLink () {
    localStorage.setItem('logged_user_id', '')
    localStorage.setItem('logged_client_id', '')
    localStorage.setItem('logged_role_id', '')
    localStorage.setItem('logged_user_name', '')
    localStorage.setItem('logged_user_email', '')
    localStorage.setItem('logged_user_phone', '')
    localStorage.setItem('logged_user_image', '')
    localStorage.setItem('logged_company_name', '')

    this.props.history.push('/')
  }

  //   dataTaggingFunc(list_id,file_id) {
  //     this.props.history.push("/data_tagging/" + list_id + "/" + file_id );
  //     window.scrollTo(0, 0);
  //   }

  routedChange (parameter) {
    this.props.history.push('/' + parameter)
    window.scrollTo(0, 0)
  }

  pageLink (page_slug) {
    this.props.history.push('/' + page_slug)
  }

  render () {
    return (
      <div id='mynode'>
        <div className='container-fluid'>
          <div className='row'>
            <LeftSidebar pageSubmit={e => this.pageLink(e)} />

            <div className='main-wrap col-md-12 col-xs-12 pad-r-no'>
              <div className='top-bar col-md-12 col-xs-12 pad-r-no'>
                <div className='nav-brand-res visible-xs'>
                  <img
                    className='img-responsive'
                    src='../images/logo-icon.png'
                    alt='LogoIcon'
                  />
                </div>

                <span className='page-title hidden-xs'>Invoice templates</span>

                <Topbar logoutSubmit={e => this.logoutLink()} />
              </div>
              <div className='main-content col-md-12 col-xs-12'>
                <span className='page-title visible-xs'>
                  Choose &amp; Customize
                </span>
                <div className='content-sec col-md-12 col-xs-12 pad-no mar-t-no template-enclose'>
                  <div className='col-md-4 col-sm-6 col-xs-12 template-wrap'>
                    <a href='#' className='btn create-temp-btn'>
                      <img src='images/create-template-icon.svg' alt='icon' />
                      Create Your Template
                    </a>
                  </div>

                  {this.state.response.list != undefined &&
                    this.state.response.list.map((i, index) => {
                      let template_id = i.id
                      //    alert(i.image_path)
                      return (
                        <div
                          className='col-md-4 col-sm-6 col-xs-12 template-wrap'
                          key={index}
                        >
                          <div className='col-md-12 col-xs-12 pad-no'>
                            <div className='thumb-img'>
                              {/* <PDFtoIMG file={i.image_path}>
            {({pages}) => {
                if (!pages.length) return 'Loading...';
                return pages.map((page, index)=>
                    <img className='bdr-all img-responsive' key={index} src={page}/>
                );
            }}
        </PDFtoIMG> */}

                              {/* <Document

          file={i.image_path}
          onLoadSuccess={this.onDocumentLoadSuccess}
        >
          <Page pageNumber={1} />
        </Document> */}

                              {/* <iframe src={i.image_path} width="390px" height="302px" style={{overflow:'hidden'}}>
    </iframe> */}
                              <embed
                                src={i.image_path}
                                width='394px'
                                height='296px'
                              />

                              {/* <img
                                src={this.state.dataUrl!=''?this.state.dataUrl:""}
                                className='bdr-all img-responsive'
                                alt='thumb-img'
                              /> */}
                              {/* <PDFtoIMG file={i.image_path}>
            {({pages}) => {
                if (!pages.length) return 'Loading...';
                return pages.map((page, index)=>
                    <img className='bdr-all img-responsive' key={index} src={page}/>
                );
            }}
        </PDFtoIMG> */}

                              <div className='thumb-menu'>
                                <a
                                  href='invoice-editor-1.html'
                                  className='btn btn-green'
                                >
                                  Edit
                                </a>
                                <div className='dropdown menu-item'>
                                  <button
                                    className='btn btn-yellow dropdown-toggle'
                                    data-toggle='dropdown'
                                    aria-expanded='true'
                                  >
                                    More
                                  </button>
                                  <ul className='dropdown-menu'>
                                    <li>
                                      <a href='javascript:;'>Set as Default</a>
                                    </li>
                                    <li>
                                      <a href='javascript:;'>Preview</a>
                                    </li>
                                    <li>
                                      <a>Duplicate</a>
                                    </li>
                                    <li>
                                      <a
                                        onClick={() =>
                                          this.delete_invoice_list(template_id)
                                        }
                                      >
                                        Delete
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>
                          <span className='col-md-12 col-xs-12 text-center fw-med'>
                            {this.state.response.list[index].template_name}{' '}
                          </span>
                        </div>
                      )
                    })}
                </div>

                {/* <PDFViewer
            document={{
                url: 'http://ec2-54-251-142-179.ap-southeast-1.compute.amazonaws.com:9002/invoice_templates_pdf/Template-1.pdf',
            }}
        /> */}
              </div>
              {/* Main Content Ends here */}
            </div>
          </div>
        </div>

        <Footer logoutSubmit={e => this.logoutLink(e)} />
      </div>
    )
  }
}
export default invoice_template_listing
