import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";

//import config from './api_links/api_links';

import login from './login.js';
import user_inbox from './user_inbox.js';
import forgot_password from './forgot_password.js';
import data_tagging from './data_tagging';
import inbox from './inbox.js';
import compose from './compose.js';
import sent_items from './sentItems';
import reviewedItems from './reviewedItems.js';
import register from "./register.js";
import registerPayment from "./registerPayment.js";
import rolesAndPermissions from "./rolesAndPermissions.js";
import registerFinished from "./registerFinished.js";
import custom_editable_template from "./custom_editable_template.js";

class commonlinks extends React.Component {

    constructor(props) {
        super(props);
        //const { history } = this.props;
        this.state = { logged_user_id: localStorage.getItem("logged_user_id"), logged_client_id: localStorage.getItem("logged_client_id"), logged_role_id: localStorage.getItem("logged_role_id"), logged_user_name: localStorage.getItem("logged_user_name"), logged_user_email: localStorage.getItem("logged_user_email"), logged_user_phone: localStorage.getItem("logged_user_phone"), logged_user_image: localStorage.getItem("logged_user_image"), logged_company_name: localStorage.getItem("logged_company_name"), dropdown: '', inbox_list: [], response_stus: 0, response_msg: 'No data found', item_details: '', item_file_path: '' };
    }

    render() {


       return (


           <div>  

           	<Router>

                   <div>
                    <Route exact path="/" component={login} />
                    <Route exact path="/login" component={login} />
                    <Route exact path="/user_inbox" component={user_inbox} />
                    <Route exact path="/forgot_password" component={forgot_password} />
                    <Route exact path="/data_tagging/:list_id/:file_id" component={data_tagging} />
                    <Route exact path="/inbox" component={inbox} />
                    <Route exact path="/compose" component={compose} />
                    <Route exact path="/sent_items" component={sent_items} />
                    <Route exact path="/reviewed_items" component={reviewedItems} />
                    <Route exact path="/register" component={register} />
                    <Route exact path="/register_Payment" component={registerPayment} />
                    <Route exact path="/roles_permissions" component={rolesAndPermissions} />
                    <Route exact path="/register_finished" component={registerFinished} />
                    <Route exact path="/editable" component={custom_editable_template} />

                   </div>

               </Router>

           </div>        
        );

   }

}

export default commonlinks

