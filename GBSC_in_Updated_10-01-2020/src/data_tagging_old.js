import React from 'react';
import LeftSidebar from './left_sidebar.js';
import Footer from './footer.js';

import Topbar from './topbar.js';

import FetchAllApi from './api_links/fetch_all_api.js';

import jQuery from 'jquery';

import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";



class data_tagging extends React.Component {

    constructor(props) {
        super(props);
        //const { history } = this.props;
        this.state = { logged_user_id: localStorage.getItem("logged_user_id"), logged_client_id: localStorage.getItem("logged_client_id"), logged_role_id: localStorage.getItem("logged_role_id"), logged_user_name: localStorage.getItem("logged_user_name"), logged_user_email: localStorage.getItem("logged_user_email"), logged_user_phone: localStorage.getItem("logged_user_phone"), logged_user_image: localStorage.getItem("logged_user_image"), logged_company_name: localStorage.getItem("logged_company_name"), get_file_path: [], add_cmnt_msg: '', file_comments: [], list_id: this.props.match.params.list_id, is_called: false, sub_comments:[], checkSubComments: false, clickedParentId: null, attachment_file: [], attachment_file_jquery: [], attachment_file_length: 0, attachment_fileName: [], currencies: [], item_total_foreign_currency: "", tax_amount_foreign_currency: "", grand_total_foreign_currency: "", item_total_home_currency: 1, ToCurrency: "USD", company_name: "", invoice_no: "", incorport_date: "", address: "", account_category: "", exchange_value: ""};

        this.loadFile = this.loadFile.bind(this);
    }

    UNSAFE_componentWillMount() {
        //console.log("logged_user_id", this.state.logged_user_id);
        jQuery('title').html('Data Tagging | GBSC');

        if(this.state.logged_user_id === "" || this.state.logged_user_id === "null" || this.state.logged_user_id === "undefined"){
            this.props.history.push('/');
        }

        var file_id = this.props.match.params.file_id;

        FetchAllApi.getFilePath(file_id, (err, response) => {
        //console.log('\Article Data', response);
            if(response.status === 1){
                this.setState({
                    get_file_path: response.file_path
                })
            } else{

            }
        });

        this.getCommments(file_id);

    }

    getCommments(file_id){
        FetchAllApi.getFileCmnts(file_id, (err, response) => {
        //console.log('\Article Data', response);
            if(response.status === 1){
                this.setState({
                    file_comments: response.details
                })
            } else{

            }
        });
    }

    handleClick(e, data) {
        console.log(data.foo);
    }

    loadFile(e) {
        var files = e.target.files;    
        this.setState({attachment_file_length: files.length})
    
        if(files.length > 0){    
          var fileArra = this.state.attachment_file;
          //var fileThumbArra = this.state.imgThumb;
    
          for (var i = 0; i < files.length; i++) {
            fileArra.push(e.target.files[i]);
            this.setState({ 
            //   selectedFile:URL.createObjectURL(e.target.files[i]),
              attachment_file:fileArra
            });
          }
        }

        console.log('attachments_length', this.state.attachment_file.length);
    
    }

    addCommentFunc(e){
        e.preventDefault();
        var pstCommnt = jQuery("#comment_text").val();
        var user_id = parseInt(this.state.logged_user_id);
        var list_id = parseInt(this.props.match.params.list_id);
        var file_id = parseInt(this.props.match.params.file_id);
        var parent_comment_id = 0;
        var attachments = '';

        FetchAllApi.addComment(pstCommnt,user_id,list_id,file_id,attachments,parent_comment_id, (err, response) => {
            console.log('add comment', response.status);
            if(response.status === 1){
                this.setState({
                    add_cmnt_msg: response.message
                })
                jQuery(".comment-sec")[0].reset();
                
                jQuery(".resp_msg").fadeIn(2000);
                setTimeout(function(){ jQuery(".resp_msg").fadeOut(2000); },8000);

                this.getCommments(file_id);
            } else{
                this.setState({
                    add_cmnt_msg: response.message
                })
                jQuery(".resp_msg").fadeIn(2000);
                setTimeout(function(){ jQuery(".resp_msg").fadeOut(2000); },8000);
            }
        });
    }

    addSubCommentFunc(e){
        e.preventDefault();
        var pstCommnt = e.target.elements.reply_txt.value;
        var user_id = parseInt(this.state.logged_user_id);
        var list_id = parseInt(this.props.match.params.list_id);
        var file_id = parseInt(this.props.match.params.file_id);
        var parent_comment_id = e.target.elements.parent_comment_id.value;
        if(this.state.attachment_file.length > 0){
            var attachments = this.state.attachment_file;
        } else{
            var attachments = '';
        }
        
        
        
        this.addSubComment(pstCommnt,user_id,list_id,file_id,attachments,parent_comment_id);
    }

    addSubComment(pstCommnt,user_id,list_id,file_id,attachments,parent_comment_id){
        console.log('attachments', attachments);

        FetchAllApi.addComment(pstCommnt,user_id,list_id,file_id,attachments,parent_comment_id, (err, response) => {
            console.log('add comment', response.status);
            if(response.status === 1){
                this.setState({
                    add_cmnt_msg: response.message,
                    attachment_file: []
                })
                jQuery(".reply_txt_cls").val('');
                jQuery(".add_img").val('');
                jQuery(".files_txt").html('');

                jQuery(".resp_msg").fadeIn(2000);
                setTimeout(function(){ jQuery(".resp_msg").fadeOut(2000); },8000);

                this.getCommments(file_id);
                this.replyLink(parent_comment_id, "replyBtn");
            } else{
                this.setState({
                    add_cmnt_msg: response.message
                })
                jQuery(".resp_msg").fadeIn(2000);
                setTimeout(function(){ jQuery(".resp_msg").fadeOut(2000); },8000);
            }
        });
    }


    dataTaggingFunc(list_id,file_id){
        this.props.history.push("/data_tagging/" + list_id + "/" + file_id );
        window.scrollTo(0,0);
    }
    
    
    logoutLink(){

        localStorage.setItem("logged_user_id", "");
        localStorage.setItem("logged_client_id", "");
        localStorage.setItem("logged_role_id", "");
        localStorage.setItem("logged_user_name", "");
        localStorage.setItem("logged_user_email", "");
        localStorage.setItem("logged_user_phone", "");
        localStorage.setItem("logged_user_image", "");
        localStorage.setItem("logged_company_name", "");

        this.props.history.push('/');
    }

    routedChange(parameter){
        this.props.history.push('/'+parameter);
        window.scrollTo(0,0);
    }

    pageLink(page_slug){
        this.props.history.push('/'+page_slug);
    }

    updateCmmnt(e){
        e.preventDefault();
        var comment_id = e.target.elements.comment_id.value;
        var comment_text = e.target.elements.comment_text.value;
        var user_id = this.state.logged_user_id;
        var file_id = this.props.match.params.file_id;
        var parent_comment_id = e.target.elements.parent_comment_id.value;
        
        this.updateCommentApi(comment_id, comment_text, user_id, file_id, parent_comment_id);
    }

    updateCommentApi(comment_id, comment_text, user_id, file_id, parent_comment_id){
        FetchAllApi.updateComment(comment_id,comment_text,user_id, (err, response) => {
            console.log('update comment', response.status);
            if(response.status === 1){
                this.setState({
                    add_cmnt_msg: response.message
                })
                
                jQuery(".resp_msg").fadeIn(2000);
                setTimeout(function(){ jQuery(".resp_msg").fadeOut(2000); },8000);

                jQuery(".comment-txt").removeClass('hide');
                jQuery(".update_cmnt").addClass('hide');

                this.getCommments(this.props.match.params.file_id);
                // if(parseInt(parent_comment_id) > 0){
                //     this.replyFunc(parent_comment_id);
                // }
                this.replyLink(parent_comment_id, "replyUpdt");
            } else{
                this.setState({
                    add_cmnt_msg: response.message
                })
                jQuery(".resp_msg").fadeIn(2000);
                setTimeout(function(){ jQuery(".resp_msg").fadeOut(2000); },8000);
            }
        });
    }

    deleteComment(comment_id, list_id, parent_comment_id){
        FetchAllApi.deleteComment(comment_id,list_id, (err, response) => {
            console.log('delete comment', response.status);
            if(response.status === 1){
                this.setState({
                    add_cmnt_msg: response.message
                })

                //jQuery(".comment-txt").removeClass('hide');
                //jQuery(".reply-form").addClass('hide');
                
                jQuery(".resp_msg").fadeIn(2000);
                setTimeout(function(){ jQuery(".resp_msg").fadeOut(2000); },8000);

                //this.getCommments(this.props.match.params.file_id);
                if(parseInt(parent_comment_id) > 0){
                    console.log('parent_comment_id123', parent_comment_id);
                    this.replyLink(parseInt(parent_comment_id), "replyDel");
                } else{
                    this.getCommments(this.props.match.params.file_id);
                }
            } else{
                this.setState({
                    add_cmnt_msg: response.message
                })
                jQuery(".resp_msg").fadeIn(2000);
                setTimeout(function(){ jQuery(".resp_msg").fadeOut(2000); },8000);
            }
        });
    }

    // replyFunc(parent_comment_id){
    //     FetchAllApi.getSubCmmnts(parent_comment_id, (err, response) => {
    //     //console.log('\Article Data', response);
    //         if(response.status === 1){
    //             this.setState({
    //                 sub_comments: response.details
    //             })
    //         } else{

    //         }
    //     });
    // }
    componentDidUpdate(){
        var THIS = this;
        jQuery(".edit_cmnt").click(function(){
            var text_cmnt = jQuery(this).closest(".prnt_cmnt").next(".comment-txt").text();
            var this_cmnt_id = jQuery(this).attr('data-comment-id');
            jQuery(this).closest(".prnt_cmnt").next(".comment-txt").addClass('hide');
            jQuery(this).closest(".prnt_cmnt").next(".comment-txt").next(".reply-form").removeClass('hide');
            jQuery("#cmmnt_txt_id"+this_cmnt_id).val(text_cmnt);
        });

        jQuery(".cancel_button").click(function(){
            jQuery(this).closest(".reply-form").addClass('hide');
            jQuery(this).closest(".reply-form").prev(".comment-txt").removeClass('hide');
        });
 
    }

    replyLink(parent_comment_id, replyToggele){
        var THIS = this;
        //alert(replyToggele+' '+typeof(parent_comment_id));
        if(replyToggele === "replyLink"){
            jQuery("#reply_cnt"+parent_comment_id).toggleClass("in");
        }

        console.log('parent_comment_id', parent_comment_id);

        FetchAllApi.getSubCmmnts(parent_comment_id, (err, response) => {
        //console.log('\Article Data', response);
            if(response.status === 1){
                this.setState({
                    sub_comments: response.details
                })
                
                var comments_length = response.details.length;
                var comment_list = [];
                if(comments_length > 0){
                    for(var i=0; i<comments_length; i++){
                        if(response.details[i].user_image){
                            var user_img = response.details[i].user_image
                        } else{
                            var user_img = "../../images/user-img-1.png";
                        }

                        var comment_id = response.details[i].comment_id;

                        if(response.details[i].sub_comment_count > 0){
                            var replyTxt = "Replies ("+response.details[i].sub_comment_count+")";
                        } else{
                            var replyTxt = "Reply";
                        }
                        var dis_file_path = '';
                        var get_file_path = response.details[i].file_path;
                        console.log('get_file_path_'+i, response.details[i].file_path);
                        if(get_file_path.length > 0){   
                            var dis_file_path = [];                         
                            var attach_file_path = [];
                            var split_file_path = get_file_path.toString().split(',');

                            var split_file_id = response.details[i].attachments.toString().split(',');

                            if(get_file_path.length > 1){                                                    
                                for(var j=0; j<get_file_path.length; j++){
                                    var get_file_name = split_file_path[j];
                                    var split_file_name = split_file_path[j].toString().split('/');
                                    var arr_reverse = split_file_name.reverse();

                                    var get_file_ext = arr_reverse[0].substring(arr_reverse[0].lastIndexOf('.')+1, arr_reverse[0].length);
                                    if(get_file_ext === "pdf"){
                                        var file_icon =  "../../images/pdf-icon.png";
                                    } else{
                                        var file_icon =  "../../images/img-icon.png";
                                    }
                                    
                                    dis_file_path.push('<a onClick="dataTaggingFunc('+this,this.state.list_id,split_file_id[j]+')"><img src="'+file_icon+'" alt="'+get_file_ext+'" /><span>'+arr_reverse[0].substring(arr_reverse[0].length - 15, arr_reverse[0].length)+'</span></a>');
                                }
                            } else{
                                var split_file_name = response.details[i].file_path[0].toString().split('/');
                                var arr_reverse = split_file_name.reverse();
                                
                                var get_file_ext = arr_reverse[0].substring(arr_reverse[0].lastIndexOf('.')+1, arr_reverse[0].length);
                                if(get_file_ext === "pdf"){
                                    var file_icon =  "../../images/pdf-icon.png";
                                } else{
                                    var file_icon =  "../../images/img-icon.png";
                                }
                                dis_file_path.push('<a onClick="dataTaggingFunc('+this,this.state.list_id,split_file_id[0]+')"><img src="'+file_icon+'" alt="'+get_file_ext+'" /><span>'+arr_reverse[0].substring(arr_reverse[0].length - 15, arr_reverse[0].length)+'</span></a>');
                            }
                        }

                        //comment_list.push('<div class="reply_cmment_row" id="'+response.details[i].comment_id+'">gfgf</div>');

                        var replyFrm = '<div class="reply_cnt collapse" id=reply_cnt'+comment_id+'><form action="javascript:;" class="col-md-12 col-xs-12 pad-no reply-form reply-frm"><input type="hidden" class="parent_comment_id" name="parent_comment_id" value="'+comment_id+'" /><textarea class="col-md-12 col-xs-12 reply_txt_cls" name="reply_txt" placeholder="Reply..." required></textarea><div class="pull-right"><div className="files_txt"></div><a href="javascript:;" class=" btn btn-empty"><input type="file" name="imgInp[]" id="imgInp2" class="add_img replyAttach" id="upload_file_'+comment_id+'" multiple accept="image/*,application/pdf" /><img src="../../images/attach-icon.svg" alt="icon"/></a><button type="submit" class="btn btn-green replySubmit"><img src="../../images/reply-icon.svg" alt="icon"/>Reply</button></div></form><div class="subCmnt"></div></div>';

                        comment_list.push('<div class="reply_cmment_row col-md-12 col-xs-12 pad-no" id="'+response.details[i].comment_id+'"><div class="reply-cont col-md-12 col-xs-12"><div class="col-md-12 col-xs-12 pad-no"><div class="avatar-img"><img class="img-responsive" src="'+user_img+'" alt="AvatarIMG"/></div><div class="reply-user"><span class="col-md-12 col-xs-12 pad-no user-name">'+response.details[i].comment_user+'</span><span class="col-md-12 col-xs-12 pad-no date">'+response.details[i].ago_value+'</span></div></div><div class="dropdown menu-item prnt_cmnt sub_cmnt"><a href="javascript" class="dropdown-toggle" data-toggle="dropdown"><img src="../../images/menu-dot.svg" alt="icon"/></a><ul class="dropdown-menu"><li><a href="javascript:;" data-comment-id="'+response.details[i].comment_id+'" class="edit_cmnt">Edit</a></li><li><a href="javascript:;" data-comment-id="'+response.details[i].comment_id+'" data-parent-id="'+parent_comment_id+'" class="del_cmnt">Delete</a></li></ul></div><p class="col-md-12 col-xs-12 pad-no comment-txt">'+response.details[i].comment_text+'</p><form action="javascript:;" class="col-md-12 col-xs-12 pad-no reply-form update_cmnt hide"><input type="hidden" name="comment_id" class="comment_id" value="'+response.details[i].comment_id+'" /><input type="hidden" name="parent_comment_id" class="parent_comment_id" value="'+response.details[i].comment_id+'" /><textarea class="col-md-12 col-xs-12 cmnt_txt" id="cmmnt_txt_id'+response.details[i].comment_id+'" name="comment_text" placeholder="" required></textarea><div class="pull-right"><button type="button" class="btn btn-default cancel_button">Cancel</button><button type="submit" class="btn btn-green updateCmnt">Update</button></div></form><div class="attachment-item col-md-12 col-xs-12 pad-no">'+dis_file_path+'</div></div><div class="col-md-12 col-xs-12 pad-no"><button class="btn btn-lightgray">Resolved</button><a href="javascript:;" class="reply-link" data-comment-id="'+response.details[i].comment_id+'" onclick="return replyLink('+response.details[i].comment_id+');">'+replyTxt+'</a></div>'+replyFrm+'</div>');
                    }

                    jQuery("#reply_cnt"+parent_comment_id).children('.subCmnt').html(comment_list);

                    jQuery(".edit_cmnt").click(function(){
                        var text_cmnt = jQuery(this).closest(".prnt_cmnt").next(".comment-txt").text();
                        var this_cmnt_id = jQuery(this).attr('data-comment-id');
                        jQuery(this).closest(".prnt_cmnt").next(".comment-txt").addClass('hide');
                        jQuery(this).closest(".prnt_cmnt").next(".comment-txt").next(".reply-form").removeClass('hide');
                        jQuery("#cmmnt_txt_id"+this_cmnt_id).val(text_cmnt);
                    });
            
                    jQuery(".cancel_button").click(function(){
                        jQuery(this).closest(".reply-form").addClass('hide');
                        jQuery(this).closest(".reply-form").prev(".comment-txt").removeClass('hide');
                    });

                    jQuery(".updateCmnt").click(function(e){
                        e.preventDefault();                            
                        var parent_comment_id = jQuery(this).closest(".reply-form").children(".parent_comment_id").val();
                        var comment_text = jQuery(this).closest(".pull-right").prev(".cmnt_txt").val();
                        var user_id = THIS.state.logged_user_id;
                        var file_id = THIS.props.match.params.file_id;

                        THIS.updateCommentApi(comment_id, comment_text, user_id, file_id, parent_comment_id);
                        jQuery(this).closest(".reply-form").prev(".comment-txt").text(comment_text);
                        
                    });

                    jQuery(".del_cmnt").click(function(e){
                        e.preventDefault();
                        var comment_id = jQuery(this).attr('data-comment-id');
                        var parent_comment_id = jQuery(this).attr('data-parent-id');                        
                        var user_id = parseInt(THIS.state.logged_user_id);
                        var list_id = parseInt(THIS.props.match.params.list_id);
                        var file_id = parseInt(THIS.props.match.params.file_id);

                        THIS.deleteComment(comment_id, list_id, parent_comment_id);
                    });

                    jQuery(".replySubmit").click(function(e){
                        e.preventDefault();

                        var pstCommnt = jQuery(this).closest(".pull-right").prev(".reply_txt_cls").val();
                        var user_id = parseInt(THIS.state.logged_user_id);
                        var list_id = parseInt(THIS.props.match.params.list_id);
                        var file_id = parseInt(THIS.props.match.params.file_id);
                        var parent_comment_id = jQuery(this).closest(".reply-form").children(".parent_comment_id").val();
                        if(THIS.state.attachment_file_jquery.length > 0){
                            var attachments = THIS.state.attachment_file_jquery;
                        } else{
                            var attachments = '';
                        }

                        console.log('jquery_attachments', attachments);
                        
                        
                        THIS.addSubComment(pstCommnt,user_id,list_id,file_id,attachments,parent_comment_id);
                    });

                    jQuery(".replyAttach").on('change', function(e){
                        var files = e.target.files;    
                        THIS.setState({attachment_file_length: files.length})
                    
                        if(files.length > 0){    
                            var fileArra = THIS.state.attachment_file_jquery;
                            //var fileThumbArra = this.state.imgThumb;
                        
                            for (var i = 0; i < files.length; i++) {
                                fileArra.push(e.target.files[i]);
                                THIS.setState({ 
                                //   selectedFile:URL.createObjectURL(e.target.files[i]),
                                attachment_file_jquery:fileArra
                                });
                            }
                        }

                        jQuery(this).closest(".pull-right").children(".files_txt").html("2 files are attached");

                        console.log('attachments_length', THIS.state.attachment_file_jquery.length);
                        console.log('attachment_file', THIS.state.attachment_file_jquery);
                    });
                }
            } else{
                jQuery("#reply_cnt"+parent_comment_id).children('.subCmnt').html("");
            }
        });
    }


    selectHandler = event => {
        event.preventDefault();
        let ToCurrency = event.target.value;
        this.setState({ ToCurrency: event.target.value });
        // this.convertHandler(ToCurrency);
    }


    convertHandler = (ToCurrency) => {
        // let currency_type = this.state.ToCurrency;

        let currency_type = ToCurrency;

        let item_total = this.state.item_total_home_currency;
        let tax = item_total / 4;
        let grant_total = item_total + tax;
        if (item_total != "") {
            fetch('https://api.exchangerate-api.com/v4/latest/SGD')
                .then(response => response.json())
                .then(data => {
                    let todayValue = data.rates;
                    // console.log('qwerty',todayValue[hello]);
                    let res1 = todayValue[currency_type] * item_total;
                    let converted_item_total = res1.toFixed(2);

                    let res2 = todayValue[currency_type] * tax;
                    let converted_tax = res2.toFixed(2);

                    let res3 = todayValue[currency_type] * grant_total;
                    let converted_grant_total = res3.toFixed(2);
                    let exchange_valuee = todayValue[currency_type]
                    this.setState({
                        item_total_foreign_currency: converted_item_total,
                        tax_amount_foreign_currency: converted_tax,
                        grand_total_foreign_currency: converted_grant_total,
                        exchange_value: exchange_valuee
                    })

                }

                );
        }

    }
    saveAndContinue = () => {
        let nike = {
            company_name: this.state.company_name,
            invoice_no: this.state.invoice_no,
            address: this.state.address,
            incorport_date: this.state.incorport_date,
            account_category: this.state.account_category,
        }
        console.log('nike', nike)
    }
    convertCurrency = (e) => {
        if (this.state.ToCurrency != "") {
            let target_name = e.target.name
            let hello = this.state.ToCurrency
            let value = e.target.value;
            // console.log('qwerty',hello)

            fetch('https://api.exchangerate-api.com/v4/latest/SGD')
                .then(response => response.json())
                .then(data => {
                    let todayValue = data.rates;
                    // console.log('qwerty',todayValue[hello]);
                    let convertedValueed = todayValue[hello] * value;
                    let convertedValuee = convertedValueed.toFixed(2);
                    let exchange_value= todayValue[hello];
                    if (target_name === "grand_total_home_currency") {
                        this.setState({ grand_total_foreign_currency: convertedValuee ,})

                    } else if (target_name === "item_total_home_currency") {
                        this.setState({ item_total_foreign_currency: convertedValuee ,exchange_value:exchange_value})

                    }
                    else if (target_name === "tax_amount_home_currency") {
                        this.setState({ tax_amount_foreign_currency: convertedValuee })

                    }
                    console.log('qwerty', convertedValuee);

                    //  const values = Object.values(todayValue)

                    //  for (let value of Object.values()) {
                    // //    if() alert(value); // John, then 30
                    //   }

                }

                );

        }

    }

    handleChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }
    

    render() {

        let file_path = [], file_path_list = '', scanned_div = [], comment_list = [];

        file_path_list = this.state.get_file_path.toString();
        if(file_path_list !== ""){

            var get_file_ext = file_path_list.substring(file_path_list.lastIndexOf('.')+1, file_path_list.length);
            if(get_file_ext === "png" || get_file_ext === "jpg" || get_file_ext === "jpeg"){
                file_path.push(
                    <li><a href="javascript:;" className="active"><img src={file_path_list} className="img-responsive" /></a></li>
                )

                scanned_div.push(
                    <div className="scanned-file">
                        <img src={file_path_list} alt="Scanned-file" />
                    </div>
                )

                
            } else{
                var pdf_file_url = file_path_list + "#toolbar=0&navpanes=0";
                file_path.push(
                    <li><a href="javascript:;" className="active">
                        {/* <img src={file_path_list} className="img-responsive" /> */}
                        <iframe src={pdf_file_url} className="data_tagging_thumb" frameborder="0" scrolling="no" ></iframe>
                    </a></li>
                )

                scanned_div.push(
                    <div className="scanned-file">
                        {/* <img src={file_path_list} alt="Scanned-file" /> */}
                        <iframe src={pdf_file_url} className="data_tagging_large" frameborder="0" scrolling="no"></iframe>
                    </div>
                )
            }
        }


       return (

        <div>

            <div className="container-fluid">
                <div className="row">
                    
                    <LeftSidebar pageSubmit = {(e) => this.pageLink(e)} />

                    <div className="main-wrap col-md-12 col-xs-12 pad-r-no">

                        <div className="top-bar col-md-12 col-xs-12 pad-r-no">
                            <div className="nav-brand-res visible-xs">
                                <img className="img-responsive" src="../../images/logo-icon.png" alt="LogoIcon" /></div>
                            <a onClick={this.routedChange.bind(this,'user_inbox')} className="back hidden-xs">
                                <svg xmlns="http://www.w3.org/2000/svg" width="18.5" height="14.249" viewBox="0 0 18.5 14.249">
                                <g id="left-arrow_2_" data-name="left-arrow (2)" transform="translate(0 -58.83)">
                                <g id="Group_25" data-name="Group 25" transform="translate(0 65.207)">
                                <g id="Group_24" data-name="Group 24" transform="translate(0 0)">
                                <path id="Path_19" data-name="Path 19" d="M17.753,235.318H.747a.747.747,0,0,0,0,1.495H17.753a.747.747,0,0,0,0-1.495Z" transform="translate(0 -235.318)"/>
                                </g>
                                </g>
                                <g id="Group_27" data-name="Group 27" transform="translate(0 58.83)">
                                <g id="Group_26" data-name="Group 26" transform="translate(0 0)">
                                <path id="Path_20" data-name="Path 20" d="M1.8,65.954l5.849-5.849A.747.747,0,1,0,6.6,59.049L.219,65.426a.747.747,0,0,0,0,1.057L6.6,72.86A.747.747,0,1,0,7.653,71.8Z" transform="translate(0 -58.83)"/>
                                </g>
                                </g>
                                </g>
                                </svg>
                            </a>
                            <span className="page-title hidden-xs">Inbox / <span>Data Tagging</span></span>

                            <Topbar logoutSubmit = {(e) => this.logoutLink()} />
                        </div>


                        <div className="main-content col-md-12 col-xs-12">
                            <div className="resp_msg">{this.state.add_cmnt_msg}</div>

                            <input type="hidden" id="logged_user_id" value={this.state.logged_user_id} />
                            <input type="hidden" id="file_id" value={this.props.match.params.file_id} />
                            <input type="hidden" id="list_id" value={this.props.match.params.list_id} />

                            <a onClick={this.routedChange.bind(this,'user_inbox')} className="back visible-xs">
                                {/* <img src="../../images/back-arrow-dark.svg"> */}
                                <svg xmlns="http://www.w3.org/2000/svg" width="18.5" height="14.249" viewBox="0 0 18.5 14.249">
                                <g id="left-arrow_2_" data-name="left-arrow (2)" transform="translate(0 -58.83)">
                                <g id="Group_25" data-name="Group 25" transform="translate(0 65.207)">
                                <g id="Group_24" data-name="Group 24" transform="translate(0 0)">
                                <path id="Path_19" data-name="Path 19" d="M17.753,235.318H.747a.747.747,0,0,0,0,1.495H17.753a.747.747,0,0,0,0-1.495Z" transform="translate(0 -235.318)"/>
                                </g>
                                </g>
                                <g id="Group_27" data-name="Group 27" transform="translate(0 58.83)">
                                <g id="Group_26" data-name="Group 26" transform="translate(0 0)">
                                <path id="Path_20" data-name="Path 20" d="M1.8,65.954l5.849-5.849A.747.747,0,1,0,6.6,59.049L.219,65.426a.747.747,0,0,0,0,1.057L6.6,72.86A.747.747,0,1,0,7.653,71.8Z" transform="translate(0 -58.83)"/>
                                </g>
                                </g>
                                </g>
                                </svg>
                            </a>
                            <span className="page-title visible-xs">Inbox / <span>Data Tagging</span></span>
                            <div className="content-top col-md-12 col-xs-12 pad-no">
                                <select className="select-dropdown selectpicker">
                                    <option>Bill-Payment.pdf</option>
                                    <option>Taxi-bill.jpg</option>
                                    <option>Stationary.jpg</option>
                                </select>
                            </div>

                            <div className="content-sec col-md-12 col-xs-12 pad-no inbox-listing">
                                <div className="col-md-12 col-xs-12 scanned-wrap">
                                    <p className="visible-xs note-content">This feature cant use in mobile. Please use Desktop</p>
                                    <div className="col-md-6 col-md-12 scanned-left hidden-xs">
                                        <div className="file-thumbnail">
                                            <ul className="list-unstyled">
                                                {file_path}
                                            </ul>                                            
                                        </div>
                                        <div className="doc-wrap">
                                            <div className="zoom-btn">
                                                <a href="javascript:;" className="plus"><img src="../../images/zoom-in.svg" alt="icon"/></a>
                                                <a href="javascript:;" className="minus"><img src="../../images/zoom-out.svg" alt="icon"/></a>
                                            </div>

                                            <ContextMenuTrigger id="some_unique_identifier">

                                            {scanned_div}      

                                            </ContextMenuTrigger>    

                                            <ContextMenu id="some_unique_identifier">
                                                <MenuItem data={{foo: 'bar1'}} onClick={this.handleClick}>Copy</MenuItem>
                                                <MenuItem data={{foo: 'bar2'}} onClick={this.handleClick}>Add to First Name</MenuItem>
                                                <MenuItem divider />
                                                <MenuItem data={{foo: 'bar3'}} onClick={this.handleClick}>Add to Last Name</MenuItem>
                                            </ContextMenu>                                  

                                            <form className="comment-sec" method="post" onSubmit={this.addCommentFunc.bind(this)}>
                                                <textarea cols="3" rows="5" name="comment_text" id="comment_text" className="form-control" placeholder="Comments" required></textarea>
                                                <button className="btn btn-green" type="submit">Send</button>
                                            </form>

                                            

                                            <div className="comments-wrap">
                                                {
                                                    this.state.file_comments.map((comments_data, index) => {
                                                        if(comments_data.user_image){
                                                            var user_img = comments_data.user_image
                                                        } else{
                                                            var user_img = "../../images/user-img-1.png";
                                                        }

                                                        if(comments_data.user_id === parseInt(this.state.logged_user_id)){
                                                            var disp_cmnt = 'show';
                                                        } else{
                                                            var disp_cmnt = 'hide';
                                                        }

                                                        var get_file_path = comments_data.file_path;
                                                        if(get_file_path.length > 0){
                                                            var dis_file_path = [];
                                                            var attach_file_path = [];
                                                            var split_file_path = get_file_path.toString().split(',');

                                                            var split_file_id = comments_data.attachments.toString().split(',');

                                                            if(get_file_path.length > 1){                                                    
                                                                for(var i=0; i<2; i++){
                                                                    var get_file_name = split_file_path[i];
                                                                    var split_file_name = split_file_path[i].toString().split('/');
                                                                    var arr_reverse = split_file_name.reverse();

                                                                    var get_file_ext = arr_reverse[0].substring(arr_reverse[0].lastIndexOf('.')+1, arr_reverse[0].length);
                                                                    if(get_file_ext === "pdf"){
                                                                        var file_icon =  "images/pdf-icon.png";
                                                                    } else{
                                                                        var file_icon =  "images/img-icon.png";
                                                                    }
                                                                    
                                                                    dis_file_path.push(<a onClick={this.dataTaggingFunc.bind(this,this.state.list_id,split_file_id[i])}><img src={file_icon} alt={get_file_ext} /><span>{arr_reverse[0].substring(arr_reverse[0].length - 15, arr_reverse[0].length)}</span></a>);
                                                                }

                                                                if(get_file_path.length > 2){
                                                                    var more_div = <span className="etc">+2 more</span>;
                                                                }
                                                            } else{
                                                                var split_file_name = comments_data.file_path[0].toString().split('/');
                                                                var arr_reverse = split_file_name.reverse();
                                                                
                                                                var get_file_ext = arr_reverse[0].substring(arr_reverse[0].lastIndexOf('.')+1, arr_reverse[0].length);
                                                                if(get_file_ext === "pdf"){
                                                                    var file_icon =  "../../images/pdf-icon.png";
                                                                } else{
                                                                    var file_icon =  "../../images/img-icon.png";
                                                                }
                                                                var dis_file_path = <a onClick={this.dataTaggingFunc.bind(this,this.state.list_id,split_file_id[0])}><img src={file_icon} alt={get_file_ext} /><span>{arr_reverse[0].substring(arr_reverse[0].length - 15, arr_reverse[0].length)}</span></a>;
                                                            }

                                                            var replyTxt = "Reply";
                                                            console.log('sub_comment_count',comments_data.sub_comment_count);
                                                            if(comments_data.sub_comment_count > 0){
                                                                
                                                                var replyTxt = "Replies("+comments_data.sub_comment_count+")";
                                                            } else{
                                                                var replyTxt = "Reply";
                                                            }

                                                        }
                                                        return(
                                                        <div className="comment-sec col-md-12 col-xs-12 pad-no" data-comment-id={comments_data.comment_id}>
                                                            <div className="avatar-img">
                                                                <img className="img-responsive" src={user_img} alt="AvatarIMG"/>
                                                            </div>
                                                            <div className="comment-cont">
                                                                <span className="col-md-12 col-xs-12 pad-no user-name">{comments_data.comment_user}</span>
                                                                <span className="col-md-12 col-xs-12 pad-no date">{comments_data.ago_value}</span>


                                                                <div className={`dropdown menu-item prnt_cmnt ${disp_cmnt}`}>
                                                                    <a href="javascript" className="dropdown-toggle" data-toggle="dropdown"><img src="../../images/menu-dot.svg" alt="icon"/></a>
                                                                    <ul className="dropdown-menu">
                                                                        <li><a href="javascript:;" data-comment-id={comments_data.comment_id} className="edit_cmnt">Edit</a></li>
                                                                        <li><a onClick={this.deleteComment.bind(this,comments_data.comment_id, this.props.match.params.list_id, 0)} >Delete</a></li>
                                                                    </ul>
                                                                </div>


                                                                <p className="col-md-12 col-xs-12 pad-no comment-txt">
                                                                    {comments_data.comment_text}
                                                                </p>

                                                                <form className="col-md-12 col-xs-12 pad-no reply-form update_cmnt hide" onSubmit={this.updateCmmnt.bind(this)}>
                                                                    <input type="hidden" name="comment_id" value={comments_data.comment_id} />
                                                                    <input type="hidden" name="parent_comment_id" value="0" />
                                                                    <textarea className="col-md-12 col-xs-12 cmnt_txt" id={`cmmnt_txt_id${comments_data.comment_id}`} name="comment_text" placeholder="" required></textarea>
                                                                    <div className="pull-right">
                                                                        <button type="button" className="btn btn-default cancel_button">Cancel</button>
                                                                        <button type="submit" className="btn btn-green">Update</button>
                                                                    </div>
                                                                </form>

                                                                <div className="attachment-item col-md-12 col-xs-12 pad-no">
                                                                    {dis_file_path} {more_div}
                                                                </div>
  
                                                                {/* <a href="javascript:;" data-target={`#reply_frm${comments_data.comment_id}`} className="pull-left reply-link" onClick={this.replyFunc.bind(this,comments_data.comment_id)}>Reply</a> */}

                                                        <a href="javascript:;" data-target={`#reply_frm${comments_data.comment_id}`} className="pull-left reply-link" data-comment-id={comments_data.comment_id} onClick={this.replyLink.bind(this,comments_data.comment_id, "replyLink")}>
                                                            {comments_data.sub_comment_count ? ('Replies ('+comments_data.sub_comment_count+')') : ('Reply')}
                                                            </a>

                                                                <div className="reply_cnt collapse" id={`reply_cnt${comments_data.comment_id}`}>
                                                                    <form className="col-md-12 col-xs-12 pad-no reply-form reply-frm" onSubmit={this.addSubCommentFunc.bind(this)} ref="form">
                                                                        <input type="hidden" name="parent_comment_id" value={comments_data.comment_id} />

                                                                        <textarea className="col-md-12 col-xs-12 reply_txt_cls" name="reply_txt" placeholder="Reply..." required></textarea>
                                                                        <div className="pull-right">
                                                                            <div className="files_txt">
                                                                                {this.state.attachment_file.length ? (this.state.attachment_file.length+' files are attached'):('')}
                                                                            </div>
                                                                            <a href="javascript:;" className=" btn btn-empty">
                                                                                <input type="file" name="imgInp[]" id="imgInp2" className="add_img replyAttach" multiple onChange={this.loadFile.bind(this)} accept="image/*,application/pdf" />
                                                                                <img src="../../images/attach-icon.svg" alt="icon"/>
                                                                            </a>
                                                                            <button type="submit" className="btn btn-green">
                                                                                <img src="../../images/reply-icon.svg" alt="icon"/>
                                                                                Reply
                                                                            </button>
                                                                        </div>
                                                                    </form>

                                                                    <div className="subCmnt"></div>

                                                                </div>

                                                            </div>
                                                        </div>
                                                        )
                                                    })
                                                }

                                            </div>

                                        </div>                                        
                                    </div>

                                    <div className="col-lg-6 col-md-12 col-xs-12 scanned-right hidden-xs">
                                        <form className="data-feed">
                                            <div className="form-group col-md-12 col-xs-12">
                                                <label>Company Name</label>
                                                <input type="text" name="company-name" className="form-control"/>
                                            </div>
                                            <div className="form-group col-md-6 col-sm-6">
                                                <label>invoice No</label>
                                                <input type="text" name="invoice-no" className="form-control"/>
                                            </div>
                                            <div className="form-group col-md-6 col-sm-6">
                                                <label>Date</label>
                                                <input type="text" name="date" className="form-control"/>
                                            </div>
                                            <div className="form-group col-md-12 col-xs-12">
                                                <label>Address</label>
                                                <textarea cols="3" rows="5" className="form-control"></textarea>
                                            </div>
                                            <div className="form-group col-md-6 col-sm-6">
                                                <label>Currency</label>
                                                <select className="form-control select-dropdown">
                                                    <option>USD</option>
                                                    <option>INR</option>
                                                    <option>SGD</option>
                                                </select>
                                            </div>
                                            <div className="form-group col-md-6 col-sm-6">
                                                <label>Account Category</label>
                                                <select className="form-control select-dropdown">
                                                    <option>Choose Category</option>
                                                    <option>Expenses</option>
                                                    <option>Interest</option>
                                                    <option>Due Amount</option>
                                                </select>
                                            </div>
                                            <div className="form-table">
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <th>No</th>
                                                            <th>Items</th>
                                                            <th>Oty</th>
                                                            <th>Sub Total</th>
                                                            <th>Category</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>01</td>
                                                            <td>Cafe Latte</td>
                                                            <td>01</td>
                                                            <td>35,000</td>
                                                            <td>
                                                                <select className="form-control select-dropdown">
                                                                    <option>Choose Category</option>
                                                                    <option>Expenses</option>
                                                                    <option>Interest</option>
                                                                    <option>Due Amount</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>02</td>
                                                            <td>Creamcheese Walnut Bread</td>
                                                            <td>01</td>
                                                            <td>40,000</td>
                                                            <td>
                                                                <select className="form-control select-dropdown">
                                                                    <option>Choose Category</option>
                                                                    <option>Expenses</option>
                                                                    <option>Interest</option>
                                                                    <option>Due Amount</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <a href="javascript:;" className="add-col"><img src="../../images/plus-icon.svg" alt="icon"/></a>
                                                {/* <div className="category-dd dropdown">
                                                    <span className="dropdown-toggle" data-toggle="dropdown">Category</span>
                                                    <ul className="dropdown-menu">
                                                        <li><a href="javascript:;"><img src="../../images/edit-icon.svg" alt="icon" />Edit Profile</a></li>
                                                        <li><a href="javascript:;"><img src="../../images/settings-icon.svg" alt="icon"/>Settings</a></li>
                                                        <li><a href="javascript:;"><img src="../../images/turn-off-icon.svg" alt="icon"/>Logout</a></li>
                                                    </ul>
                                                </div> */}
                                                
                                            </div>
                                            <div className="form-group currency-label">
                                                <span>Foreign Currency (USD)</span>
                                                <span>Home Currency (SGD)</span>
                                            </div>
                                            <div className="form-group total-input">
                                                <label>Item Total</label>
                                                <div>
                                                    <input type="text" name="item_total_foreign_currency" className="form-control" value={this.state.item_total_foreign_currency} />
                                                    <input type="text" name="item_total_home_currency" className="form-control" onChange={this.convertCurrency} />
                                                </div>
                                            </div>
                                            <div className="form-group total-input">
                                                <label>Tax</label>
                                                <div>
                                                    <input type="text" name="tax_amount_foreign_currency" className="form-control" value={this.state.tax_amount_foreign_currency} />
                                                    <input type="text" name="tax_amount_home_currency" className="form-control" onChange={this.convertCurrency} value={this.state.tax_amount_home_currency} />
                                                </div>
                                            </div>
                                            <div className="form-group total-input">
                                                <label>Grand Total</label>
                                                <div>
                                                    <input type="text" name="grand_total_foreign_currency" className="form-control" value={this.state.grand_total_foreign_currency} />
                                                    <input type="text" name="grand_total_home_currency" className="form-control" onChange={this.convertCurrency} />
                                                </div>
                                            </div>

                                            <div className="form-group exchange-rate">
                                                <label>Exchange Rate 1 SGD </label>
                                                <div>
                                                    <span type="text" name="exchangeRate" className="form-control"  >{this.state.exchange_value}</span>
                                                    <span className="label">{this.state.ToCurrency}</span>
                                                </div>
                                            </div>
                                            <div className="form-group total-input text-right">
                                                <div className="submit-enclose">
                                                    <button className="btn btn-gray" type="button" onClick={this.saveAndContinue}>Clear</button>
                                                    <button className="btn btn-yellow" type="submit" >Save Draft</button>
                                                    <button className="btn btn-green" type="submit">Save & Continue</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                
                        </div>

                    </div>
                </div>
            </div>

            <Footer logoutSubmit = {(e) => this.logoutLink()} />

        </div>
        
       );
    }

 }
 export default data_tagging;
